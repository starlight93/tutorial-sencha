Ext.onReady(function(){
    document.title = qlconfig.project;
});
Ext.require(["Ext.*","QL.*","store_*"]);
var HELPER = new QL.Helper();
Ext.util.Format.currencySign="";
Ext.util.Format.currencySpacer=" "; 

var api = new QL.Api({
	project 	: qlconfig.project,
    writerUrl   : qlconfig[`backend_${qlconfig.development?'development':'production'}_api`],
    readerUrl   : qlconfig[`backend_${qlconfig.development?'development':'production'}_api`],
    loginUrl    : qlconfig[`backend_${qlconfig.development?'development':'production'}_login`],
    rootProperty: qlconfig[`backend_${qlconfig.development?'development':'production'}_root_key`]
});
api.refreshServer();
Ext.application({
	extend: 'qlproject.Application',
    name: 'qlproject'
});