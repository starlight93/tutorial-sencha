new Ext.data.Store({
    storeId : "store_offline__server",
    data:[
        {
            serverName      : "Production",
            serverUrl       : "http://efisukses.dejozz.com/efiserver/public/operation",
            serverPublicPath: "http://efisukses.dejozz.com/efiserver/public",
            serverAuthCheck : "http://efisukses.dejozz.com/efiserver/public/me",
            serverAuthLogin : "http://efisukses.dejozz.com/efiserver/public/login",
            serverPdf       : "https://backend.dejozz.com/pdfrenderer/one.php",
            serverXls       : "https://backend.dejozz.com/pdfrenderer/excel.php",
            serverBarcode   : "https://backend.dejozz.com/pdfrenderer/barcode.php",
            serverWebsock   : "wss://backend.dejozz.com:9001"
        },{
            serverName      : "Development",
            serverUrl       : "http://efisukses.dejozz.com/efiserverdev/public/operation",
            serverPublicPath: "http://efisukses.dejozz.com/efiserverdev/public",
            serverAuthCheck : "http://efisukses.dejozz.com/efiserverdev/public/me",
            serverAuthLogin : "http://efisukses.dejozz.com/efiserverdev/public/login",
            serverPdf       : "https://backend.dejozz.com/pdfrenderer/one.php",
            serverXls       : "https://backend.dejozz.com/pdfrenderer/excel.php",
            serverBarcode   : "https://backend.dejozz.com/pdfrenderer/barcode.php",
            serverWebsock   : "wss://backend.dejozz.com:9001"
        }
    ],
    listeners:{
        load:function(store,data){
            console.log(data);
        }
    }
});
Ext.define("store_offline__server",{});