new Ext.data.Store({
    storeId : "store_offline_date_month",
    data:[
        { id  : "JANUARI", value:"JANUARI" },
        { id  : "FEBRUARI", value:"FEBRUARI" },
        { id  : "MARET", value:"MARET" },
        { id  : "APRIL", value:"APRIL" },
        { id  : "MEI", value:"MEI" },
        { id  : "JUNI", value:"JUNI" },
        { id  : "JULI", value:"JULI" },
        { id  : "AGUSTUS", value:"AGUSTUS" },
        { id  : "SEPTEMBER", value:"SEPTEMBER" },
        { id  : "OKTOBER", value:"OKTOBER" },
        { id  : "NOVEMBER", value:"NOVEMBER" },
        { id  : "DESEMBER", value:"DESEMBER" },
    ]
});
Ext.define("store_offline_date_month",{});