//Generate Year List
var currentTime = new Date();
var now = currentTime.getFullYear();
var years = [];
var y = now+10;
while(now<=y){
    years.push([now]);
    now++;
}
//Create Store
new Ext.data.Store({
    storeId : "store_offline_date_year_now",
    fields: [ 'tahun' ],        
    data: years
});
Ext.define("store_offline_date_year_now",{});