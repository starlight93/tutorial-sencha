new Ext.data.Store({
    storeId : "store_online_current_user_role",
    api:{
        model   :"backend_roles/getroles",
        parameters:{
            paginate : 1000,
            page     : 1
        }
    },
    listeners:{
        load:function(store,data){
            if(Ext.getStore("storemenulist").getData().items.length>2){
                return true;
            }
            let menus = [];
            let alldata = [];
            (data).forEach(function(dt){
                dt=dt.data;
                if( !menus.includes(dt.menu) ){
                    menus.push(dt.menu);
                }
                alldata.push(dt);
            });
            let new_menu = [];
            new_menu.push({ text:"Home",iconCls:"x-fa fa-home", xtype:"Home", leaf:true,expanded:true});
            new_menu.push({ text:"Notifications",iconCls:"x-fa fa-bell", xtype:"notifications", leaf:true,expanded:true});
            qlconfig_sidebar.forEach(function (menu,menuindex) {
                if( menus.includes(menu['text']) ){
                    if(Array.isArray(menu["children"])) {
                        let lastsubmenu = [];
                        menu["children"].forEach(function (submenu,submenuindex) {
                            if (Array.isArray(submenu["children"])) {
                                let lastmenu = [];
                                submenu["children"].forEach(function (childmenu,childmenuindex) {
                                    if(alldata.find(dt=>dt.child_menu==childmenu['text']&&dt.sub_menu==submenu['text'])){
                                        lastmenu.push(childmenu);                             
                                    }
                                });
                                if(lastsubmenu.find(dt=>submenu['text']==dt['text'])===undefined && lastmenu.length>0){
                                    submenu["children"]=lastmenu;
                                    lastsubmenu.push(submenu);
                                }
                            }
                        });
                        if(lastsubmenu.length>0){
                            menu["children"]=lastsubmenu;
                            new_menu.push(menu);
                        }
                    }
                }
            });
            Ext.getStore("storemenulist").setRoot({
                "expanded": true,
                "children": new_menu.length==2?qlconfig_sidebar:new_menu
            });
            var oldXtype = localStorage.xtype;
            window.location.href="#-";
            if( (window.location.href).includes("#")){
                setTimeout(function () {
                    window.location.href="#"+oldXtype;
                },100);
            }
        }
    }
});
Ext.define("store_online_current_user_role",{});