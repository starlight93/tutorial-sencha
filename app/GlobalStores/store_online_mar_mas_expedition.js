new Ext.data.Store({
    storeId : "store_online_mar_mas_expedition",
    api:{
        model   :"mar_mas_expedition",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "mar_mas_expedition.name",
		    orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_mar_mas_expedition",{});