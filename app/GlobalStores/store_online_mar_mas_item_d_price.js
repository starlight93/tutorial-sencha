new Ext.data.Store({
    storeId : "store_online_mar_mas_item_d_price",
    api:{
        model   :"mar_mas_item_d_price",
        parameters:{
            paginate    : 50,
            page        : 1,
            orderby     : "id",
            orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_mar_mas_item_d_price",{});