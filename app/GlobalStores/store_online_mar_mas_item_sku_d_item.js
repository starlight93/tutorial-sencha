new Ext.data.Store({
    storeId : "store_online_mar_mas_item_sku_d_item",
    api:{
        model   :"mar_mas_item_sku_d_item",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "mar_mas_item_sku_d_item.name",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_mar_mas_item_sku_d_item",{});