new Ext.data.Store({
    storeId : "store_online_mar_mas_penanggung_jawab",
    api:{
        model   :"mar_mas_penanggungjawab_all",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_mar_mas_penanggung_jawab",{});