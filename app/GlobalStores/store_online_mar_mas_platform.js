new Ext.data.Store({
    storeId : "store_online_mar_mas_platform",
    api:{
        model   :"mar_mas_platform",
        parameters:{
            paginate    : 50,
            page        : 1,
            orderby     : "id",
            orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_mar_mas_platform",{});