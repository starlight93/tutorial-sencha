new Ext.data.Store({
    storeId : "store_online_pur_mas_item",
    api:{
        model   :"mar_mas_item",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "nama",
		    orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_pur_mas_item",{});