new Ext.data.Store({
    storeId : "store_online_pur_mas_service",
    api:{
        model   :"pur_mas_service",
        parameters:{
            paginate    : 50,
            page        : 1,
            orderby     : "code",
            orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_pur_mas_service",{});