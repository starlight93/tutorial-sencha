new Ext.data.Store({
    storeId : "store_online_pur_mas_supplier",
    api:{
        model   :"pur_mas_supplier",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "pur_mas_supplier.code",
		    orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_pur_mas_supplier",{});