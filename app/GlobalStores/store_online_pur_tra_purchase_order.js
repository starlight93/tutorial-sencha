new Ext.data.Store({
    storeId : "store_online_pur_tra_purchase_order",
    api:{
        model   :"pur_tra_purchase_order",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_pur_tra_purchase_order",{});