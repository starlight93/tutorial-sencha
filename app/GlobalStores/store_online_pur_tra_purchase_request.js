new Ext.data.Store({
    storeId : "store_online_pur_tra_purchase_request",
    api:{
        model   :"pur_tra_bukti_permintaan_stok",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "id",
		    orderbype   : "DESC",
            join        : false,
            where       : "pur_tra_bukti_permintaan_stok.status = 'APPROVAL'",
        }
    }
});
Ext.define("store_online_pur_tra_purchase_request",{});