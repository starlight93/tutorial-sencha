new Ext.data.Store({
    storeId : "store_online_set_mas_business_unit",
    api:{
        model   :"set_mas_business_unit",
        parameters:{
            paginate    : 25,
            page        : 1,
            orderby     : "set_mas_business_unit.name",
            ordertype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_set_mas_business_unit",{});