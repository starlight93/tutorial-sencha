new Ext.data.Store({
    storeId : "store_online_set_mas_coa",
    api:{
        model   :"set_mas_coa",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "segment",
		    orderbype   : "ASC",
            join        : true,
        }
    }
});
Ext.define("store_online_set_mas_coa",{});