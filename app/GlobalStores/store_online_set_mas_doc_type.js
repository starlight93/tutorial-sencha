new Ext.data.Store({
    storeId : "store_online_set_mas_doc_type",
    api:{
        model   :"set_mas_doc_type",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "dbhr.set_mas_doc_type.group",
		    orderbype   : "ASC",
            join        : false,
        }
    }
});
Ext.define("store_online_set_mas_doc_type",{});