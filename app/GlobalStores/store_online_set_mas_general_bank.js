new Ext.data.Store({
    storeId : "store_online_set_mas_general_bank",
    api:{
        model   :"set_mas_general_bank",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "code",
		    orderbype   : "ASC",
            join        : false,
            where       : "status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_bank",{});