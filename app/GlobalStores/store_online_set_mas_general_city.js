new Ext.data.Store({
    storeId : "store_online_set_mas_general_city",
    api:{
        model   :"set_mas_general_city",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "city_name",
		    orderbype   : "ASC",
            join        : false,
            where       : "status = 'true'",
            transform:function(dt){
                return Object.assign(dt,{
                    'name':dt['city_name']
                })
            }
        }
    }
});
Ext.define("store_online_set_mas_general_city",{});