new Ext.data.Store({
    storeId : "store_online_set_mas_general_country",
    api:{
        model   :"set_mas_general_country",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "set_mas_general_country.country_name",
		    orderbype   : "ASC",
            join        : false,
            where       : "set_mas_general_country.status = 'ACTIVE'",
            transform:function(dt){
                return Object.assign(dt,{
                    'name':dt['country_name']
                })
            }
        }
    }
});
Ext.define("store_online_set_mas_general_country",{});