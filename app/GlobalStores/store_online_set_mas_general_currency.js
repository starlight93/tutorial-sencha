new Ext.data.Store({
    storeId : "store_online_set_mas_general_currency",
    api:{
        model   :"set_mas_general_currency",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "set_mas_general_currency.name",
		    orderbype   : "ASC",
            // join        : false,
            where       : "set_mas_general_currency.status = 'ACTIVE' "
        }
    }
});
Ext.define("store_online_set_mas_general_currency",{});