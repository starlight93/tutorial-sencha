new Ext.data.Store({
    storeId : "store_online_set_mas_general_gudang",
    api:{
        model   :"set_mas_general_gudang",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_set_mas_general_gudang",{});