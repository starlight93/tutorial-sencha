new Ext.data.Store({
    storeId : "store_online_set_mas_general_item_type",
    api:{
        model   :"set_mas_general_item_type",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_set_mas_general_item_type",{});