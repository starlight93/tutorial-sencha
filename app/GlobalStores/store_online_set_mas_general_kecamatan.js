new Ext.data.Store({
    storeId : "store_online_set_mas_general_kecamatan",
    api:{
        model   :"set_mas_general_kecamatan",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "set_mas_general_kecamatan.name",
		    orderbype   : "ASC",
            join        : false,
            where       : "set_mas_general_kecamatan.status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_kecamatan",{});