new Ext.data.Store({
    storeId : "store_online_set_mas_general_kelurahan",
    api:{
        model   :"set_mas_general_kelurahan",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "set_mas_general_kelurahan.name",
		    orderbype   : "ASC",
            join        : false,
            where       : "set_mas_general_kelurahan.status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_kelurahan",{});