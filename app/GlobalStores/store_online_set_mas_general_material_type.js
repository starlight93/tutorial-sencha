new Ext.data.Store({
    storeId : "store_online_set_mas_general_material_type",
    api:{
        model   :"set_mas_general_material_type",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "set_mas_general_material_type.name",
		    orderbype   : "ASC",
            where       : "set_mas_general_material_type.status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_material_type",{});