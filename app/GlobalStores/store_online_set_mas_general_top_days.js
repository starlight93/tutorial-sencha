new Ext.data.Store({
    storeId : "store_online_set_mas_general_top_days",
    api:{
        model   :"set_mas_general_top_days",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "set_mas_general_top_days.name",
		    orderbype   : "ASC",
            // join        : false,
            where       : "set_mas_general_top_days.status = 'ACTIVE' "
        }
    }
});
Ext.define("store_online_set_mas_general_top_days",{});