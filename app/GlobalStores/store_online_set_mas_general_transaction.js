new Ext.data.Store({
    storeId : "store_online_set_mas_general_transaction",
    api:{
        model   :"set_mas_general_transaction",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false,
            where       : "status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_transaction",{});