new Ext.data.Store({
    storeId : "store_online_set_mas_general_type_coa",
    api:{
        model   :"set_mas_general_type_coa",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "code",
		    orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_set_mas_general_type_coa",{});