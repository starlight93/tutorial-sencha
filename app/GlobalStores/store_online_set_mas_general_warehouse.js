new Ext.data.Store({
    storeId : "store_online_set_mas_general_warehouse",
    api:{
        model   :"set_mas_general_warehouse",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "set_mas_general_warehouse.code",
		    orderbype   : "ASC",
            join        : false,
            where       : "set_mas_general_warehouse.status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_general_warehouse",{});