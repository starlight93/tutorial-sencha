new Ext.data.Store({
    storeId : "store_online_set_mas_item_gl_category",
    api:{
        model   :"set_mas_item_gl_category",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "item_gl_category",
		    orderbype   : "ASC",
            join        : false
        }
    }
});
Ext.define("store_online_set_mas_item_gl_category",{});