new Ext.data.Store({
    storeId : "store_online_set_mas_ledger",
    api:{
        model   :"set_mas_ledger",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    ordertype   : "ASC",
            where       : "set_mas_ledger.active_flag = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_ledger",{});