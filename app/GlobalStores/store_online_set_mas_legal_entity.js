new Ext.data.Store({
    storeId : "store_online_set_mas_legal_entity",
    api:{
        model   :"set_mas_legal_entity",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    ordertype   : "ASC",
            where       : "set_mas_legal_entity.status = 'ACTIVE'"
        }
    }
});
Ext.define("store_online_set_mas_legal_entity",{});