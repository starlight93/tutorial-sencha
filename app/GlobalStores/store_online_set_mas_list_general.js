new Ext.data.Store({
    storeId : "store_online_set_mas_list_general",
    api:{
        model   :"set_mas_list_general",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_set_mas_list_general",{});