new Ext.data.Store({
    storeId : "store_online_set_mas_region",
    api:{
        model   :"set_mas_region",
        parameters:{
            paginate    : 9999,
            page        : 1,
		    orderby     : "name",
		    orderbype   : "ASC",
            join        : false,
        },
        transform   :function(dt){
            Object.assign(dt,{
                cobasaja:dt['id'],
                aku:'dia'
            })
            return dt;
        }
    }
});
Ext.define("store_online_set_mas_region",{});