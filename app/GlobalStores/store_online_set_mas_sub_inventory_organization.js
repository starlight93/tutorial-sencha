new Ext.data.Store({
    storeId : "store_online_set_mas_sub_inventory_organization",
    api:{
        model   :"set_mas_sub_inventory_organization",
        parameters:{
            paginate    : 25,
            page        : 1,
		    orderby     : "name",
            ordertype   : "ASC",
            // join        : false
        }
    }
});
Ext.define("store_online_set_mas_region",{});