new Ext.data.Store({
    storeId : "store_online_warehouse_stock",
    api:{
        model   :"inv_tra_mutation_list/warehouse",
        parameters:{
            paginate    : 50,
            page        : 1,
            orderby     : "warehouse",
            orderbype   : "ASC",
        }
    }
});
Ext.define("store_online_warehouse_stock",{});