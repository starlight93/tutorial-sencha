Ext.define('QL.Api', function(){
    var me;
    return {
    constructor:function(data={}){
        me = this;
        me.writerUrl = data.writerUrl;
        me.readerUrl = data.readerUrl;
        me.loginUrl  = data.loginUrl;
        me.project = data.project;
        me.rootProperty = data.rootProperty;
        let allStores = Ext.StoreManager.keys;
        allStores.forEach(function( storeName ){
            if( storeName.includes("store_online") ){
                var store = Ext.StoreManager.lookup(storeName);
                store.setProxy(
                    me.getData(store.api)
                );
                // store.load();
            }

        });
    },
    writerUrl   : "",
    readerUrl   : "",
    loginUrl    : "",
    token       : "",
    project     : "",
    rootProperty: "",
    headers     : {},
    setToken    : function(token){
        me.token = token;
    },
    setHeaders  : function(data){
        localStorage.auth=JSON.stringify(data);
        me.headers = data;
    },
    addHeaders  : function(data){
        let oldHeaders = me.headers;
        Object.assign(oldHeaders,data);
        me.headers=oldHeaders;
    },
    //================================================================
    changeStatus : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        let bigUrl = (data.model).includes("http")?data.model:`${me.readerUrl}/${data.model}/${data.id}`;
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url:bigUrl,
            headers: me.headers,
            method: "PATCH",
            jsonData :data.data,
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText,true);
                callback(obj);
                Ext.getCmp("loadingDialog").hide();
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
                error(response);
                Ext.Msg.alert("Failed!","Maybe Server Error");
            }
        })
    },
    // =======================================================
    getDraftId:function(callback=function(code){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        Ext.Ajax.request({
            url: `https://laradev2.dejozz.com/get-draft-id`,
            method:"GET",
            success: function(response, opts) {
                callback(response.responseText);
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    getArrayData:function(data={},callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        var params = data.params == undefined ? {}:data.params;
        params = data.parameters !== undefined ? data.parameters:params;
        let keys = Object.keys(params);
        var parameters="";
        keys.forEach(function(key){
            var value = params[key];
            parameters += `${key}=${value}&`
        });
        let bigUrl = (data.model).includes("http")?data.model:`${me.readerUrl}/${data.model}`;
        Ext.Ajax.request({
            headers: me.headers,
            url: `${bigUrl}?${parameters}`,
            method:"GET",
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true)===null?response.responseText:Ext.decode(response.responseText,true);
                if(!Ext.isArray(obj) && Ext.isObject(obj)){
                    obj = obj[me.rootProperty]===undefined?obj:obj[me.rootProperty];
                }
                callback(obj);
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
                Ext.getCmp("loadingDialog").hide();
                Ext.getCmp("loadingDialogBOM").hide();
                // Ext.Msg.alert("Failed!","Maybe Server Error");
            }
        });
    },
//===============================================================
    getData: function(data={},custom=false){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        var params = data.parameters == undefined ? {}:data.parameters;
        var url  = custom? `${qlconfig[`backend_${qlconfig.development?'development':'production'}_custom`]}/${data.model}/${data.function}`:`${me.readerUrl}/${data.model}`;
        let bigUrl = (data.model).includes("http")?data.model:url;
        let rootProperty = data['rootProperty']!==undefined?data['rootProperty']:me.rootProperty;
        var proxy=  {
                headers:me.headers,
                type: 'ajax',
                url: bigUrl,
                extraParams : params,
                // paramsAsJson:true,
                reader: {
                    type: 'json',
                    rootProperty: rootProperty,
                    transform: {
                        fn: function(dt) {
                            if(data.transform!==undefined && typeof(data.transform)=='function'){
                                for(let iterasi in dt [ rootProperty ] ){
                                    dt[rootProperty][iterasi] = data.transform(dt[rootProperty][iterasi]);
                                };
                            }
                            return dt;
                        },
                        scope: this
                    }
                },
            }
        return proxy;
    },
//=================================================================UPDATE UPLOAD
    updateUpload: function (data,callback=function(json){},error=function(json){}){
        var me = this;
        let bigUrl = (data.model).includes("http")?data.model:`${me.writerUrl}/${data.model}/${data.id}`;
        Ext.getCmp("loadingDialog").show();
        var request = new XMLHttpRequest();
        data.data.append("_method", "put");
        request.open("POST", bigUrl);
        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.status == 200 || request.status == 201) {
                    Ext.getCmp("loadingDialog").hide();
                    callback(Ext.decode(request.responseText,true));
                }else{
                    Ext.getCmp("loadingDialog").hide();
                    if( qlconfig[`backend_error_action`] !==undefined){
                        let error=qlconfig[`backend_error_action`];
                        error(data.model,Ext.decode(request.responseText,true), request.status );
                    }else{
                        error(Ext.decode(request.responseText,true));
                    }
                }
            }
        }
        request.setRequestHeader("Accept", 'Application/json');
        (Object.keys(me.headers)).forEach(function(key){
            request.setRequestHeader(key,me.headers[key]);
        });
        request.send(data.data);
    },
//=================================================================CREATE UPLOAD
    createUpload: function (data,callback=function(json){},error=function(json){},progress=function(evt){},load=true){
        var me = this;
        let bigUrl = (data.model).includes("http")?data.model:`${me.writerUrl}/${data.model}`;
        if(load){
            Ext.getCmp("loadingDialog").show();
        }
        var request = new XMLHttpRequest();
        request.open("POST", bigUrl);
        request.onreadystatechange = function() {
            if (request.readyState === 4) {
                if (request.status == 200 || request.status == 201) {
                    if(load){
                        Ext.getCmp("loadingDialog").hide();
                    }
                    callback(Ext.decode(request.responseText,true));
                }else{
                    if(load){
                        Ext.getCmp("loadingDialog").hide();
                    }else{
                        error(Ext.decode(request.responseText,true));
                        return;
                    }
                    if( qlconfig[`backend_error_action`] !==undefined){
                        let error=qlconfig[`backend_error_action`];
                        error(data.model,Ext.decode(request.responseText,true), request.status );
                    }else{
                        error(Ext.decode(request.responseText,true));
                    }
                }
            }
        }
        request.setRequestHeader("Accept", 'Application/json');
        (Object.keys(me.headers)).forEach(function(key){
            request.setRequestHeader(key,me.headers[key]);
        });
        request.upload.addEventListener('progress', progress);
        request.send(data.data);
    },
//================================================================
    create : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        if(data["multipart"]!==undefined && data["multipart"]===true){
            me.createUpload(data,callback,error);
            return true;
        }
        let payloads = data.data;
        let keys = Object.keys(payloads)
        for(let i=0;i< keys.length;i++){
            if( !Array.isArray(payloads[keys[i]]) ){
                if( !(/^[a-z0-9._]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( Array.isArray(payloads[keys[i]]) && !(keys[i]).includes("_detail")){
                if( !(/^[a-z0-9._]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( (keys[i]).includes("_detail")){
                if(payloads[keys[i]].length>0){
                    var detailKeys= Object.keys(payloads[keys[i]][0]);
                    for(let j=0;j< detailKeys.length;j++){
                        if( !(/^[a-z0-9._]{1,}$/).test(detailKeys[j]) ){
                            Ext.Msg.alert("Correction!",`field [${detailKeys[j]}] di [${keys[i]}] tidak sesuai standar nama`);
                            return true;
                        }
                    }
                }
            }
        }

        let bigUrl = (data.model).includes("http")?data.model:`${me.writerUrl}/${data.model}`;
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: bigUrl,
            headers: me.headers,
            method: "POST",
            jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true);
                callback(obj);
                // console.dir(obj);
            },
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                if( qlconfig[`backend_error_action`] !==undefined){
                    let error=qlconfig[`backend_error_action`];
                    error(data.model,Ext.decode(response.responseText,true), response.status );
                }else{
                    error(Ext.decode(response.responseText,true));
                }
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },
    delete : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        let bigUrl = (data.model).includes("http")?data.model:`${me.writerUrl}/${data.model}/${data.id}`;
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: bigUrl,
            headers: me.headers,
            method: "DELETE",
            jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true);
                callback(obj);
                // console.dir(obj);
            },
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();if( qlconfig[`backend_error_action`] !==undefined){
                    let error=qlconfig[`backend_error_action`];
                    error(data.model,Ext.decode(response.responseText,true), response.status );
                }else{
                    error(Ext.decode(response.responseText,true));
                }
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    update : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        if(data["multipart"]!==undefined && data["multipart"]===true){
            me.updateUpload(data,callback,error);
            return true;
        }
        let payloads = data.data;
        let keys = Object.keys(payloads)
        for(let i=0;i< keys.length;i++){
            if( !Array.isArray(payloads[keys[i]]) ){
                if( !(/^[a-z0-9._]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( Array.isArray(payloads[keys[i]]) && !(keys[i]).includes("_detail")){
                if( !(/^[a-z0-9._]{1,}$/).test(keys[i]) ){
                    Ext.Msg.alert("Correction!",`field [${keys[i]}] tidak sesuai standar nama`);
                    return true;
                }
            }else if( (keys[i]).includes("_detail")){
                if(payloads[keys[i]].length>0){
                    var detailKeys= Object.keys(payloads[keys[i]][0]);
                    for(let j=0;j< detailKeys.length;j++){
                        if( !(/^[a-z0-9._]{1,}$/).test(detailKeys[j]) ){
                            Ext.Msg.alert("Correction!",`field [${detailKeys[j]}] di [${keys[i]}] tidak sesuai standar nama`);
                            return true;
                        }
                    }
                }
            }
        }

        let bigUrl = (data.model).includes("http")?data.model:`${me.writerUrl}/${data.model}/${data.id}`;
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url: bigUrl,
            headers: me.headers,
            method: "PUT",
            jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true);
                callback(obj);
                // console.dir(obj);
            },
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                if( qlconfig[`backend_error_action`] !==undefined){
                    let error=qlconfig[`backend_error_action`];
                    error(data.model,Ext.decode(response.responseText,true), response.status );
                }else{
                    error(Ext.decode(response.responseText,true));
                }
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    read : function(data,callback=function(json){}, error=function(json){}){
        let readHeader = {
            URLORIGIN : window.location.hash
        };
        if(data['headers'] !==undefined ){
            Object.assign(readHeader,data['headers']);
        }
        me.addHeaders(readHeader);
        let bigUrl = (data.model).includes("http")?data.model:`${me.readerUrl}/${data.model}/${data.id}`;
        Ext.getCmp("loadingDialog").hide();
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url:bigUrl,
            headers: me.headers,
            method: "GET",
            jsonData :data.data,
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true)===null?response.responseText:Ext.decode(response.responseText,true);
                callback(obj);
                // console.dir(obj);
            },
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                error(Ext.decode(response.responseText,true));
            }
        })
    },

    login : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        Ext.getCmp("loadingDialog").show();
            Ext.Ajax.request({
                url: me.loginUrl,
                method: "POST",
                jsonData :data.data,
                success: function(response, opts) {
                    me.afterLogin();
                    var obj = Ext.decode(response.responseText,true);
                    callback(obj);
                    Ext.getCmp("loadingDialog").hide();
                    console.log('request success');
                },
                failure: function(response, opts) {
                    Ext.getCmp("loadingDialog").hide();
                    error(Ext.decode(response.responseText,true));
                    var text = "Request Failed!"
                    if( [422,401].includes(response.status)){
                        text = "Username or Password was not correct!"
                    }else if( [405].includes(response.status)){
                        text = "Method Not Allowed"
                    }
                    Ext.Msg.alert("Failed!",text);
                }
        });
    },
    getMe : function(callback=function(json){}, error=function(json){}){
        Ext.getCmp("loadingDialog").hide();
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        if(!qlconfig.use_login){
            if(localStorage.first=='1'){
                if((Ext.Viewport.getItems().items).length>0){
                    Ext.Viewport.remove(0);
                }
                Ext.Viewport.add([{xtype:"Login"}]);
                me.afterLogin();
            }else{
                if((Ext.Viewport.getItems().items).length>0){
                    Ext.Viewport.remove(0);
                }
                Ext.Viewport.add([{xtype:"projectmainview"}]);
                var oldXtype = localStorage.xtype!=undefined?localStorage.xtype:"Home";
                window.location.href="#-";
                setTimeout(function () {
                    window.location.href="#"+oldXtype;
                },100);
            }
            localStorage.first="0";
            return true;
        }
        Ext.getCmp("loadingDialog").show();
        Ext.Ajax.request({
            url      :qlconfig[`backend_${qlconfig.development?'development':'production'}_auth`],
            headers  : me.headers,
            method   : "GET",
            success: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                var obj = Ext.decode(response.responseText,true);
                callback(obj);
                if(localStorage.first=='1'){
                    if((Ext.Viewport.getItems().items).length>0){
                        Ext.Viewport.remove(0);
                    }
                    Ext.Viewport.add([{xtype:"projectmainview"}]);
                    me.afterLogin();
                }
                localStorage.first="0";
            },
            failure: function(response, opts) {
                Ext.getCmp("loadingDialog").hide();
                console.log('server-side failure with status code ' + response.status);
                if([401].includes(response.status)){
                    if((Ext.Viewport.getItems().items).length>0){
                        Ext.Viewport.remove(0);
                    }
                    Ext.Viewport.add( {xtype:'Login'} );
                }
            }
        })
    },
    afterLogin: function(){
        let me = this;
        let allStores = Ext.StoreManager.keys;
        allStores.forEach(function( storeName ){
            if( storeName.includes("store_online") ){
                var store = Ext.StoreManager.lookup(storeName);
                store.setProxy(
                    me.getData(store.api)
                );
            }

        });
    },

    custom : function(data,callback=function(json){}, error=function(json){}){
        me.addHeaders({
            URLORIGIN : window.location.hash
        });
        let bigUrl = (data.model).includes("http")?data.model:`${qlconfig[`backend_${qlconfig.development?'development':'production'}_custom`]}/${data.model}/${data.function}`;
        Ext.Ajax.request({
            url:bigUrl,
            headers: me.headers,
            method: data['method']==undefined?'GET':data['method'],
            jsonData :data.data,
            success: function(response, opts) {
                // Ext.getCmp("loadingDialog").hide();
                // var obj = Ext.decode(response.responseText,true);
                callback(response);
            },
            failure: function(response, opts) {
                // Ext.getCmp("loadingDialog").hide();
                error(Ext.decode(response.responseText,true) );
                console.log('server-side failure with status code ' + response.status);
            }
        })
    },

    downloadFile: function(model,filename='file.xlsx'){
        let xhr = new XMLHttpRequest();
        xhr.open('GET', (model.includes('http')?'':api.readerUrl) + model, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.setRequestHeader("UserToken", me.token==""?sessionStorage.token:me.token);
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            if (this.status === 200) {
                var blob = this.response;
                var contentTypeHeader = xhr.getResponseHeader("Content-Type");
                var downloadLink = window.document.createElement('a');
                downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                downloadLink.download = filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        };
        xhr.send();
    },
    refreshServer : function(){
        if(localStorage.server_configuration===undefined){
            return;
        }
        let data = JSON.parse(localStorage.server_configuration);
        qlconfig[`backend_${qlconfig.development?'development':'production'}_api`]=data.serverUrl;
        qlconfig[`backend_${qlconfig.development?'development':'production'}_public_path`]=data.serverPublicPath;
        qlconfig[`backend_${qlconfig.development?'development':'production'}_login`]= data.serverAuthLogin;
        qlconfig[`backend_${qlconfig.development?'development':'production'}_auth`] = data.serverAuthCheck;

        qlconfig.backend_websocket=data.serverWebsock;
        qlconfig.backend_pdf_renderer=data.serverPdf;
        qlconfig.backend_xls_renderer=data.serverXls;
        qlconfig.backend_barcode_renderer=data.serverBarcode;

        me.writerUrl = qlconfig[`backend_${qlconfig.development?'development':'production'}_api`];
        me.readerUrl = qlconfig[`backend_${qlconfig.development?'development':'production'}_api`];
        me.loginUrl  = qlconfig[`backend_${qlconfig.development?'development':'production'}_login`];
    }
  }
//===================================================
});