Ext.define("QL.CalendarForm",{
    override:'Ext.calendar.form.Base',
    fireSave: function(data) {
        this.fireEvent('save', this, {
            data: data
        });
    },
})
Ext.define('QL.CalendarAbstractForm', {
    override: 'Ext.calendar.form.AbstractForm',
 
    trackResetOnLoad: true,
    defaultListenerScope: true,
 
    platformConfig: {
        '!desktop': {
            width: '100%',
            height: '100%',
            layout: 'fit',
            isCompact: true,
            fullscreen: true
        },
 
        'desktop': {
            modal: true,
            centered: true,
            scrollable: 'y'
        }
    },
 
    floated: true,
 
    config: {
        /**
         * @cfg {Object} calendarField
         * The config for the calendar field.
         */
        calendarField: {
            xtype: 'calendar-calendar-picker',
            label: 'Calendar',
            name: 'calendarId',
            displayField: 'title',
            valueField: 'id'
        },
 
        /**
         * @cfg {Object} titleField
         * The config for the title field.
         */
        titleField: {
            xtype: 'textfield',
            label: 'Title',
            name: 'title'
        },
 
        /**
         * @cfg {Object} startDateField
         * The config for the start date field.
         */
        startDateField: {
            xtype: 'datepickerfield',
            label: 'From',
            itemId: 'startDate',
            name: 'startDate',
            dateFormat:"Y-m-d"
        },
 
        /**
         * @cfg {Object} startTimeField
         * The config for the start time field.
         */
        startTimeField: {
            xtype: 'calendar-timefield',
            label: '&#160;',
            itemId: 'startTime',
            name: 'startTime'
        },
        endDateField: {
            xtype: 'datepickerfield',
            label: 'To',
            itemId: 'endDate',
            name: 'endDate',
            dateFormat:"Y-m-d"
        },
 
        /**
         * @cfg {Object} endTimeField
         * The config for the end time field.
         */
        endTimeField: {
            xtype: 'calendar-timefield',
            label: '&#160;',
            itemId: 'endTime',
            name: 'endTime',
            dateFormat:"Y-m-d"
        },
        allDayField: {
            xtype: 'checkboxfield',
            itemId: 'allDay',
            name: 'allDay',
            label: 'All Day',
            listeners: {
                change: 'onAllDayChange'
            }
        },
        descriptionField: {
            xtype: 'textareafield',
            label: 'Description',
            name: 'description',
            flex: 1,
            minHeight: '6em'
        },
        dropButton: {
            text: 'Delete',
            handler: 'onDropTap'
        },
        saveButton: {
            text: 'Save',
            handler: 'onSaveTap'
        },
        cancelButton: {
            text: 'Cancel',
            handler: 'onCancelTap'
        }
    },
});

Ext.define('QL.CalendarForm',{
    override:"Ext.calendar.form.Form",
    produceEventData: function(values) {
        var D = Ext.Date,
            view = this.getView(),
            startTime = values.startTime,
            endTime = values.endTime,
            startDate = new Date(values.startDate),
            endDate = new Date(values.endDate),
            sYear = startDate.getFullYear(),
            sMonth = startDate.getMonth(),
            sDate = startDate.getDate(),
            eYear = endDate.getFullYear(),
            eMonth = endDate.getMonth(),
            eDate = endDate.getDate();
 
        if (values.allDay) {
            // All day events are always GMT.
            startDate = D.utc(sYear, sMonth, sDate);
 
            // midnight the next day
            endDate = D.add(D.utc(eYear, eMonth, eDate), D.DAY, 1);
            delete values.startTime;
            delete values.endTime;
        }
        else {
            startDate = view.toUtcOffset(
                new Date(sYear, sMonth, sDate, startTime.getHours(), startTime.getMinutes())
            );
            endDate = view.toUtcOffset(
                new Date(eYear, eMonth, eDate, endTime.getHours(), endTime.getMinutes())
            );
        }
 
        values.startDate = startDate;
        values.endDate = endDate;
 
        return values;
    },
})