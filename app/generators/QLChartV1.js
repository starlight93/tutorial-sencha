Ext.define('QL.ChartV1',{
    extend: 'Ext.Panel',
    requires:['Ext.Panel'],
    xtype: 'QLChartV1',    
    alias: 'widget.QLChartV1',
    cls:"QLchart",
    height: "400px",
    caption:"chart",
    legend:false,
    labelX:true,
    labelY:true,
    style:"background-color:white;margin-bottom:15px",
    items:[
        {
        html:`<h4 class="QLchart_title" style:"margin-bottom:5px"></h4>`,
        set:function(){
            var el = this.renderElement.dom.getElementsByTagName("h4")[0];
            el.innerText = this.up("panel").caption;
            el.style.backgroundColor = 'white';
        }
    },
    {
        xtype: "container",
        html:`
        <div class="QLchart-container" style="box-shadow:0px 0px 5px rgba(0,0,0,0.18);border-radius:10px;overflow:hidden;">
        <canvas width="400" height="200"></canvas></div>`,
        set:function(){
            var me = this;
            this.renderElement.dom.getElementsByTagName("canvas")[0].setAttribute("height",this.up("panel").chartHeight+"px");
            this.renderElement.dom.getElementsByTagName("canvas")[0].setAttribute("width",this.up("panel").getWidth());
            var ctx = this.renderElement.dom.getElementsByTagName("canvas")[0];
            ctx.style.backgroundColor = 'white';
            akuctx=ctx;
            let chartData = this.up("panel").getData();
            let labels=chartData['labels'];
            var border = chartData['border'], backgroundColor,borderColor;
            var fill = chartData['fill'];
            var data = chartData['data'];
            if(Array.isArray(border)){
                borderColor=border;
            }else{
                borderColor=[];
                labels.forEach(element => {
                    borderColor.push(border);
                });
            }
            if(Array.isArray(fill)){
                backgroundColor=fill;
            }else{
                backgroundColor=[];
                labels.forEach(element => {
                    backgroundColor.push(fill);
                });
            }
            new Chart(ctx, {
                type: this.up("panel").type!==undefined?this.up("panel").type:"line",
                data: {
                    labels: labels,
                    datasets: [{
                        fill: false,
                        label: 'Data',
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: borderColor,
                        borderWidth: 2
                    }]
                },
                options: {
                    plugins:{                        
                        labels: [
                            // {
                            //     render: 'label',
                            //     fontColor: '#000',
                            //     position: 'outside'
                            // },
                            {
                                render: function (isi) {
                                    // args will be something like:
                                    // { label: 'Label', value: 123, percentage: 50, index: 0, dataset: {...} }
                                    return ['pie',"doughnut"].includes(me.up("panel").type)?isi.percentage+"%":isi.value.toLocaleString();
                                
                                    // return object if it is image
                                    // return { src: 'image.png', width: 16, height: 16 };
                                  },
                                precision: 2
                            }
                        ]
                    },
                    tooltips: {
                        callbacks: {
                            // label: function(tooltipItem, data) {
                            //     return tooltipItem.yLabel.toLocaleString();
                            // }
                        }
                    },
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            },
                            ticks: {
                                display: this.up("panel").labelX,
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display: true,
                                labels: {
                                    fontSize:8,
                                    boxWidth:25
                                }
                            },
                            ticks:{
                                display: this.up("panel").labelY,
                                beginAtZero:true,
                                callback: function(label, index, labels) {
                                    return label.toLocaleString();
                                }
                            }
                        }]
                    },
                    legend: {
                        display: this.up("panel").legend,
                        position:'bottom'
                    }
                }
            });
        }
    }],    
    view:function(){
        this.getItems().items[0].set();
        this.getItems().items[1].set();
    },
});