
Ext.define('QL.Controller', {
    alias: 'controller.qlcontroller',
	extend: 'Ext.app.ViewController',
	formPasswordValidator: function (value) {
		var password1 = this.lookup('password1'),
			password1Value = password1.getValue();

		if (value || password1Value) {
			if (value !== password1Value) {
				return 'Passwords do not match.';
			}
		}
		return true;
    },
	formFocusErrors: function (el) {
        
        var form = (el.up("formpanel") ==null)?form = this.getView():el.up("formpanel");
			// errorCmp = this.lookup('formErrorState'),
			// tooltip = errorCmp.getTooltip(),
		var	errors = [],
			data = {
				errors: errors
			};
            var firstFieldError=null;

		form.getFields(false).forEach(function (field) {
			var error;
			if (!field.validate() && (error = field.getError())) {
				errors.push({
					errors: error,
					name: field.getLabel()
                });
                if(firstFieldError==null){
                    firstFieldError = field;
                }
			}
        });
        if(firstFieldError!=null){
            firstFieldError.focus(false,1200);
            console.log(data);
        }
		// if (errors.length) {
		// 	tooltip.setData(data);
		// 	tooltip.show();
		// } else {
		// 	tooltip.hide();
		// }

		// errorCmp.setData(data);
	},
	formSubmit: function (el) {
        var out;
        var form = el.up("formpanel");
        try{
            form.validate();
        }catch(err){
            form = this.getView();     
        }
		/* Normally we would submit the form to the server here and handle the response...
		form.submit({
            headers:{

            },
		    clientValidation: true,
		    url: 'register.php',
		    success: function(form, action) {
		       //...
		    },
		    failure: function(form, action) {
		        //...
		    }
		});
		*/
		if (form.validate()) {
			out = [];

			Ext.Object.each(form.getValues(), function (key, value) {
				out.push(key + '=' + value);
			});
			console.log(out);
			Ext.Msg.alert('Submitted Values', out.join('<br />'));
		} else {
			this.formFocusErrors(el);
		}
    },
    formPopup: (el,x)=>{
        var dstTxt = el.getId()+"_txt";
        var dstId  = el.getId()+"_id";
        var storePopup = Ext.StoreManager.lookup(el.popupStore());
        var sortedData = storePopup.getProxy().data;
        var matches = [];
        for(let i=0;i<10;i++){
            if(i==storePopup.length){
                break;
            }
            matches.push(sortedData[i]);
        }
        Ext.StoreManager.lookup(el.popupStore()).loadData(matches);
        var gridColumns = storePopup.columns;
        if(gridColumns[gridColumns.length-1].text!=undefined && gridColumns[gridColumns.length-1].text!='Select'){
            gridColumns.push(
                {   
                    text:'Select',
                    align:'center',
                    value:"{record.id}",
                    cell: {
                        tools: {
                            approve: {
                                iconCls: 'x-fa fa-check',
                                ui:'action',
                                handler:function(element,currentEl){
                                    var data= currentEl.record.data;
                                    Ext.getCmp(dstTxt).setValue(data[storePopup.selector.dataText]);
                                    Ext.getCmp(dstId).setValue(data[storePopup.selector.dataId]);
                                    element.up('dialog').destroy();
                                }
                            }
                        }
                    }, 
                    resizable:false,
                    width:"10%"
                },
            );
        }
        var dialog = Ext.create({      
            viewModel: {
                data: {
                    gridCurrentPage: "1",
                    currentRows : 10,
                    disabledPrev: false,
                    disabledNext: false
                },
                formulas:{
                    nextPressed:{
                        get: function(data){
                            return false;
                        },
                        set:function(value){
                            console.log(value);
                            this.set({
                                gridCurrentPage:gridCurrentPage+value
                            });
                        }
                    },
                    storeSampling:{
                        get: function(data){
                            diaa= this;
                        },
                        set:function(value){
                            console.log(value);
                            this.set({
                                gridCurrentPage:gridCurrentPage+value
                            });
                        }
                    }
                },  
            },
            controller: {
                maxRows:function(dataView){
                    var thisModel = this.getView().getViewModel();
                    thisModel.set('currentRows',dataView.value);
                    var data = Ext.StoreManager.lookup(el.popupStore()).getProxy().data;
                    if(dataView.value=='All'){
                        Ext.StoreManager.lookup(el.popupStore()).load();
                    }else{
                        var matches = [];
                        for(let i=0;i<dataView.value;i++){
                            if(i==data.length){
                                break;
                            }
                            matches.push(data[i]);
                        }
                        Ext.StoreManager.lookup(el.popupStore()).loadData(matches);
                    }
                },
                pressPage: function(element){
                    var thisModel = this.getView().getViewModel();
                    thisModel.set("gridCurrentPage",element.getText());
                    for(let i=0;i< element.getParent().getItems().keys.length;i++){
                        Ext.getCmp(element.getParent().getItems().keys[i]).setPressed(false);
                    }
                    element.setPressed(true);
                },
                pressNext: function(element){
                    var thisModel = this.getViewModel();
                    var buttons = element.getParent().down('panel').getItems().items;
                    // var currentPage = parseInt( thisModel.get("gridCurrentPage") );
                    var currentPageArray=0;
                    for(let i=0;i< buttons.length;i++){
                        if(buttons[i].getText() == thisModel.get("gridCurrentPage")){
                            currentPageArray = i;
                        }
                        buttons[i].setPressed(false);
                    }
                    if( (currentPageArray+1) < buttons.length){
                        buttons[ (currentPageArray+1) ].setPressed(true);
                        thisModel.set("gridCurrentPage",buttons[ (currentPageArray+1) ].getText());
                        thisModel.set("disabledNext",false)
                    }else{
                        buttons[ buttons.length-1 ].setPressed(true);
                        thisModel.set("disabledNext",true)
                    }
                    thisModel.set("disabledPrev",false)
                },
                pressPrev: function(element){
                    var thisModel = this.getViewModel();
                    var buttons = element.getParent().down('panel').getItems().items;
                    // var currentPage = parseInt( thisModel.get("gridCurrentPage") );
                    var currentPageArray=0;
                    for(let i=0;i< buttons.length;i++){
                        if(buttons[i].getText() == thisModel.get("gridCurrentPage")){
                            currentPageArray = i;
                        }
                        buttons[i].setPressed(false);
                    }
                    if(currentPageArray > 0){
                        buttons[ (currentPageArray-1) ].setPressed(true);
                        thisModel.set("gridCurrentPage",buttons[ (currentPageArray-1) ].getText());
                        thisModel.set("disabledPrev",false);
                    }else{
                        buttons[ 0 ].setPressed(true);
                        thisModel.set("disabledPrev",true);
                    }
                    thisModel.set("disabledNext",false)
                },
            },
            xtype:'dialog',
            maximizable: true,	
            modal:true,
            minWidth:800,
            minHeight:600,
            scrollable:true,
            closable:true,
            title: 'Business Unit',
            cls: 'go-table',
            bbar: [
                {
                    xtype:'button',
                    ui:'action',
                    text:'prev',
                    margin:'5 5 0 0',
                    bind: {
                        disabled:"{disabledPrev}"
                    },
                    handler: 'pressPrev'
                },{
                    xtype:'panel',
                    // layout:'hbox',
                    items:[
                        {
                            xtype:'button',
                            text:'1',
                            ui:'back',
                            margin:'2 2 2 2',
                            pressed: true,
                            handler: 'pressPage'
                        },{
                            xtype:'button',
                            text:'2',
                            ui:'back',
                            margin:'2 2 2 2',
                            pressed: false,
                            handler: 'pressPage'
                        },{
                            xtype:'button',
                            text:'3',
                            ui:'back',
                            margin:'2 2 2 2',
                            pressed: false,
                            handler: 'pressPage'
                        }
                    ]
                },                
                {
                    xtype:'button',
                    ui:'action',
                    text:'next',
                    margin:'5 0 0 5',
                    bind: {
                        disabled:"{disabledNext}"
                    },
                    handler: 'pressNext'
                },                
                {
                    xtype:'button',
                    ui:'action',
                    docked:"right",
                    text:'SELECT',
                    margin:'30 15 30 0',
                    handler: function(el,a){
                        dia=el;
                    }
                }
        ],
            items: [
                {
                        xtype: 'grid',    
                        title: ' ',
                        scrollable:true,
                        header: false,
                        viewModel:true,
                        store: storePopup,
                        // bbar:[
                        //     {
                        //         xtype:'displayfield', text:'aku'
                        //     }
                        // ],
                        titleBar: {
                            shadow: true,
                            maxHeight: 42,
                            items: [{
                                align: 'left',
                                xtype: 'button',
                                bind:{
                                    text: '{currentRows} Rows',
                                },
                                stretchMenu: true,
                                cls: 'go-table--sort',
                                // arrow: true,
                                // style:"background-color:white",
                                menu: {
                                    indented: false,
                                    items: [{
                                        text: '10 Rows',
                                        value:10,
                                        handler: 'maxRows'
                                    },{
                                        text: '25 Rows',
                                        value:25,
                                        handler: 'maxRows'
                                    },{
                                        text: '50 Rows',
                                        value:50,
                                        handler: 'maxRows'
                                    },{
                                        text: '100 Rows',
                                        value:100,
                                        handler: 'maxRows'
                                    },{
                                        text: 'All Rows',
                                        value:'All',
                                        handler: 'maxRows'
                                    }]
                                }
                            },{
                                xtype:'searchfield',
                                placeholder: 'search',
                                ui:'solo',
                                cls:'go-table--search',
                                align: 'right',
                                autoComplete:false,
                                listeners: {
                                    buffer: 500,
                                    change: function(field){
                                        this.up('dialog').getViewModel().set('currentRows',"All");
                                        // .up('dialog').getView().getViewModel();                
                                        var data = Ext.StoreManager.lookup(el.popupStore()).getProxy().data;
                                        if(field.getValue()==''||field.getValue()==' '){
                                            Ext.StoreManager.lookup(el.popupStore()).load();
                                        }else{
                                            // akuu = Ext.StoreManager.lookup(el.popupStore());
                                            var keys = Object.keys(data[0]);
                                            var matches = [];
                                            for(let i=0;i<data.length;i++){
                                                var row=data[i];
                                                for(let y=0;y<keys.length;y++){
                                                    if( ( ((row[keys[y]]).toString()).toLowerCase()).includes ( (field.getValue()).toLowerCase() ) ){
                                                        matches.push(row);
                                                        break;
                                                    }
                                                }
                                            }
                                            // Ext.StoreManager.lookup(el.popupStore()).read({filter:{all: field.getValue()}});
                                            Ext.StoreManager.lookup(el.popupStore()).setData(matches);
                                        }
                                        
                                    }
                                }
                            }]
                        },
                        columns: gridColumns,  
                        // height: "100%",
                        layout: 'fit',
                        fullscreen: true
                    
                }
            ]
        });
        dialog.show();
    },
    resetField:function(el,currentEl){        
        el.up("container").getItems().items[0].reset();
        el.up("container").getItems().items[1].reset();
    },
	logoutConfirmation: function () {
        Ext.Msg.show({
            title: 'Logout',
            message: 'Are you sure?',
            buttons: [
                { text:'Cancel' , style:'text-align:center',padding:'1 1 1 1', ui:'decline', "iconCls": "x-fa fa-cancel", 'margin':'5 5 5 5',
                handler: function(){
                    Ext.Msg.alert('Batal', 'Batal Disimpan.', Ext.emptyFn);
                }
            },
                { text:'Logout' , style:'text-align:center',padding:'1 1 1 1', ui:'confirm', "iconCls": "x-fa fa-ok", 'margin':'5 5 5 5',
                handler: function(){
                    // Ext.Msg.alert('Batal', 'Batal Disimpan.', Ext.emptyFn);
                    Ext.Msg.hide();
                    sessionStorage.setItem('wesLogin',undefined);
                    Ext.Viewport.remove(0);					
                    Ext.Viewport.add([{xtype:'loginview'}]);	
                }}
            ],
            // multiLine: true,
            // prompt : { maxlength : 180, autocapitalize : true },
        });
    }
});