Ext.define('QL.DatatablePopupMulti',{
    extend: 'Ext.Dialog',
    xtype: 'QLDatatablePopupMulti',    
    alias: 'widget.QLDatatablePopupMulti',
    // maximizable: true,
    modal:true,
    minWidth:999,
    minHeight:450,
    scrollable:true,
    closable:true,
    ordering:false,
    customApi:false,
    configParams:{},
    initialize:function(){},
    viewModel: {
        data: {
            gridCurrentPage: "1",
            currentRows : 25,
            disabledPrev: true,
            disabledNext: false,
            totalPages:1,
            totalTerpilih:0
        },
        stores:{
            terpilih:{data:[]}
        }
    },
    controller: new Ext.app.ViewController(),
    items:[{
        afterRender:function(){
            let parentUppest = this.up(); //DATATABLEPOPUP
            let paramPagination = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['paginate'];
            let paramOrderBy = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['order_by'];
            let paramOrderType = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['order_type'];
            let pageNumber = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['current_page'];
            let page = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['page'];
            let pageEnd = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['page_end'];
            let filterField = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['filter_fields'];
            let filterText = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['filter_text'];
            parentUppest.configParams = {
                pagePaginate : paramPagination,
                pageNumber   : pageNumber,
                page   : page,
                pageEnd      : pageEnd,
                orderBy      : paramOrderBy,
                orderType    : paramOrderType,
                filterField  : filterField,
                filterText   : filterText
            }
            var storeUp = parentUppest.store.proxy;           
            if(parentUppest.ordering){            
                storeUp.parameters[parentUppest.configParams['orderBy']]=(storeUp.parameters[parentUppest.configParams['orderBy']]!=null && storeUp.parameters[parentUppest.configParams['orderBy']]!=undefined)?storeUp.parameters[parentUppest.configParams['orderBy']]:"id";
                storeUp.parameters[parentUppest.configParams['orderType']]=(storeUp.parameters[parentUppest.configParams['orderType']]!=null && storeUp.parameters[parentUppest.configParams['orderType']]!=undefined)?storeUp.parameters[parentUppest.configParams['orderType']]:"desc";    
            } 
            let viewModel = parentUppest.getViewModel();
            // let desiredPagination = storeUp.parameters[paramPagination]!==undefined?(storeUp.parameters[paramPagination]>50?50:storeUp.parameters[paramPagination]):(storeUp.parameters[paramPagination]<25?25);
            let desiredPagination = 50;
            storeUp.parameters[paramPagination] = desiredPagination;
            viewModel.set("currentRows", desiredPagination);
            Object.assign(storeUp, api.getData(storeUp,parentUppest.customApi) ); 
            storeUp.reader['transform'] = function(_responsedata){
                let lastPage = parseInt(_responsedata[parentUppest.configParams['pageEnd']]);
                let tombolPaginationNumber = [];
                let currentPage = parseInt( (parseInt(_responsedata[parentUppest.configParams['pageNumber']])-1)/10)*10;
                for(let i=1;i<=(lastPage>10?10:lastPage);i++){
                    if(currentPage+i>lastPage){
                        break;
                    }
                    tombolPaginationNumber.push(
                    {
                        xtype:'button',
                        text :   (currentPage+i).toString(),
                        ui:'back',
                        margin:'2 1 2 1',
                        pressed: parseInt(_responsedata[parentUppest.configParams['pageNumber']])==(currentPage+i)?true:false,
                        disabled:parseInt(_responsedata[parentUppest.configParams['pageNumber']])==(currentPage+i)?true:false,
                        handler: 'pressPage'
                    });
                }
                viewModel.set("gridCurrentPage",(_responsedata[parentUppest.configParams['pageNumber']]).toString() );
                // viewModel.set("currentRows", parseInt(_responsedata[parentUppest.configParams['pageNumber']])==parseInt(_responsedata[parentUppest.configParams['pageEnd']])?"All":_responsedata[parentUppest.configParams['pagePaginate']]  );
                viewModel.set("disabledPrev",parseInt(_responsedata[parentUppest.configParams['pageNumber']])==1?true:false);
                viewModel.set("disabledNext",parseInt(_responsedata[parentUppest.configParams['pageNumber']])==lastPage?true:false);
                parentUppest.getBbar().getItems().items[2].setItems(tombolPaginationNumber);
                let existing = parentUppest['storeId']!==undefined?getStoreData(parentUppest.storeId):[];
                let primaryKeys = parentUppest['primaryKeys'];
                let primaryKeysOnly=Object.keys(primaryKeys===undefined?{}:primaryKeys);
                let dataTerpilihObj = parentUppest.getViewModel().getStore('terpilih').getData().items;
                if(primaryKeysOnly.length>0){
                    existing.forEach( (element,i) => {
                        let newData ={};
                        primaryKeysOnly.forEach(mykey=>{
                            newData[mykey] = element[ primaryKeys[mykey] ];
                        });
                        existing[i]=newData;
                    });
                    let keys= Object.keys(existing[0]===undefined?[]:existing[0]);
                    _responsedata[api.rootProperty] =  _responsedata[api.rootProperty].filter( (dt,i)=>{
                        let unique=true;
                        existing.forEach(existdata=>{
                            let detected=0;
                            keys.forEach(function(key){
                                if(existdata[key] == dt[key] ){
                                    detected++;
                                }
                            });
                            if(keys.length==detected){
                                unique=false;
                            }
                        });
                        if(unique===true){ 
                            if(parentUppest['transform']!==undefined && typeof(parentUppest['transform'])=='function'){
                                return parentUppest.transform(dt);
                            }else{
                                return dt; 
                            }
                        }
                    });
                }else{
                    if(parentUppest['transform']!==undefined && typeof(parentUppest['transform'])=='function'){
                        for(let iterasi in _responsedata[api.rootProperty]){
                            _responsedata[api.rootProperty][iterasi] = parentUppest.transform(_responsedata[api.rootProperty][iterasi]);
                        };
                    }
                }
                if(dataTerpilihObj.length>0){                    
                    let dataTerpilih=[];
                    dataTerpilihObj.forEach(dt=>{
                        delete dt.data._tercheck;
                        dataTerpilih.push(dt.data);
                    });
                    for(let iterasi in _responsedata[api.rootProperty]){
                        for(i in dataTerpilih){
                            if(Ext.encode(dataTerpilih[i]) === Ext.encode(_responsedata[api.rootProperty][iterasi]) ){
                                _responsedata[api.rootProperty][iterasi]["_tercheck"]=true;
                                continue;
                            }
                        }
                    };
                }
                return _responsedata;
            }
            let storeId = storeUp.model;
            this["apiModel"] = storeUp.model;
            delete storeUp.model;
            let grid = this; 
            grid.setHeight(this.up().gridHeight);
            this.setStore({
                storeId: storeId,
                proxy: storeUp,
                listeners:{
                    load : function( el, records, successful, operation, eOpts ) {
                        let totalRows= el.totalCount;
                        let pages = totalRows%viewModel.get("currentRows")==0?totalRows/viewModel.get("currentRows"):   parseInt(totalRows/viewModel.get("currentRows")+1); 
                        viewModel.set("totalPages",pages);
                    },
                    datachanged:function(el){
                        try{
                            let ids=[];
                            let storeTemp=grid.up().getViewModel().getStore('terpilih');
                            let dataTemp = storeTemp.getData().items;                        
                            dataTemp.forEach(function (dt) {
                                ids.push(dt.data['id']);
                            });
                            let items  = el.getData().items;
                            items.forEach(function (record) {
                                if(record.data['_tercheck'] && !ids.includes(record.data['id'])){
                                    storeTemp.add(record);
                                }else if(!record.data['_tercheck'] && ids.includes(record.data['id'])){
                                    storeTemp.remove(record);
                                };
                            });
                            grid.up().getViewModel().set('totalTerpilih',storeTemp.getData().items.length);
                        }catch(e){}
                    }
                }
            });
            var columns = this.up().columns;
            if(['no','number','nomor'].includes(columns[0].text.toLowerCase())){
                columns[0]={
                    text : columns[0].text,
                    xtype:"gridcolumn",
                    dataIndex:"id",
                    align:"center",
                    width:columns[0].width,
                    // cell:{
                    //     xtype:"rownumberercell"
                    // },
                    renderer:function(value,model){
                        var data = model.store.getData().items;
                        var tambahan = parseInt(  viewModel.get("currentRows")=="All"?0:viewModel.get("currentRows") ) * ( parseInt( viewModel.get("gridCurrentPage") )-1 );
                        return (data.findIndex(x => x.internalId === model.internalId) +1) + tambahan  ;
                    }
                }
            }
            if(['action','Action'].includes(columns[columns.length-1].text.toLowerCase())){
                columns[columns.length-1]={
                    xtype       : 'checkcolumn',
                    align       :columns[columns.length-1].align,
                    headerCheckbox : true,
                    dataIndex   : '_tercheck',
                    text    : columns[columns.length-1].text,
                    width   : columns[columns.length-1].width,
                    listeners:{
                        checkchange:function( el, rowIndex, checked, record, e){
                            if( el.getValue() ){
                                grid.up().getViewModel().getStore('terpilih').add(record);
                            }else{
                                grid.up().getViewModel().getStore('terpilih').remove(record);
                            }
                        },
                        headercheckchange:function(el){
                            // el.getAllChecked;
                            // console.log(el)
                        }
                    }
                };
            }
            columns=columns.filter(function(col){
                if(col['dataIndex']!=null && col['dataIndex']!=undefined){
                    col.dataIndex=col.dataIndex.toLowerCase();
                }
                return col;
            });
            this.setColumns(columns);
            this.getStore().load();
            if(parentUppest["editCell"]!==undefined && parentUppest["editCell"]){
                this.setPlugins({
                    gridexporter: true,
                    gridcellediting: {
                        selectOnEdit: true
                    }
                });
                this.setSelectable({
                    rows: false,
                    cells: true
                });
                this.setListeners({
                    complete:function(editor,value,ori,other){
                        let record=editor.up().getRecord();
                        let items = grid.up().getViewModel().getStore('terpilih').getData().items;
                        for(i in items){
                            if(items[i].getId()==record.getId()){
                                items[i].set(record.data);
                                break;
                            }
                        }
                    }
                });
            }
        },
        xtype: 'grid',
        cls: 'goes-data-table',
        plugins:{            
            gridexporter: true,
        },
        itemConfig:{
            viewModel:true
        },  
        infinite:true, 
        rowLines:true,
        columnLines:true,
        autoComplete:false,
        title: ' ',
        scrollable:true,
        header: false,
        viewModel:true,
        store:{
            data:[]
        },
        titleBar: {
            shadow: true,
            maxHeight: 42,
            items: [{
                align: 'left',
                xtype: 'button',
                bind:{
                    text: '{currentRows} Rows',
                },
                stretchMenu: true,
                cls: 'go-table--sort',
                menu: {
                    indented: false,
                    items: [{
                        text: '25 Rows',
                        value:25,
                        handler: 'maxRows'
                    },{
                        text: '50 Rows',
                        value:50,
                        handler: 'maxRows'
                    },{
                        text: '100 Rows',
                        value:100,
                        handler: 'maxRows'
                    },{
                        text: 'All Rows',
                        value:'All',
                        handler: 'maxRows'
                    }]
                }
            },{
                xtype:'searchfield',
                placeholder: 'search',
                ui:'solo',
                style:{
                    "margin-top":"16px",
                    "max-height":"34px",
                    "max-width": "150px",
                    "margin-right": "10px"
                },
                cls:'go-table--search',
                align: 'right',
                listeners: {
                    buffer: 500,
                    change: function(el,value){
                        var panel = el.up("panel");
                        var gridStore = panel.down("grid").getStore();
                        var viewModel = panel.getViewModel();
                        let koloms=panel.down("grid").getColumns();
                        var datakolom = "";
                        koloms.forEach(function(dt){
                            datakolom+= (dt.getDataIndex()===null||dt.getDataIndex()=="id"?"":(dt.getDataIndex()+","));
                        });
                        let fixedkolom=datakolom.substring(0, datakolom.length - 1);
                        viewModel.set("gridCurrentPage","1");
                        // gridStore.getProxy().setExtraParam(panel.configParams["pagePaginate"],999);
                        if(value===''||value==' '){
                            gridStore.getProxy().setExtraParam(panel.configParams["filterField"],null);
                            gridStore.getProxy().setExtraParam(panel.configParams["filterText"],null);
                            gridStore.load();
                        }else{
                            gridStore.getProxy().setExtraParam(panel.configParams["filterField"],fixedkolom);
                            gridStore.getProxy().setExtraParam(panel.configParams["filterText"],value);
                            gridStore.load();                            
                        }
                        
                    }
                }
            }]
        },
        columns: []    
    }],
    bbar:[{
        xtype:'button',
            ui:'action',
            docked:"right",
            bind:{
                text:'Add [{totalTerpilih}]',
                disabled:'{totalTerpilih<1}'
            },
            style:{
                "margin-top":"30px",
                "height":"50px",
                "width": "95px",
                "margin-right": "10px"
            },
            handler: function(el){
                let dialog = el.up("dialog");
                var data = dialog.getViewModel().getStore('terpilih').getData().items;
                if(typeof(dialog['action'])=='function'){
                    let action=dialog['action'];
                    action(Ext.getCmp(dialog.storeId),getStoreData(data));
                }else{
                    Ext.getStore(dialog.storeId).add(data);
                    try{
                        var tabpanel = Ext.getCmp(dialog.storeId).up("tabpanel");
                        tabpanel.getItems().items.forEach(function(children){
                            let before = children['lastHeight'];
                            if(before===undefined){
                                before = (children.renderElement.dom.offsetHeight);
                            }
                            let after=before;
                            let grid = children.down("grid");
                            if(grid!==null){
                                if(dialog.storeId == grid.getStore().getStoreId()){
                                    after-= isNaN(grid.getHeight())?0:grid.getHeight();
                                    let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                                    let gridHeightBody   = 0;
                                    let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                                    Array.from(rows).forEach(function(row){
                                        gridHeightBody+=row.offsetHeight;
                                    });
                                    grid.setHeight(gridHeightHeader+gridHeightBody+40);
                                    after+=grid.getHeight();
                                    children['lastHeight'] = after;
                                }
                            }
                        });
                        tabpanel.afterRender();
                    }catch(e){}
                };
                dialog.destroy();
            }
        },{
            xtype:'button',
            ui:'action',
            text:'prev',
            margin:'30 13 30 0',
            bind: {
                disabled:"{disabledPrev}"
            },
            handler: 'pressPrev'
        },{
            xtype:'panel',
            items:[],
        },{
            xtype:'button',
            ui:'action',
            text:'next',
            margin:'0 0 0 3',
            bind: {
                disabled:"{disabledNext}"
            },
            handler: 'pressNext'
        }
        // ,{
        //     xtype:'component',
        //     html:"banyak halaman"
        // }
    ]

});