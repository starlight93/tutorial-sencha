Ext.define('QL.DatatablePopupMultiV1',{
    extend: 'Ext.Dialog',
    xtype: 'QLDatatablePopupMultiV1',    
    alias: 'widget.QLDatatablePopupMultiV1',
    // maximizable: true,
    modal:true,
    minWidth:850,
    minHeight:400,
    scrollable:true,
    customApi:false,
    closable:true,
    initialize:function(){
        // console.log(this);        
    },
    controller : new Ext.app.ViewController(),
    viewModel: {
        data: {
            gridCurrentPage: "1",
            currentRows : 25,
            disabledPrev: true,
            disabledNext: false,
            totalPages:1
        },
        stores:{
            terpilih:{data:[]}
        }
    },
    items:[ {
        afterRender:function(){
            let parentUppest = this.up();
            var storeTujuan = Ext.data.StoreManager.lookup(this.up().storeTujuan);
            var storeUp = this.up().store.proxy;            
            storeUp.parameters["orderby"]=(storeUp.parameters["orderby"]!=null && storeUp.parameters["orderby"]!=undefined)?storeUp.parameters["orderby"]:"id";
            storeUp.parameters["ordertype"]=(storeUp.parameters["ordertype"]!=null && storeUp.parameters["ordertype"]!=undefined)?storeUp.parameters["ordertype"]:"desc";
            let viewModel = this.up().getViewModel();
            viewModel.set("currentRows", storeUp.parameters.paginate);
            Object.assign(storeUp, api.getData(storeUp,parentUppest.customApi) ); 
            storeUp.reader['transform'] = function(data) {
                let customData = parentUppest['customData'];
                let ids=[];
                let dataTelahTerpilih = storeTujuan.getData().items;                        
                dataTelahTerpilih.forEach(function (dt) {
                    ids.push(dt.data['id']);
                });
                
                let idsTemp=[];
                let dataTemp = grid.up().getViewModel().getStore('terpilih').getData().items;               
                dataTemp.forEach(function (dt) {
                    idsTemp.push(dt.data['id']);
                });
                data = data[api.rootProperty];
                var fixedData = data.filter(dt=>{
                    dt['_tercheck']=idsTemp.includes(dt['id'])?true:false;
                    if(customData!==undefined){
                        Object.keys(customData).forEach(function(key){
                            dt[key] = customData[key](dt);
                        });
                    }
                    return !(ids).includes(dt['id']);
                });
                
                return fixedData;
            }
            
            delete storeUp.model;
            let grid = this; 
            grid.setHeight(this.up().gridHeight);
            this.setStore({
                proxy:storeUp,
                listeners:{
                    load : function( el, records, successful, operation, eOpts ) {
                        let totalRows= el.totalCount;
                        let pages = totalRows%viewModel.get("currentRows")==0?totalRows/viewModel.get("currentRows"):   parseInt(totalRows/viewModel.get("currentRows")+1); 
                        viewModel.set("totalPages",pages);      
                        // console.log(pages);
                        // let rowLength = records.length==0?1:records.length;
                        // let newHeight = (rowLength*32)+50;
                        // grid.setHeight(newHeight);
                        // console.log(records);
                    },
                    datachanged:function(el){
                        let ids=[];
                        let storeTemp=grid.up().getViewModel().getStore('terpilih');
                        let dataTemp = storeTemp.getData().items;                        
                        dataTemp.forEach(function (dt) {
                            ids.push(dt.data['id']);
                        });
                        let items  = el.getData().items;
                        items.forEach(function (record) {
                            if(record.data['_tercheck'] && !ids.includes(record.data['id'])){
                                storeTemp.add(record);
                            }else if(!record.data['_tercheck'] && ids.includes(record.data['id'])){
                                storeTemp.remove(record);
                            };
                        });
                    }
                }
            });
            var columns = this.up().columns;
            if(['no','number','nomor'].includes(columns[0].text.toLowerCase())){
                columns[0]={
                    text : columns[0].text,
                    xtype:"gridcolumn",
                    dataIndex:"id",
                    align:"center",
                    width:columns[0].width,
                    // cell:{
                    //     xtype:"rownumberercell"
                    // },
                    renderer:function(value,model){
                        var data = model.store.getData().items;
                        return (data.findIndex(x => x.internalId === model.internalId) +1) + ((parseInt(viewModel.get("currentRows"))* (parseInt( viewModel.get("gridCurrentPage") )-1) )==NaN?0:(parseInt(viewModel.get("currentRows"))* (parseInt( viewModel.get("gridCurrentPage") )-1) ))  ;
                    }
                }
            }
            if(['action','Action'].includes(columns[columns.length-1].text.toLowerCase())){
                columns[columns.length-1]={
                    xtype       : 'checkcolumn',
                    align       :columns[columns.length-1].align,
                    headerCheckbox : true,
                    dataIndex   : '_tercheck',
                    text    : columns[columns.length-1].text,
                    width   : columns[columns.length-1].width,
                    listeners:{
                        checkchange:function( el, rowIndex, checked, record, e){
                            if( el.getValue() ){
                                grid.up().getViewModel().getStore('terpilih').add(record);
                            }else{
                                grid.up().getViewModel().getStore('terpilih').remove(record);
                            }
                            // console.log( el.up("grid").getViewModel().getStore('terpilih').getData().items );
                        },
                        headercheckchange:function(el){
                            // el.getAllChecked;
                            // console.log(el)
                        }
                    }
                };
            }
            columns=columns.filter(function(col){
                if(col['dataIndex']!=null && col['dataIndex']!=undefined){
                    col.dataIndex=col.dataIndex.toLowerCase();
                }
                return col;
            });
            this.setColumns(columns);       
            this.getStore().load();         
        },
        xtype: 'grid',
        itemConfig:{
            viewModel:true
        },  
        infinite:true, 
        rowLines:true,
        columnLines:true,
        autoComplete:false,
        title: ' ',
        scrollable:true,
        header: false,
        viewModel:true,
        store:{
            data:[]
        },
        // bbar:[
        //     {
        //         xtype:'displayfield', text:'aku'
        //     }
        // ],
        titleBar: {
            shadow: true,
            maxHeight: 42,
            items: [{
                align: 'left',
                xtype: 'button',
                bind:{
                    text: '{currentRows} Rows',
                },
                stretchMenu: true,
                cls: 'go-table--sort',
                // arrow: true,
                // style:"background-color:white",
                menu: {
                    indented: false,
                    items: [{
                        text: '10 Rows',
                        value:10,
                        handler: 'maxRows'
                    },{
                        text: '25 Rows',
                        value:25,
                        handler: 'maxRows'
                    },{
                        text: '50 Rows',
                        value:50,
                        handler: 'maxRows'
                    },{
                        text: '100 Rows',
                        value:100,
                        handler: 'maxRows'
                    },{
                        text: 'All Rows',
                        value:'All',
                        handler: 'maxRows'
                    }]
                }
            },{
                xtype:'searchfield',
                placeholder: 'search',
                ui:'solo',  
                style:{
                    "margin-top":"16px",
                    "max-height":"34px",
                    "max-width": "150px",
                    "margin-right": "10px"
                },
                cls:'go-table--search',
                align: 'right',
                listeners: {
                    buffer: 500,
                    change: function(el,value){
                        var panel = el.up("dialog");
                        var gridStore = panel.down("grid").getStore();
                        var viewModel = panel.getViewModel();
                        viewModel.set("gridCurrentPage","1");
                        // gridStore.getProxy().setExtraParam("paginate",1000);
                        if(value===''||value==' '){
                            gridStore.getProxy().setExtraParam("filter",null);
                            gridStore.load();
                        }else{
                            gridStore.getProxy().setExtraParam("filter",value);
                            gridStore.load();                            
                        }
                        
                    }
                }
            }]
        },
        columns: []
        
        // infinite:true,
        // layout: 'fit',
        // fullscreen: true,
    
    }],
    bbar:[{
            xtype:'button',
            ui:'action',
            docked:"right",
            text:'OK',
            style:{
                "margin-top":"30px",
                "height":"50px",
                "width": "50px",
                "margin-right": "10px"
            },
            handler: function(el){
                var data = el.up("dialog").getViewModel().getStore('terpilih').getData().items;
                Ext.StoreManager.lookup(Ext.data.StoreManager.lookup(el.up("dialog").storeTujuan)).add(data);
                el.up("dialog").destroy();
            }
        },
        {
            xtype:'button',
            ui:'action',
            text:'prev',
            margin:'30 15 30 0',
            bind: {
                disabled:"{disabledPrev}"
            },
            handler: 'pressPrev'
        },{
            xtype:'panel',
            // layout:'hbox',
            items:[
                {
                    xtype:'button',
                    text:'1',
                    ui:'back',
                    margin:'2 2 2 2',
                    pressed: true,
                    handler: 'pressPage'
                },{
                    xtype:'button',
                    text:'2',
                    ui:'back',
                    margin:'2 2 2 2',
                    pressed: false,
                    handler: 'pressPage'
                },{
                    xtype:'button',
                    text:'3',
                    ui:'back',
                    margin:'2 2 2 2',
                    pressed: false,
                    handler: 'pressPage'
                }
            ]
        },                
        {
            xtype:'button',
            ui:'action',
            text:'next',
            margin:'5 0 0 5',
            bind: {
                disabled:"{disabledNext}"
            },
            handler: 'pressNext'
        },
    ]

});