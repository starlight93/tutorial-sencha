
Ext.define('QL.DatatableV1',{
    extend: 'Ext.Panel',
    xtype: 'QLDatatableV1',
    alias: 'widget.QLDatatableV1',
    exportName: "Data",
    cls: 'goes-data-table',
    ordering:false,
    customApi:false,
    layout:"fit",
    fullscreen:true,
    storeAlias:null,
    groupField:"",
    configParams:{},
    initialize:function(){},
    controller : new Ext.app.ViewController(),
    viewModel: {
        data: {
            gridCurrentPage: "1",
            currentRows : 25,
            disabledPrev: true,
            disabledNext: false,
            totalPages:1
        }
    },
    items:[{
        afterRender:function(){
            let parentUppest = this.up();
            parentUppest.setStyle({
                "margin-top":"45px"
            })
            let paramPagination = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['paginate'];
            let paramOrderBy = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['order_by'];
            let paramOrderType = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['order_type'];
            let pageNumber = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['current_page'];
            let page = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['page'];
            let pageEnd = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['page_end'];
            let filterField = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['filter_fields'];
            let filterText = qlconfig[`backend_${qlconfig.development?'development':'production'}_parameters`]['filter_text'];
            parentUppest.configParams = {
                pagePaginate : paramPagination,
                pageNumber   : pageNumber,
                page   : page,
                pageEnd      : pageEnd,
                orderBy      : paramOrderBy,
                orderType    : paramOrderType,
                filterField  : filterField,
                filterText   : filterText
            }
            var storeUp = parentUppest.store.proxy;
            if(parentUppest.ordering){
                storeUp.parameters[parentUppest.configParams['orderBy']]=(storeUp.parameters[parentUppest.configParams['orderBy']]!=null && storeUp.parameters[parentUppest.configParams['orderBy']]!=undefined)?storeUp.parameters[parentUppest.configParams['orderBy']]:"id";
                storeUp.parameters[parentUppest.configParams['orderType']]=(storeUp.parameters[parentUppest.configParams['orderType']]!=null && storeUp.parameters[parentUppest.configParams['orderType']]!=undefined)?storeUp.parameters[parentUppest.configParams['orderType']]:"desc";
            }
            let viewModel = parentUppest.getViewModel();
            // let desiredPagination = storeUp.parameters[paramPagination]!==undefined?(storeUp.parameters[paramPagination]>50?50:storeUp.parameters[paramPagination]):(storeUp.parameters[paramPagination]<25?25);
            let desiredPagination = 50;
            storeUp.parameters['origin_type']='front_datatable';
            storeUp.parameters['origin']='original_page';
            storeUp.parameters[paramPagination] = desiredPagination;
            viewModel.set("currentRows", desiredPagination);
            Object.assign(storeUp, api.getData(storeUp,parentUppest.customApi) );
            storeUp.reader['transform'] = function(_responsedata){
                let lastPage = parseInt(_responsedata[parentUppest.configParams['pageEnd']]);
                let tombolPaginationNumber = [];
                let currentPage = parseInt( (parseInt(_responsedata[parentUppest.configParams['pageNumber']])-1)/10)*10;
                for(let i=1;i<=(lastPage>10?10:lastPage);i++){
                    if(currentPage+i>lastPage){
                        break;
                    }
                    tombolPaginationNumber.push(
                    {
                        xtype:'button',
                        text :   (currentPage+i).toString(),
                        ui:'back',
                        margin:'2 1 2 1',
                        pressed: parseInt(_responsedata[parentUppest.configParams['pageNumber']])==(currentPage+i)?true:false,
                        disabled:parseInt(_responsedata[parentUppest.configParams['pageNumber']])==(currentPage+i)?true:false,
                        handler: 'pressPage'
                    });
                }
                viewModel.set("gridCurrentPage",(_responsedata[parentUppest.configParams['pageNumber']]).toString() );
                // viewModel.set("currentRows", parseInt(_responsedata[parentUppest.configParams['pageNumber']])==parseInt(_responsedata[parentUppest.configParams['pageEnd']])?"All":_responsedata[parentUppest.configParams['pagePaginate']]  );
                viewModel.set("disabledPrev",parseInt(_responsedata[parentUppest.configParams['pageNumber']])==1?true:false);
                viewModel.set("disabledNext",parseInt(_responsedata[parentUppest.configParams['pageNumber']])==lastPage?true:false);
                parentUppest.getBbar().getItems().items[1].setItems(tombolPaginationNumber);
                if(_responsedata[api.rootProperty].length>0 && _responsedata[api.rootProperty][0]["meta_create"]!==undefined){
                    var nav = parentUppest.up("navigationview");
                    if(nav!=undefined){
                        let buttonCreate = nav.down('button');
                        if(buttonCreate.getText()!==null && ((buttonCreate.getText()).toLowerCase()).includes('create')){
                            buttonCreate.setHidden(!_responsedata[api.rootProperty][0]["meta_create"]);
                        }
                    }
                }
                if(_responsedata["metaScript"]!==undefined && _responsedata['metaScript']!==null){

                    try{
                        var fn = new Function(atob(atob(_responsedata["metaScript"])));
                        fn();
                    }catch(e){}
                }
                return _responsedata;
            }
            let storeId = parentUppest.storeAlias!==null?parentUppest.storeAlias:storeUp.model;
            this["apiModel"] = storeUp.model;
            delete storeUp.model;
            let grid = this;
            // grid.setHeight("100%");
            // grid.setHeight( parseInt(this.up().gridHeight)>parseInt("430px")?this.up().gridHeight:"430px");
            this.setStore({
                storeId:storeId,
                remoteSort:storeUp['remoteSort']?true:false,
                proxy:storeUp,
                listeners:{
                    load : function( el, records, successful, operation, eOpts ) {
                        let totalRows= el.totalCount;
                        let pages = totalRows%viewModel.get("currentRows")==0?totalRows/viewModel.get("currentRows"):   parseInt(totalRows/viewModel.get("currentRows")+1);
                        viewModel.set("totalPages",pages);
                    },
                    metachange:function(el, meta){
                        console.log(el,meta);
                    },
                    beforeSort:function(currentStore,sorters){
                        if(qlconfig.backend_orderby!==undefined && typeof(qlconfig.backend_orderby)=='function'){
                            let functionSort = qlconfig.backend_orderby;
                            functionSort(currentStore, currentStore.getSorters().items);
                        }
                    }
                }
            });
            var columns = this.up().columns;
            if(['no','number','nomor'].includes(columns[0].text.toLowerCase())){
                columns[0]={
                    text : columns[0].text,
                    xtype:"gridcolumn",
                    dataIndex:"id",
                    dataIndexReal:'aloha',
                    align:"center",
                    // filter: {
                    // type: 'string',
                    //     menu: {
                    //         items: {
                    //             like: {
                    //                 xtype:"selectfield",
                    //                 options:[{
                    //                     value:1,text:'satu'
                    //                 },{
                    //                     value:3,text:'tiga'
                    //                 }],
                    //                 afterRender:function(){
                    //                     this.setPlaceholder(this.up('gridcolumn').getText());
                    //                 }
                    //             }
                    //         }
                    //     }
                    // },
                    width:columns[0].width,
                    renderer:function(value,model){
                        var data = model.store.getData().items;
                        var tambahan = parseInt(  viewModel.get("currentRows")=="All"?0:viewModel.get("currentRows") ) * ( parseInt( viewModel.get("gridCurrentPage") )-1 );
                        return (data.findIndex(x => x.internalId === model.internalId) +1) + tambahan  ;
                    }
                }
            }
            columns=columns.filter(function(col){
                col['filter'] = col['filter']===undefined?'string':col['filter'];
                if(col['dataIndex']!=null && col['dataIndex']!=undefined){
                    col.dataIndex=col.dataIndex.toLowerCase();
                }
                return col;
            });
            this.setColumns(columns);
            if(parentUppest["groupField"]!=""){
                this.getStore().setGroupField(parentUppest["groupField"]);
                this.setGrouped(true);
            }
            this.getStore().load();
        },
        xtype: 'grid',
        plugins:{
            gridexporter: true,
            gridfilters: true
        },
        itemConfig:{
            viewModel:true
        },
        infinite:true,
        rowLines:true,
        multiColumnSort:true,
        columnLines:true,
        autoComplete:false,
        title: ' ',
        scrollable:true,
        header: false,
        viewModel:true,
        store:{
            data:[]
        },
        titleBar: {
            shadow: true,
            maxHeight: 42,
            items: [{
                align: 'left',
                xtype: 'button',
                bind:{
                    text: '{currentRows} Rows',
                },
                stretchMenu: true,
                cls: 'go-table--sort',
                menu: {
                    indented: false,
                    items: [{
                        text: '25 Rows',
                        value:25,
                        handler: 'maxRows'
                    },{
                        text: '50 Rows',
                        value:50,
                        handler: 'maxRows'
                    },{
                        text: '100 Rows',
                        value:100,
                        handler: 'maxRows'
                    },{
                        text: 'All Rows',
                        value:'All',
                        handler: 'maxRows'
                    }]
                }
            },{
                xtype:'searchfield',
                placeholder: 'search',
                ui:'solo',
                style:{
                    "margin-top":"16px",
                    "margin-bottom":"16px",
                    "max-height":"34px",
                    "max-width": "500px",
                    "width": "350px",
                    "margin-right": "10px"
                },
                cls:'go-table--search',
                align: 'right',
                listeners: {
                    buffer: 500,
                    change: function(el,value){
                        var panel = el.up("panel");
                        var gridStore = panel.down("grid").getStore();
                        var viewModel = panel.getViewModel();
                        let koloms=panel.down("grid").getColumns();
                        var datakolom = "";
                        koloms.forEach(function(dt){
                            if(dt['dataIndexReal']!==undefined){
                                datakolom+= dt['dataIndexReal']+",";
                            }else{
                                datakolom+= (dt.getDataIndex()===null||dt.getDataIndex()=="id"?"":(dt.getDataIndex()+","));
                            }
                        });
                        let fixedkolom=datakolom.substring(0, datakolom.length - 1);
                        viewModel.set("gridCurrentPage","1");
                        // gridStore.getProxy().setExtraParam(panel.configParams["pagePaginate"],999);
                        if(value===''||value==' '){
                            gridStore.getProxy().setExtraParam(panel.configParams["filterField"],null);
                            gridStore.getProxy().setExtraParam(panel.configParams["filterText"],null);
                            gridStore.load();
                        }else{
                            gridStore.getProxy().setExtraParam(panel.configParams["filterField"],fixedkolom);
                            gridStore.getProxy().setExtraParam(panel.configParams["filterText"],value);
                            gridStore.load();
                        }

                    }
                }
            }]
        },
        columns: []
    }],
    bbar:[{
            xtype:'button',
            ui:'action',
            text:'prev',
            margin:'30 13 30 0',
            bind: {
                disabled:"{disabledPrev}"
            },
            handler: 'pressPrev'
        },{
            xtype:'panel',
            items:[],
        },{
            xtype:'button',
            ui:'action',
            text:'next',
            margin:'0 0 0 3',
            bind: {
                disabled:"{disabledNext}"
            },
            handler: 'pressNext'
        },{
            xtype:'button',
            ui:'action',
            docked:"right",
            text:'',
            iconCls: "fas fa-sync",
            tooltip: "refresh",
            style:{
                "margin-top":"30px",
                "height":"50px",
                "width": "50px",
                "margin-right": "10px"
            },
            handler: function(el){
                let grid = el.up("panel").down("grid");
                if(qlconfig.backend_orderby!==undefined && typeof(qlconfig.backend_orderby)=='function'){
                    let functionSort = qlconfig.backend_orderby;
                    functionSort(grid.getStore());
                }else{
                    grid.getStore().load();
                }
            }
        },{
            xtype:'button',
            tooltip:"CSV",
            text:"CSV",
            docked:"right",
            style:{
                "margin-top":"30px",
                "height":"50px",
                "width": "50px",
                "margin-right": "10px"
            },
            handler: function(el){
               Ext.getCmp("loadingDialog").show();
               let grid = el.up("panel").down("grid");
            //    var dataList = grid.getStore().getData().items;
            //    if(dataList.length>0){
            //        let columns = Object.keys(dataList[1].data);
            //    }
               grid.saveDocumentAs({
                    type: 'csv',
                    title: (grid.apiModel).replace(/\//g,"_"),
                    fileName: (grid.apiModel).replace(/\//g,"_")+` (${HELPER.Date()}).csv`,
                    includeGroups: true,
                    includeSummary: true
                });
                Ext.getCmp("loadingDialog").hide();
            }
        },{
            xtype:'togglefield',
            tooltip:'Auto Refresh',
            docked:"right",
            cls:"QLtogglefield",
            style:{
                "margin-top":"16px",
                "margin-bottom":"16px",
                "margin-right":"12px",
            },
            listeners:{
                change:function(el,val){
                    if(val){
                        Ext.toast({message:"Auto reload has been activated on this page!", timeout:1200,centered:true,alignment:'c-c'});
                    }else{
                        Ext.toast({message:"Auto reload is now off!", timeout:1200,centered:true,alignment:'c-c'});
                    };
                }
            }
        },
        // ,{
        //     xtype:'component',
        //     html:"banyak halaman"
        // }
    ]

});
Ext.define('QL.DatatableV1Dev',{
    extend: 'QL.DatatableV1',
    xtype: 'QLDatatableV1Dev',
    alias: 'widget.QLDatatableV1Dev'
});