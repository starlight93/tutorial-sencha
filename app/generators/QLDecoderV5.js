Ext.define('QL.DecoderV5',
function(){
    var self;
    return {
    name: 'QLDecoderV5',
    config : {
        type            : "header",
        data            : {},
        controller      : 'qlcontroller',
        formName        : null,
        rowSpaces       : [],
        indexJSON       : 0,
        title           : null,
        id              : null,
        rowSpaces       : [],
        newHbox         : [],
        isOnline        : false,
        cols            : 2,
        defaultWidth    : '100%',
        labelWidth      : '100%',
        convertedFields : [],
        properties      : [],
        buttons         : [],
        buttonAlign     : "center",
        grid            : null,
        margin          : "0 0 0 0",
        aliases         : []
    },
    constructor : function(config){
        self=this;
        self.initConfig(config);
        return self.get();
    },
    defaultSelect : (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                text: arrayDefault[i],
                value: arrayDefault[i]
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;

        return valuesAkhir;
    },
    defaultRadio : (valueDefault, checkedDefault=null)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }

        try{
            var arrayDefault = valueDefault.split(",");
            var calonValues = [];
            for(let i=0;i<arrayDefault.length ;i++){
                var candidateOption ={
                    label: arrayDefault[i],
                    value: arrayDefault[i],
                    // ui:'solo'
                    checked: checkedDefault !=null && checkedDefault.toLowerCase()==arrayDefault[i]?true:false
                };

                calonValues.push(candidateOption);
            }
            var valuesAkhir = calonValues;

            return valuesAkhir;
        }catch(e){ return [];}
    },
    defaultCheckBox : (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        try{
            var arrayDefault = valueDefault.split(",");
            var calonValues = [];
            for(let i=0;i<arrayDefault.length ;i++){
                var candidateOption ={
                    label: arrayDefault[i],
                    // name: arrayDefault[i],
                    value: arrayDefault[i],
                };
                calonValues.push(candidateOption);
            }
            var valuesAkhir = calonValues;
            return valuesAkhir;
        }catch(e){ return [];}
    },
    defaultCombo: (valueDefault)=>{
        if(valueDefault==null){
            valueDefault="sample Value1,sample Value2";
        }
        var arrayDefault = valueDefault.split(",");
        var calonValues = [];
        for(let i=0;i<arrayDefault.length ;i++){
            var candidateOption ={
                text: Ext.String.capitalize(arrayDefault[i]),
                id: arrayDefault[i],
            };
            calonValues.push(candidateOption);
        }
        var valuesAkhir = calonValues;
        return valuesAkhir;
    },
    kamusElement:(column,isChild=false)=>{
        let perluTextHidden=true;
        if(column['name']!=null && column['name']!=undefined){
            column.name=column.name.toLowerCase();
        }
        if(column.label==""||column.label==null||column.label==undefined){
            column.label="";
        }
        var defaultWidth = isChild?"100%":self.getDefaultWidth();
        if(column.withlabel != undefined && column.withlabel != null ){
            column.label = column.withlabel? column.label:"";
        }
        var convertMe = false;
        var elements;
        for(let index=0; index<self.getConvertedFields().length;index++){
            if( self.getConvertedFields()[index].id == column.id ){
                convertMe = true;
                elements = self.getConvertedFields()[index].fields;
                break;
            }
        }
        if(convertMe){
            return {
                    xtype:'container',
                    autoSize:true,
                    cls:'qlfield',
                    // padding: "1 1 1 1",
                    margin: "1 30 4 5", //TOP RIGHT BOTTOM LEFT
                    items:elements
            };
        }

        var data;
        if(["enum","select"].includes(column.type)){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data = {
                    name: column.name+"_id",
                    cls: "qlselectfield",
                    xtype: 'combobox',
                    editable:false,
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    // autoSelect:true,
                    // flex:1,
                    placeholder: "choose...",
                    // options: self.defaultSelect(column.value)

            };
            if(column.value!=null && column.value!=""){
                data["name"] = data["name"].replace("_id","");
                perluTextHidden=false;
                if( ( (column.value).toLowerCase() ).includes("store:") ){
                    data["store"] = column.value.replace("store:","");
                }else{
                    data["options"] = self.defaultSelect(column.value);
                    data["value"] = data.options[0].value;
                }
            }

        }else if(["enum_popup","popup"].includes(column.type) || column.name=='___'){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data ={
                    xtype: 'container',
                    cls: "qlpopup",
                    layout: 'hbox',
                    hidden: column.name=='___'?true:column.hidden,
                    label: column.label,
                    // width: isChild?parseInt(self.tempWidth-80)+"%":defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelAlign : 'left',
                    required:column.required,
                    style:{
                        // "margin-left" : "10px"
                    },
                    labelWrap:true,
                    items: [{
                        xtype : 'textfield',
                        cls : "qltextfield",
                        customtype : "popupfield",
                        readOnly : true,
                        autoComplete : false,
                        disabled : column.disabled,
                        required : column.required,
                        name : column.name,
                        flex: self.getCols()==1?null:1,
                        labelWidth : "100%",
                        width : `${ parseInt( isChild?self.tempWidth: parseInt(defaultWidth) ) - (isChild?0:self.getCols()==1?9:15)}%`,
                        label : column.label,
                        labelAlign : 'left',
                        labelWrap : true,
                        style   : {
                            "margin-bottom": "0px !important"
                        }
                    }, {
                        xtype: 'textfield',
                        hidden:true,
                        customtype:"hiddenpopupfield",
                        name: column.name+"_id",
                    },
                    {
                        xtype: 'button',
                        ui: 'confirm',
                        width:"33px",
                        iconCls: 'x-fa fa-search',
                        handler: function(el){
                            console.log('ahai');
                        },
                        cls: 'ql-btn--popup',
                        style: {
                            "margin-left":"2px"
                        }
                    },
                    {
                        xtype: 'button',
                        ui: 'alt decline',
                        title: ' reset',
                        width:"33px",
                        iconCls: 'x-fa fa-lg fa-eraser',
                        cls: 'ql-btn--popup',
                        style: {
                            "margin-left":"2px"
                        }
                    }
                ]

            };

        }else if(["enum_search","search"].includes(column.type)){
            if(column.name!==null){
                column.name = column.name.replace("_id","");
            }
            data ={
                    name: column.name+"_id",
                    xtype: 'combobox',
                    cls: "qlcombofield",
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    flex:1,
                    queryMode: 'local',
                    displayField: 'text',
                    valueField: 'id',
                    minChars:0,
                    anyMatch:true,
                    // autoSelect:true,
                    forceSelection:true,
                    placeholder: "search..."
                    // store: self.defaultCombo(column.value)

            };
            if(column.value!=null && column.value!=""){
                data["name"] = data["name"].replace("_id","");
                perluTextHidden=false;
            }
        }else if(["enum_radio","radio"].includes(column.type)){
            data ={
                xtype: 'radiogroup',
                cls: "qlradiogroupfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                cls: 'x-radio-group-alt',
                name: column.name,
                items: self.defaultRadio(column.value,column["default"]),
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
            };
            // if(data["default"]!==null){
            //     data["value"] = data["default"];
            // }
        }else if(["enum_checkbox","checkbox","check one","checkone","array"].includes(column.type)){
            data ={
                xtype:'container',
                hidden: column.hidden,
                layout: "hbox",
                width: isChild? self.tempWidth:defaultWidth,
                items:[{
                    xtype: 'checkboxfield',
                    cls: "qlcheckboxfield",
                    label: column.label,
                    labelAlign : 'left',
                    flex:1,
                    maxWidth: 165,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    cls: 'x-check-group-alt',
                    name: column.name,
                    items: self.defaultCheckBox(column.value),
                    layout: {
                        type: 'hbox',
                        align: 'middle'
                    },
                },{
                    xtype:'label',
                    html:`<span>${column.value=='null'?'':column.value}</span>`,
                    style:{
                        "margin-top":"6px"
                    }
                }]
            }
            // data["items"] = data.items[0].value;
        }else if(["enum_checkbox","checkbox","check many","checkmany","array"].includes(column.type)){
            data ={
                xtype: 'checkboxgroup',
                cls: "qlcheckboxgroupfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                cls: 'x-check-group-alt',
                name: column.name,
                items: self.defaultCheckBox(column.value),
                layout: {
                    type: 'hbox',
                    align: 'middle'
                },
            };
            // data["items"] = data.items[0].value;
        }else if(["check-labelright"].includes(column.type)){
            data={
                xtype : "container",
                hidden: column.hidden,
                width: isChild? self.tempWidth:defaultWidth,
                layout:"hbox",
                items : [{
                    xtype    : "checkboxfield",
                    style    : "margin-right:4px",
                    required : column.required,
                    readOnly : column.readonly,
                    disabled : column.disabled,
                    name     : column.name,
                    checked  : column.default=="true"||column.default===true?true:false,
                },{
                    xtype:"component",
                    html:`<div style='padding-top:5px'>${column.label}</div>`
                }],
                cls: "qlchecklabelright",
            }
        }else if(["file","upload","image","filefield","file one","files many"].includes(column.type)){
            data = {
                xtype:"containerfield",
                field:false,
                label: column.label,
                required:column.required,
                hidden: column.hidden,
                items:[
                    {
                        xtype:"hiddenfield",
                        name:column.name+"_url",
                        url:null
                    },
                    {
                        xtype   : "filefield",
                        width: `${ parseInt( (defaultWidth).replace("%","")) -20}%`,
                        name:column.name,
                        flex: self.getCols()==1?null:1,
                        readOnly: column.readonly,
                        disabled: column.disabled,
                        hidden: column.hidden,
                        required:column.required,
                        labelWidth: self.getLabelWidth(),
                        accept  : (column.type=='image')?"image":".gif, .jpg, .png, .jpeg, .bmp, .doc, .docx, .xls, .xlsx, .csv, .zip, .rar, .tar, .pdf",
                        maxSize : 10,
                        afterRender:function(){
                            this.getFileButton().setConfig({
                                iconCls: "fas fa-upload",
                                text : null,style:"margin-right:5px"
                            });
                        },
                        listeners:{
                            change : function(el,v){
                                if(el.getValue()===""||el.getValue()===null){return true}
                                if(el['maxSize']!==undefined && el.getFiles().length>0){
                                    let max = el['maxSize'] * 1000000;
                                    if(el.getFiles()[0].size>max){
                                        el.reset();
                                        Ext.Msg.alert("Disallowed!",`Sorry, max size must be less than ${el['maxSize']} MB`);
                                        el.setError(`Sorry, max size must be less than ${el['maxSize']} MB`);

                                        return false;
                                    }
                                }
                            }
                        },
                    },{
                        xtype:"button", iconCls:"fas fa-undo", ui:"alt decline", tooltip:"reset",
                        style:"margin-left:3px;margin-top:7px;max-height:25px;max-width:25px",
                        handler:function(el){
                            el.up("containerfield").down("filefield").reset();
                        }
                    },{
                        xtype:"button", iconCls:"fas fa-search-plus", tooltip:"preview",
                        style:"margin-left:3px;margin-top:7px;max-height:25px;max-width:25px",
                        handler:function(el){
                            if(el.up("containerfield").down("filefield").getFiles().length==0 && (el.up("containerfield").down("hiddenfield").getValue()===null||el.up("containerfield").down("hiddenfield").getValue()=="")){
                                Ext.Msg.alert("Empty","No Source");
                                return true;
                            }
                            var x = Ext.create({
                                xtype:'dialog',
                                title: "Current Image",
                                maximizable: false,
                                closable:true,
                                dismissHandler:function(){
                                    this.hide();
                                },
                                shadow:true,
                                modal:true,
                                minWidth:500,
                                minHeight:300,
                                items:[{
                                    xtype: 'panel',
                                    hidden:true,
                                    html: ``,
                                    style:{
                                        padding:10, "background-color":"white"
                                    },
                                    afterRender:function(){
                                        var filefield=el.up("containerfield").down("filefield");
                                        if( filefield.getFiles().length>0 && (filefield.getFiles()[0].type).includes("image")){
                                            this.setHtml(`<canvas width=500 height=300></canvas>`);
                                            var canvas = this.renderElement.dom.getElementsByTagName("canvas")[0];
                                            var ctx=canvas.getContext("2d");
                                            var maxW=canvas.width;
                                            var maxH=canvas.height;
                                            var img = new Image;
                                            img.onload = function() {
                                                    var iw=maxW;
                                                    var ih=maxW;
                                                    var scale=Math.min((maxW/iw),(maxH/ih));
                                                    var iwScaled=iw*scale;
                                                    var ihScaled=ih*scale;
                                                    ctx.drawImage(img,0,0,maxW,maxH);
                                                    // output.value = canvas.toDataURL("image/jpeg",0.5);
                                            }
                                            img.src = URL.createObjectURL(filefield.getFiles()[0]);
                                            this.setHidden(false);
                                            this.up().down("image").destroy();
                                            this.up("dialog").setTitle("Ready to Upload...")
                                        }else if( filefield.getFiles().length>0 && !(filefield.getFiles()[0].type).includes("image")){
                                            this.setHtml(`<center><h1>File: ${ filefield.getFiles()[0].name }</h1></center>`);
                                            this.setHidden(false);
                                            this.up().down("image").destroy();
                                            this.up("dialog").setTitle("Ready to Upload...")
                                        }else{
                                            let imagePanel = this.up().down("image");
                                            imagePanel.setSrc(el.up("containerfield").down("hiddenfield").getValue());
                                            imagePanel.setHidden(false);
                                        }
                                    }
                                },{
                                    xtype:'image',
                                    height: 280,
                                    width: 500,
                                    style:{
                                        padding:10, "background-color":"white"
                                    },
                                    listeners:{
                                        error:function(el,e){
                                            try{
                                                window.open(el.getSrc());
                                                el.up("dialog").destroy();
                                            }catch(e){}
                                        }
                                    }
                                }]
                            });
                            x.show();
                        }
                    }

                ]
            };
        }else if(["file_image"].includes(column.type)){
            data = {
                xtype:"containerfield",
                field:false,
                label: column.label,
                required:column.required,
                hidden: column.hidden,
                propertyResponse : null,
                propertyFile : "file",
                propertyAdditional:{},
                items:[{
                    xtype: 'progress',
                    shadow: false,
                    value:0,
                    width:"80%",
                    flex: self.getCols()==1?null:1,
                    hidden:true
                    // bind: {
                    //     text: 'Loading {word} {itemPercent}%',
                    //     value: '{progress}'
                    // }
                },{
                    xtype:"button", iconCls:"fas fa-undo",
                    ui:"alt decline",
                    tooltip:"reset",
                    style:"margin-left:3px;margin-top:7px;max-height:25px;max-width:25px",
                    hidden:true,
                    disabled:false,
                    handler:function(el){
                        el.up("containerfield").down("progress").setHidden(true);
                        el.setHidden(true);
                        el.up("containerfield").getItems().items[2].setHidden(true);
                        el.up("containerfield").down("filefield").reset();
                        el.up("containerfield").down("filefield").setHidden(false);
                        el.up('containerfield').down('hiddenfield').reset();
                    }
                },{
                    hidden:true,
                    xtype:"button", iconCls:"fas fa-search-plus",
                    tooltip:"preview",
                    disabled:false,
                    style:"margin-left:3px;margin-top:7px;max-height:25px;max-width:25px",
                    handler:function(el){
                        let containerfield = el.up('containerfield');
                        var x = Ext.create({
                            xtype:'dialog',
                            title: "Current Image",
                            maximizable: false,
                            closable:true,
                            dismissHandler:function(){
                                this.hide();
                            },
                            shadow:true,
                            modal:true,
                            minWidth:500,
                            minHeight:300,
                            items:[{
                                xtype: 'panel',
                                hidden:true,
                                html: ``,
                                style:{
                                    padding:10, "background-color":"white"
                                },
                                afterRender:function(){
                                    var filefield=el.up("containerfield").down("filefield");
                                    if( filefield.getFiles().length>0 && (filefield.getFiles()[0].type).includes("image")){
                                        this.setHtml(`<canvas width=500 height=300></canvas>`);
                                        var canvas = this.renderElement.dom.getElementsByTagName("canvas")[0];
                                        var ctx=canvas.getContext("2d");
                                        var maxW=canvas.width;
                                        var maxH=canvas.height;
                                        var img = new Image;
                                        img.onload = function() {
                                                var iw=maxW;
                                                var ih=maxW;
                                                var scale=Math.min((maxW/iw),(maxH/ih));
                                                var iwScaled=iw*scale;
                                                var ihScaled=ih*scale;
                                                ctx.drawImage(img,0,0,maxW,maxH);
                                                // output.value = canvas.toDataURL("image/jpeg",0.5);
                                        }
                                        img.src = URL.createObjectURL(filefield.getFiles()[0]);
                                        this.setHidden(false);
                                        this.up().down("image").destroy();
                                        this.up("dialog").setTitle("Accepted File...")
                                    }else if( filefield.getFiles().length>0 && !(filefield.getFiles()[0].type).includes("image")){
                                        this.setHtml(`<center><h1>File: ${ filefield.getFiles()[0].name }</h1></center>`);
                                        this.setHidden(false);
                                        this.up().down("image").destroy();
                                        this.up("dialog").setTitle("Accepted File...")
                                    }else{
                                        let imagePanel = this.up().down("image");
                                        if( containerfield['read']!==undefined && typeof(containerfield['read'])=='function' ){
                                            let val = containerfield.read(containerfield.down("hiddenfield").getValue());
                                            imagePanel.setSrc(val);
                                        }else{
                                            imagePanel.setSrc(containerfield.down("hiddenfield").getValue());
                                        }
                                        imagePanel.setHidden(false);
                                    }
                                }
                            },{
                                xtype:'image',
                                height: 280,
                                width: 500,
                                style:{
                                    padding:10, "background-color":"white"
                                },
                                listeners:{
                                    error:function(el,e){
                                        try{
                                            window.open(el.getSrc());
                                            el.up("dialog").destroy();
                                        }catch(e){}
                                    }
                                }
                            }]
                        });
                        x.show();
                    }
                },{
                    xtype:"hiddenfield",
                    name :column.name,
                    listeners:{
                        blur:function(){
                            if(this.getValue()===null || this.getValue()==""){return}
                            let loading = this.up("containerfield").down("progress");
                            let buttonReset = this.up("containerfield").getItems().items[1];
                            let buttonView = this.up("containerfield").getItems().items[2];
                            let filefield = this.up("containerfield").getItems().items[4];
                            buttonView.setHidden(false);
                            buttonReset.setHidden(false);
                            buttonView.setDisabled(false);
                            buttonReset.setDisabled(false);
                            loading.setHidden(false);
                            loading.setValue(1);
                            loading.setText(this.getValue());
                            filefield.setHidden(true);
                        }
                    }
                },{
                    xtype   : "filefield",
                    width: "100%",
                    // flex: self.getCols()==1?null:1,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                    labelWidth: self.getLabelWidth(),
                    // accept  : "image",
                    // maxSize : 10,
                    afterRender:function(){
                        this.getFileButton().setConfig({
                            iconCls: "fas fa-upload",
                            text : null,
                            style:"margin-right:5px"
                        });
                    },
                    listeners:{
                        change : function(el,v){

                            let loading = el.up("containerfield").down("progress");
                            let containerfield = el.up('containerfield');
                            let buttonReset = el.up("containerfield").getItems().items[1];
                            let buttonView = el.up("containerfield").getItems().items[2];
                            var formData = new FormData();
                            el.setHidden(true);

                            if(containerfield.model===undefined){
                                Ext.Msg.alert("Warning!",`model property must be set`);
                                return false;
                            }
                            if(el.getValue()===""||el.getValue()===null){return true}
                            if(containerfield['maxSize']!==undefined && el.getFiles().length>0){
                                let max = containerfield.maxSize * 1000000;
                                if(el.getFiles()[0].size>max){
                                    el.reset();
                                    buttonReset._handler(buttonReset);
                                    Ext.Msg.alert("Disallowed!",`Sorry, max size must be less than ${containerfield.maxSize} MB`);
                                    el.setError(`Sorry, max size must be less than ${containerfield.maxSize} MB`);
                                    return false;
                                }
                            }
                            loading.setHidden(false);
                            for(key in containerfield.propertyAdditional){
                                formData.append(key, containerfield.propertyAdditional[key] );
                            }
                            formData.append(containerfield.propertyFile, el.getFiles()[0] );
                            api.createUpload({
                                model:containerfield.model,
                                data: formData
                            },function(json){
                                buttonView.setHidden(false);
                                buttonReset.setHidden(false);
                                buttonView.setDisabled(false);
                                buttonReset.setDisabled(false);
                                let response = containerfield.propertyResponse===undefined||containerfield.propertyResponse===null?json:json[containerfield.propertyResponse];
                                containerfield.down('hiddenfield').setValue(response);
                                loading.setText(el.getFiles()[0].name);
                            },function(failed){
                                Ext.Msg.alert("Failed!","error");
                                buttonReset._handler(buttonReset);
                            },function(progress){
                                if (progress.lengthComputable)
                                    {
                                        var percentComplete = (progress.loaded / progress.total);
                                        loading.setValue(percentComplete);
                                        loading.setText(` uploading ${parseInt(percentComplete*100)}%`);
                                        // console.log(percentComplete);
                                        // if(percentComplete==1){
                                        // }
                                    }
                            },false);
                        }
                    },
                }]
            }
        }else if(["file_new"].includes(column.type)){
            data={
                name: column.name,
                xtype: 'qluploadfield',
                cls: "qluploadfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                value:column.default,
                errorTarget: 'side',
                readOnly: column.readonly,
                model:'set_mas_user/fileupload',
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required
            }
        }else if(["money","numeric","number"].includes(column.type)){
            data ={
                name: column.name,
                xtype: 'qlnumberfield',
                cls: "qlnumberfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                value:column.default,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                // validators: /^[0-9.]{1,}$/,
                textAlign:"right",
                // anchor: '100%',
                minValue: 0,
            };
        }else if(["date","datetime"].includes(column.type)){
            data ={
                name: column.name,
                xtype: 'datepickerfield',
                cls: "qldatepickerfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                dateFormat:'d/m/Y',
                value : column.value=="now"?new Date:null,
                placeholder:'dd/mm/yyyy',
                validators: 'date'
                // maxValue: new Date()
            };
        }else if(["time"].includes(column.type)){
            data ={
                name: column.name,
                xtype: 'timefield',
                cls: "qltimefield",
                editable:false,
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
                format:'H:i',
            };
        }else if(["rowspaces","space"].includes(column.type)){
            data ={
                xtype   : 'field',
                isField : false,
                isFormField : false,
                style   :{
                    color:"red"
                }
                // anchor: '100%',
                // minValue: 0,
            };
        }else if(["text_only","label","label_only","label only","text only"].includes(column.type)){
            data ={
                cls: "qllabel",
                html   : `<span>${column.label}</span>`,
                style:{
                    "font-size":"14px",
                    "color": "#656f75"
                }
            };
        }else if(["textnumber"].includes(column.type)){
            data ={
                xtype: 'qltextnumberfield',
                cls: "qltextnumberfield",
                name:column.name,
                label: column.label,
                labelAlign : 'left',
                value:column.default,
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
            };
        }else if(["textarea"].includes(column.type)){
            data ={
                xtype: 'textareafield',
                cls: "qltextareafield",
                name:column.name,
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required
            };
        }else if(["button"].includes(column.type)){
            data ={
                xtype: 'button',
                ui: 'confirm',
                width:"33px",
                iconCls: 'far fa-paper-plane',
                style: {
                    "margin-left":"3px",
                    "margin-top":"7px"
                }
            };
        }else if(["password"].includes(column.type)){
                data ={
                    xtype: 'passwordfield',
                    cls: "qlpasswordfield",
                    name:column.name,
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    value:column.default,
                    errorTarget: 'side',
                    autoComplete:false,
                    clearable: true,
                    revealable: true,
                    validators: {
                        type: 'format',
                        message: 'Only Alphanumeric chars are allowed',
                        matcher: /^[0-9a-zA-Z]*$/
                    },
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                };
        }else if(["domainfield"].includes(column.type)){
            data ={
                xtype: 'textfield',
                cls: "qldomainfield",
                name:column.name,
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                value:column.default,
                errorTarget: 'side',
                autoComplete:false,
                clearable: true,
                placeholder : "www.yoursite.com",
                validators: {
                   type: 'format',
                   message: 'invalid domain name',
                   matcher: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
                },
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,
            };
        }else if(["string","varchar","text","email"].includes(column.type)){
            if( (column.label.toLowerCase()).includes("email") ){
                data ={
                    xtype: 'emailfield',
                    cls: "qlemailfield",
                    name:column.name,
                    label: column.label,
                    labelAlign : 'left',
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    value:column.default,
                    errorTarget: 'side',
                    autoComplete:false,
                    validators:'email',
                    placeholder: 'email@domain.com',
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                };
            }else{
                data ={
                    xtype: 'textfield',
                    cls: "qltextfield",
                    name:column.name,
                    label: column.label,
                    labelAlign : 'left',
                    value:column.default,
                    width: isChild? self.tempWidth:defaultWidth,
                    labelWidth: self.getLabelWidth(),
                    labelWrap:true,
                    errorTarget: 'side',
                    autoComplete:false,
                    readOnly: column.readonly,
                    disabled: column.disabled,
                    hidden: column.hidden,
                    required:column.required,
                };
            }
        }else if(["hidden"].includes(column.type)){
            data ={
                name: column.name,
                xtype: 'textfield',
                hidden:true
            };
        }else if(["popupnew"].includes(column.type)){
            data = {
                name    : column.name,
                xtype   : "qlpopupfield",
                label   : column.label,
                readOnly: column.readonly,
                required: column.required,
                width   : isChild? self.tempWidth:defaultWidth,
                labelWidth : self.getLabelWidth(),
                hidden  : column.hidden,
                original:"id,password",
                idFieldConfig   : {},
                textFieldConfig : {},
                popupButtonConfig:{},
                clearButtonConfig:{}
            };
        }else{
            data ={
                name: column.name,
                xtype: 'textfield',
                cls: "qltextfield",
                label: column.label,
                labelAlign : 'left',
                width: isChild? self.tempWidth:defaultWidth,
                labelWidth: self.getLabelWidth(),
                labelWrap:true,
                errorTarget: 'side',
                autoComplete:false,
                readOnly: column.readonly,
                disabled: column.disabled,
                hidden: column.hidden,
                required:column.required,

            };
        }

        data["ui"] = 'solo';
        var propsMe = false;
        var props;
        for(let index=0; index<self.getProperties().length;index++){
            if( self.getProperties()[index].id == column.id ){
                propsMe = true;
                props = self.getProperties()[index].props;
                if(props['valueField']!==undefined && props['displayField']!==undefined && props['store'] !==undefined && typeof(props['store']) == 'string' ){
                    if(props['store'].includes('offline')){
                        perluTextHidden=false;
                        data['name']=data['name'].replace("_id", "");
                    }
                }
                break;
            }
        }
        if(propsMe){
            if(props['extraParams']!==undefined){
                Object.assign(props['extraParams'],{
                    origin_type : column.type,
                    origin : self.getId()
                });
            }
            if( !["enum_popup","popup"].includes(column.type) && column.name!='___' ){
                if( !['panel','formpanel','container'].includes(data.xtype) ){
                    Object.assign(data,props);
                }else{
                    for( let i=0;i < data.items.length; i++ ){
                        if( !['button','component'].includes(data.items[i].xtype) ){
                            Object.assign(data.items[i],props);
                        }
                    }
                }
            }else{
                if(["enum_popup","popup"].includes(column.type) || column.name=='___'){
                    if(props.textfield!==undefined){}
                    Object.assign(data.items[0],props.textfield===undefined?{}:props.textfield);
                    Object.assign(data.items[1],props.idfield===undefined?{}:props.idfield);
                    Object.assign(data.items[2],props.popupbutton===undefined?{}:props.popupbutton);
                    Object.assign(data.items[3],props.erasebutton===undefined?{}:props.erasebutton);
                    if( props['popupbutton'] === undefined || (props['popupbutton'] !== undefined && props.popupbutton['handler'] === undefined)){
                        Object.assign(data.items[2],{
                            title : props['title']===undefined?"Data":props["title"],
                            model : props['model']===undefined?"modelHttp":props["model"],
                            multi : props['multi']===undefined?false:(props["multi"]?true:false),
                            payload : props['original']===undefined?"":props["original"],
                            primaryKeys : props['primaryKeys']===undefined?"":props["primaryKeys"],
                            columns : props['columns']===undefined?[]:props["columns"],
                            extraParams : props['extraParams']===undefined?{}:props['extraParams'],
                            action : props['onSelect']===undefined?undefined:props["onSelect"],
                            transform : props['transform']===undefined?undefined:props["transform"]
                        });
                        Object.assign(data.items[2],{
                            handler:function(button){
                                let dependantOk = true;
                                let title=button.title;
                                let model=button.model;
                                let columns=button.columns;
                                let action=button.action;
                                let selected=button.payload;
                                let extraParams=button.extraParams;
                                let primaryKeys=button.primaryKeys;
                                let transform=button.transform;
                                // ========================================================
                                let liveparams = JSON.parse(JSON.stringify(extraParams));
                                let keys = Object.keys(liveparams);
                                keys.forEach(function(key){
                                    liveparams[key]=liveparams[key].toString();
                                    if( liveparams[key].includes("{") && liveparams[key].includes("}")){
                                        arrayParams = liveparams[key].split("}");
                                        arrayParams.forEach(function(rawParam){
                                            let rawParam2=rawParam.split("{");
                                            let fixedParam = rawParam2[rawParam2.length-1];
                                            if(fixedParam.includes(".") && fixedParam!==""){
                                                let formId = fixedParam.split(".")[0];
                                                let fieldName = fixedParam.split(".")[1];
                                                let val = Ext.getCmp(formId).getFields(fieldName).getValue();
                                                if(val==null || val==""){
                                                    if(fieldName.includes("_id")){
                                                        let fieldLain = Ext.getCmp(formId).getFields(fieldName.replace("_id",""));
                                                        if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                    }else{
                                                        let fieldLain = Ext.getCmp(formId).getFields(fieldName+"_id");
                                                        if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                    }
                                                    Ext.getCmp(formId).getFields(fieldName).setError("silahkan diisi dahulu");
                                                    dependantOk=false;
                                                    return false;
                                                }
                                                liveparams[key]=liveparams[key].replace(`{${fixedParam}}`,val);
                                            }else if(fixedParam!==""){
                                                try{
                                                    let val = button.up("formpanel").getFields(fixedParam).getValue();
                                                    if(val==null || val==""){
                                                        if(fixedParam.includes("_id")){
                                                            let fieldLain = button.up("formpanel").getFields(fixedParam.replace("_id",""));
                                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                        }else{
                                                            let fieldLain = button.up("formpanel").getFields(fixedParam+"_id");
                                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                        }
                                                        button.up("formpanel").getFields(fixedParam).setError("silahkan diisi dahulu");
                                                        dependantOk=false;
                                                        return false;
                                                    }
                                                    liveparams[key]=liveparams[key].replace(`{${fixedParam}}`,val);
                                                }catch(e){}
                                                
                                            }
                                        });
                                    }
                                });
                                if(!dependantOk){
                                    Ext.Msg.alert("Wait","Please Fill Parent Filter");
                                    return;
                                }
                                // ====================================================
                                let proxy = {
                                    model: model,
                                    parameters: liveparams,
                                };
                                if(columns.length>0){
                                    columns[columns.length-1]['cell']={
                                        tools: {
                                            pilih: {
                                                iconCls: "x-fa fa-check",
                                                handler: function(el,row){
                                                    var data = row.record.data;
                                                    var form = button.up("formpanel");

                                                    if(selected!=""){
                                                        selected=selected.split(",");
                                                        let selectedKey = {};
                                                        selectedKey[(column.name).replace("_id","")+"_id"]=data[selected[0]];
                                                        selectedKey[(column.name).replace("_id","")]=data[selected[1]];
                                                        form.setValues(selectedKey);
                                                    }
                                                    if(typeof(action)=='function'){
                                                        action(form,data);
                                                    }
                                                    try{
                                                        button.up("container").getItems().items[3].getHandler()(button)
                                                    }catch(e){}
                                                    el.up("dialog").destroy();
                                                }
                                            }
                                        }
                                    }
                                }
                                let storeId=button.up("formpanel").getId();
                                Ext.create({
                                    xtype: button.multi?"QLDatatablePopupMulti":"QLDatatablePopupV1",
                                    transform: transform,
                                    title: title,
                                    gridHeight: "400px",
                                    storeId:storeId,
                                    primaryKeys:primaryKeys,
                                    action:action,
                                    store: {
                                        proxy: proxy
                                    },
                                    columns: columns,
                                }).show();
                            }
                        });
                    }
                    Object.assign(data.items[3],{
                        onDelete:props['onReset']===undefined?function(form,oldData){}:props["onReset"],
                        handler: function(el){
                            let data = {};
                            let action = el['onDelete'];
                            let form = el.up("formpanel");
                            if(el['onDelete']!==undefined){
                                action(form, form.getValues());
                                data[column.name+"_id"]=null;
                                data[column.name]=null;
                                el.up("formpanel").setValues(data);
                            }
                            let vm = findParentModel(el);
                            if( vm===null || vm===undefined ){ return }
                            let dependantForms = vm.get("dependants")===null?{}:vm.get("dependants");
                            let formId = form.getId();
                            Object.keys(dependantForms).forEach(function(formKey){
                                let dependantFields  = dependantForms[formKey];
                                dependantFields.forEach(function(dependantField){
                                    let dependantArray = dependantField.split(".");
                                    if(dependantArray[0]==formId && (dependantArray[1] == (column.name).replace("_id","")+"_id" || dependantArray[1] == (column.name).replace("_id","")) ){
                                        let targetField = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]);
                                        if( ['combobox','selectfield'].includes(targetField.xtype) ){
                                            targetField.clearValue();
                                            targetField.setInputValue(null);
                                        }else{
                                            targetField.reset();
                                        }
                                        targetField.setError(null);
                                        if(dependantArray[3].includes("_id")){
                                            let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3].replace("_id",""));
                                            if(fieldLain!==undefined){
                                                if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                                    fieldLain.clearValue();
                                                    fieldLain.setInputValue(null);
                                                }else{
                                                    fieldLain.reset();
                                                }
                                            }
                                            fieldLain.setError(null);
                                        }else{
                                            let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]+"_id");
                                            if(fieldLain!==undefined){
                                                if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                                    fieldLain.clearValue();
                                                    fieldLain.setInputValue(null);
                                                }else{
                                                    fieldLain.reset();
                                                }
                                            }
                                            fieldLain.setError(null);
                                        }
                                    }
                                });
                            });
                        },
                    });
                    props.textfield=undefined;
                    props.popupbutton=undefined;
                    props.erasebutton=undefined;
                    if(props["remember"]!==undefined){
                        Object.assign(data.items[0],props.remember===undefined?{}:{
                            remember:props.remember
                        });
                        Object.assign(data.items[1],props.remember===undefined?{}:{
                            remember:props.remember
                        });
                    }
                    Object.assign(data.items[0],props);
                    if(props.fullPopup!==undefined){
                        Object.assign(data, props.fullPopup);
                    }
                }
            }
        }
        if(isChild){
            Object.assign(data,{
                labelCls:"label-normal"
            });
        }
        var fixedItems = [data];
        if(Array.isArray(data)!==true && ["selectfield","combobox"].includes(data["xtype"])){
            if(perluTextHidden===true){
                fixedItems.push({
                    name:column.name,
                    xtype :"textfield",
                    isChildCombo :true,
                    hidden :true,
                    remember :(fixedItems[0]['remember']!==undefined && fixedItems[0]['remember']===true)?true:false,
                    afterRender :function(){
                        try{this.setValue(this.up("formpanel").getValues()[column.name+"_id"]);}catch(e){console.log(e);}
                    }
                });
            }
            var listener = data["listeners"]===undefined?{}:data["listeners"];
            Object.assign(listener, {
                select:function(el,val){
                    if(data["listeners"]['selected']!==undefined){
                        try{
                            let func = data["listeners"]['selected'];
                            func(el,val);
                        }catch(e){}
                    }
                    if(perluTextHidden===true){
                        try{el.up("formpanel").getFields(column.name).setValue(val.data[el.getDisplayField()]);}catch(e){}
                    }
                    let vm = findParentModel(el);
                    if( vm===null || vm===undefined ){ return }
                    if( el.oldValue===undefined || el.oldValue == el.getValue() ){ return }
                    el.oldValue=el.getValue();
                    let dependantForms = vm.get("dependants")===null?{}:vm.get("dependants");
                    let form=el.up("formpanel");
                    let formId = form.getId();
                    Object.keys(dependantForms).forEach(function(formKey){
                        let dependantFields  = dependantForms[formKey];
                        dependantFields.forEach(function(dependantField){
                            let dependantArray = dependantField.split(".");
                            if(dependantArray[0]==formId && (dependantArray[1] == (el.getName()).replace("_id","")+"_id" || dependantArray[1] == (el.getName()).replace("_id","")) ){
                                if( el['gelondongan']===undefined || (el['gelondongan']!==undefined && !el['gelondongan'].includes(dependantArray[3])) ){
                                    let targetField = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]);
                                    if( ['combobox','selectfield'].includes(targetField.xtype) ){
                                        targetField.clearValue();
                                        targetField.setInputValue(null);
                                    }else{
                                        targetField.reset();
                                    }
                                    targetField.setError(null);
                                    if(dependantArray[3].includes("_id")){
                                        let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3].replace("_id",""));
                                        if(fieldLain!==undefined){
                                            if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                                fieldLain.clearValue();
                                                fieldLain.setInputValue(null);
                                            }else{
                                                fieldLain.reset();
                                            }
                                        }
                                        fieldLain.setError(null);
                                    }else{
                                        let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]+"_id");
                                        if(fieldLain!==undefined){
                                            if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                                fieldLain.clearValue();
                                                fieldLain.setInputValue(null);
                                            }else{
                                                fieldLain.reset();
                                            }
                                        }
                                        fieldLain.setError(null);
                                    }

                                }
                            }
                        });
                    });
                    el['gelondongan']=[];
                }
            });
            data["listeners"]=listener;
        }
        if(["hidden"].includes(column.type)){
            return data;
        }
        return isChild?fixedItems:{
                xtype:'container',
                autoSize:true,
                cls:'qlfield',
                margin: self.getMargin(), //TOP RIGHT BOTTOM LEFT
                items:fixedItems
        };
    },
    get : ()=>{
        var data = self.getData().forms[self.getIndexJSON()];
        var jsonVersion = self.getData()['json_version']===undefined?1:self.getData()['json_version'];
        var semuaKolom = [];
        var columns = data.data;
        var colsTemp = columns;
        var columns  = [];
        var hiddenfields = [];
        var joining_id = null;
        var separators = [];
        for(let i=0; i<colsTemp.length; i++){
            if(jsonVersion==2){
                colsTemp[i]['label'] = (colsTemp[i]["label-name-type"]).split("*-*")[0];
                colsTemp[i]['name'] = (colsTemp[i]["label-name-type"]).split("*-*")[1];
                colsTemp[i]['type'] = (colsTemp[i]["label-name-type"]).split("*-*")[2];
                colsTemp[i]['required'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[1]=="true"?true:false;
                colsTemp[i]['readonly'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[2]=="true"?true:false;
                colsTemp[i]['disabled'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[3]=="true"?true:false;
                colsTemp[i]['hidden'] = (colsTemp[i]["ch-req-ro-dis-hd"]).split("*-*")[4]=="true"?true:false;
                colsTemp[i]['withlabel'] = colsTemp[i]['withlabel']!==undefined?colsTemp[i]['withlabel']:true;
            }
           if(colsTemp[i].label==''||colsTemp[i].label==null){
                if(joining_id==null){
                    joining_id = i-1;
                    colsTemp[joining_id]['child']=[];
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }else{
                    (colsTemp[joining_id]['child']).push(colsTemp[i]);
                }
           }else{
               joining_id=null;
           }
        }
        columns = colsTemp.filter(function(data,index){            
            if( (data['type']).includes('separator')){
                let before = colsTemp[index-1];
                Object.assign(data,{
                    index:index
                });
                if(before==undefined){
                    separators.push(data);
                }else if(!(before.type).includes('separator')){
                   separators.push(data);
                }else{
                    if(separators.length==0){
                        separators.push(data);
                    }else{
                        data['index']=separators[separators.length-1].index;
                        separators.push(data);
                    }
                }
            }
            if(data.type=="hidden"){
                hiddenfields.push(data);
            }
            if(data.label!=''&& data.label!=null && data.type!="hidden" && !(data.type).includes('separator')){
                return data;
            }
        });

        var inlineRowSpaces = [];
        var hitung=0;
        for(let i=0;i<columns.length ;i++){
            for(let j=0;j<self.getRowSpaces().length;j++){
                if( self.getRowSpaces()[j].id == columns[i].id ){
                    for(let k=0;k<self.getRowSpaces()[j].total;k++){
                        inlineRowSpaces.push(i+( (self.getRowSpaces()[j].position=="before")?1:2)+(hitung++) );
                    }
                    break;
                }
            }
        }
        Ext.iterate(inlineRowSpaces,function(index){
            columns.splice( (index-1), 0, {
                "name": "rowspaces"+index,
                "type": "rowspaces",
                "label": "rowspaces",
            });
        });
        if(columns.length%self.getCols() > 0){
            columns.push({
                "name": "rowspacesother",
                "type": "rowspaces",
                "label": "rowspaces",
            });
        }
        var jumlahBaris = Math.ceil( columns.length/self.getCols() );

        for (let i = 0; i < jumlahBaris ; i++) {            
            var hasilFields = [];
            if( columns[i].child == undefined ){
                hasil =  self.kamusElement(columns[i]);
            }else{
                var hasil = {
                    xtype   : 'containerfield',
                    cls     : "qlsandingan",
                    required: columns[i].required,
                    labelWidth:self.getLabelWidth(),
                    isField : false,
                    layout  : 'hbox',
                    labelWrap:true,
                    hidden:columns[i].hidden,
                    label  : columns[i].withlabel?columns[i].label:""
                }
                var items = [];
                let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[i].type=='label only'?0:1))-3}%`
                self.tempWidth=defaultWidth;

                if(!["label only"].includes(columns[i].type)){
                    columns[i].withlabel=false;
                    var childArr=self.kamusElement(columns[i],true);
                    items.push( Object.assign(
                        childArr[0],{
                            // flex:1,
                            style:{
                                "margin-left":"0px",
                                "max-height": !["textarea","popup"].includes(columns[i].type)?"38px":"100%"
                            }
                        })
                    );
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                for(let j=0;j<columns[i].child.length;j++){
                    var childArr=self.kamusElement(columns[i].child[j],true);
                    var child=childArr[0];
                    if(!["label only"].includes(columns[i].type) || j>0){
                        Object.assign(child,{
                            style:{
                                "margin-left":"5px",
                                "max-height":!["textarea","popup"].includes(columns[i].child[j].type)?"38px":"100%"
                            }
                        });
                    }
                    items.push(child);
                    if(childArr[1]!==undefined){
                        items.push(childArr[1]);
                    }
                }
                hasil['items']=items;
            }
            hasilFields.push(hasil);

            try{
                if( columns[jumlahBaris+i].child ==undefined ){
                    var hasil = self.kamusElement(columns[jumlahBaris+i]);
                }else{
                    var hasil = {
                        xtype   : 'containerfield',
                        cls     : "qlsandingan",
                        required: columns[jumlahBaris+i].required,
                        labelWidth:self.getLabelWidth(),
                        layout  : 'hbox',
                        labelWrap : true,
                        hidden:columns[jumlahBaris+i].hidden,
                        label  : columns[jumlahBaris+i].withlabel?columns[jumlahBaris+i].label:""
                    }
                    var items = [];
                    let defaultWidth  = `${parseInt(self.getDefaultWidth())/(columns[jumlahBaris+i].child.filter(data=>!['button','label only'].includes(data.type)).length+(columns[jumlahBaris+i].type=='label only'?0:1))-3}%`
                    self.tempWidth=defaultWidth;

                    if(!["label only"].includes(columns[jumlahBaris+i].type)){
                        columns[jumlahBaris+i].withlabel=false;
                        var childArr=self.kamusElement(columns[jumlahBaris+i],true);
                        items.push( Object.assign(
                            childArr[0],{
                                // flex:1,
                                style:{
                                    "margin-left":"0px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].type)?"38px":"100%"
                                }
                            })
                        );
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    for(let j=0;j<columns[jumlahBaris+i].child.length;j++){
                        var childArr=self.kamusElement(columns[jumlahBaris+i].child[j],true);
                        var child=childArr[0];
                        if(!["label only"].includes(columns[jumlahBaris+i].type) || j>0){
                            Object.assign(child,{
                                style:{
                                    "margin-left":"5px",
                                    "max-height": !["textarea","popup"].includes(columns[jumlahBaris+i].child[j].type)?"38px":"100%"
                                }
                            });
                        }
                        items.push(child);
                        if(childArr[1]!==undefined){
                            items.push(childArr[1]);
                        }
                    }
                    hasil['items']=items;
                }
                hasil["style"]={ "margin-left" : "30px"}
                hasilFields.push( hasil );
            }catch(err){}
            let separatorArray = separators.filter(_separator=>{
                return _separator.index==i;
            });
            separatorArray.forEach(_separator=>{                
                let separatorElem={};
                if(_separator.type=='separator_line'){
                    separatorElem={
                        xtype:'container',
                        html:"<hr size='1px' style='border: 1px solid #d2c9c9;'>"
                    }
                }else if(_separator.type=='separator_label'){
                    separatorElem={
                        xtype:'label',
                        html:`${_separator['value']}`
                    }
                }
                semuaKolom.push( {
                    xtype:"qlbarisfield",
                    // flex:1,
                    items : separatorElem,
                } );
            })
            semuaKolom.push( {
                xtype:"qlbarisfield",
                // flex:1,
                items : hasilFields,
            } );
        }
        hiddenfields.forEach(function(dt){
            semuaKolom.push(
                self.kamusElement(dt)
            );
        });
        console.log("%c Form: "+( self.getData().process ).replace(/ /g,"_")+" created "+(self.getIsOnline()?"[online] ":"[data]"),"background: #222; color: #a0ff5c;font-weight: bold;");
        for(let i=0;i<self.getNewHbox().length;i++){
            semuaKolom.splice(self.getNewHbox()[i].index,0,self.getNewHbox()[i].container);
        }
        if(self.getGrid()!=null){
            var grid = self.getGrid();
            grid.columns.push({
                align:'center',
                text : "Remove",
                width:"10%",
                cell: {
                    tools: {
                        delete: {
                            iconCls: 'x-fa fa-trash',
                            handler: function(el,row){
                                row.grid.getStore().remove(row.record);
                            }
                        },
                    }
                }
            });
            Object.assign(grid,{
                xtype:"grid",
                rowLines: true,
                columnLines: true,
                rowNumbers: {
                    text:'No',
                    align:"center",
                    width: "5%"
                },
                columnResize:false,
                afterRender:function(){
                    var mygrid = this;
                    mygrid.getStore().on("datachanged",function(){
                        let datalength = mygrid.getStore().getData().length;
                        mygrid.setHeight(datalength*32+60);
                    },mygrid.getStore());
                }
            });
            return {
                xtype : "panel",
                bodyPadding : 0,
                autoSize:true,
                items:[
                    {
                        cls         : "customForm",
                        bodyPadding : 10,
                        xtype       :'formpanel',
                        title       : self.getTitle(),
                        id          : self.getId(),
                        aliases     : self.getAliases(),
                        afterRender :function(){
                            this.setController(self.getController())
                        },
                        listeners: {
                            delegate    : 'field',
                            buffer      : 10, // buffer as validating each field may trigger an errorchange
                            // errorchange : 'formFieldErrors'
                        },
                        items   : semuaKolom,
                        buttonAlign : self.getButtonAlign(),
                        buttons    : self.getButtons()
                    },
                    grid
                ]
            }
        }else{
            var myform =  {
                cls         : "customForm",
                bodyPadding : 25,
                noform      : self.getData().forms[self.getIndexJSON()]['noform'],
                popup      : self.getData().forms[self.getIndexJSON()]['popup'],
                xtype       :'formpanel',
                title       : self.getTitle(),
                id          : self.getId(),
                aliases     : self.getAliases(),
                afterRender :function(){
                    if( (this['noform']!==undefined && this['noform']) ){
                        this.setHeight(0);
                    };
                    let thisForm = this;
                    thisForm.setController(self.getController());
                    let thisFields = thisForm.getFields();
                    Object.keys(thisFields).forEach(function(name){
                        let vm = findParentModel(thisForm);
                        let field = thisFields[name];
                        try{
                            if(field.getBind()!==null && field.getBind()['value']!==undefined){
                                if(field.getBind().value['stub']!==undefined){
                                    let path = field.getBind().value.stub['path'];
                                    if(path!==undefined){
                                        vm.set(path,field.getValue());
                                    }
                                }
                            }
                        }catch(e){}
                        if(field['autoString']!==undefined){
                            api.getArrayData({
                                model : field['autoString']
                            }, function (json){
                                if(field.getValue()===null || field.getValue()==""){
                                    field.setValue(json[api.rootProperty]===undefined?json:json[api.rootProperty]);
                                }
                            });
                        }
                        if(field['extraParams']!==undefined){
                            let params = JSON.parse(JSON.stringify(field['extraParams']));
                            let keys = Object.keys(params);
                            let keyLama = vm.get("dependants")===null?{}:vm.get("dependants");

                            keys.forEach(function(key){
                                if( params[key].toString().includes("{") && params[key].toString().includes("}")){
                                    arrayParams = params[key].toString().split("}");
                                    arrayParams.forEach(function(rawParam){
                                        let rawParam2=rawParam.split("{");
                                        let fixedParam = rawParam2[rawParam2.length-1];
                                        if(fixedParam.includes(".") && fixedParam!==""){
                                            let form = fixedParam.split(".")[0];
                                            if(keyLama[form]===undefined){
                                                keyLama[form]=[];
                                            }
                                            if(!keyLama[form].includes(fixedParam+"."+field.up("formpanel").getId()+"."+field.getName())){
                                                keyLama[form].push(fixedParam+"."+field.up("formpanel").getId()+"."+field.getName());
                                            }
                                        }else if(fixedParam!==""){
                                            if(keyLama[field.up("formpanel").getId()]===undefined){
                                                keyLama[field.up("formpanel").getId()]=[];
                                            }
                                            if(!keyLama[field.up("formpanel").getId()].includes(field.up("formpanel").getId()+"."+fixedParam+"."+field.up("formpanel").getId()+"."+field.getName())){
                                                keyLama[field.up("formpanel").getId()].push(field.up("formpanel").getId()+"."+fixedParam+"."+field.up("formpanel").getId()+"."+field.getName());
                                            }
                                        }
                                    });
                                }
                            });
                            vm.set("dependants",keyLama);
                        }
                    });

                },
                listeners: {
                    delegate    : 'field',
                    buffer      : 10, // buffer as validating each field may trigger an errorchange
                    // errorchange : 'formFieldErrors'
                },
                items   : semuaKolom,
                buttonAlign : self.getButtonAlign(),
                buttons    : self.getButtons()
            };

            if(self.getData().forms[self.getIndexJSON()]['popup']!==undefined && self.getData().forms[self.getIndexJSON()]['popup']===true){
                try{ Ext.getCmp("popup_"+self.getId()).destroy(); }catch(e){}
                Ext.create({
                    xtype : 'dialog',
                    dismissHandler:function(){
                        this.hide();
                    },
                    cls : "popupformulir",
                    maximizable: false,
                    closable:true,
                    title:self.getTitle(),
                    id: "popup_"+self.getId(),
                    shadow:true,
                    modal:true,
                    minWidth:700,
                    // minHeight:500
                });
                // return "popup_"+self.getId();
            }
            return myform;
        };

    }

}});