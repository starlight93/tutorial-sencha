
Ext.define('QL.DetailGrid',{
    alias: 'widget.qldetailgrid',
    constructor:function(data){
        if(data.columns[ data.columns.length-1 ].text!='action'){
            data.columns.push({
                text: 'Action',
                width: '10%',
                align:'center',
                cell:{
                    tools:{
                        delete:{
                            iconCls:'x-fa fa-trash',
                            handler:function(el, row){
                                row.grid.getStore().remove(row.record);
                            },
                        },
                    }
                }
            });
        }
        var basicGrid = {
            xtype: 'grid',
            bind:{
                height:'{tableHeight}'
            },
            scrollable:false,
            scroll:false,
            // scrollabel:true,
            cls: 'go-table',
            rowNumbers: true,
            store: {
                storeId: (data.storeName!=undefined?data.storeName:data.table),
                autoLoad: true,
                proxy: {
                    type: 'indexeddb',
                    databaseName: 'QLDatabase',
                    table: data.table
                },
                listeners:{
                    remove:function(store, records, index, isMove,){
                        store.erase({id:records[0].data.id})
                    },
                    add:function(store, records, index,){
                        store.create(records[0].data);
                        // aakuGrid = Ext.getCmp(data.storeName!=undefined?data.storeName:data.table);
                    },
                    load : function( el, records, successful, operation, eOpts ) {
                        console.log(records);
                        console.log("data terload");
                        // aakuGrid = Ext.getCmp(data.storeName!=undefined?data.storeName:data.table);
                    },
                    datachanged: function(el){
                        var length = el.getData().length;
                        Ext.getCmp("panelId").getViewModel().set("tableHeight",(length*33)+50);
                    },
                }
            },
            columns: data.columns,
            listeners:{
                refresh: function(){
                    console.log('terrefresh grid');
                },
                updatedata: function(){
                    console.log('terupdate grid');
                },
                storechange: function(){
                    console.log('storechange grid');
                }
            }
        }
        if(data.config!=undefined&&data.config!=null){
            basicGrid= Object.assign(basicGrid,data.config);
        }
        return basicGrid;
    },
});