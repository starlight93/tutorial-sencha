Ext.define('QL.domainfield',{
    extend: 'Ext.field.Text',
    xtype: "qldomainfield",
    cls: "qldomainfield",
    labelAlign : 'left',
    width: '100%',
    labelWidth: '100%',
    labelWrap:true,
    errorTarget: 'side',
    autoComplete:false,
    clearable: true,
    placeholder : "www.yoursite.com",
    validators: {
       type: 'format',
       message: 'invalid domain name',
       matcher: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
    }
});