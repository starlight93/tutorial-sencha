
Ext.define('QL.popupfield',{
    extend  : 'Ext.field.Container',
    xtype   : 'qlpopupfield',
    layout  : 'hbox',
    labelWidth  : '100%',
    labelAlign  : 'left',
    labelWrap   : true,
    idFieldConfig:{},
    textFieldConfig:{},
    popupButtonConfig:{},
    clearButtonConfig:{},
    items: [{
            xtype       : 'textfield',
            cls         : "qltextfield",
            customtype  : "popupfield",
            readOnly    : true,
            autoComplete: false,
            flex        : 1,
            labelWidth  : "100%",
            width       : `100%`,
            labelAlign  : 'left',
            labelWrap   : true,
            getContainerField:function(){
                return this.up();
            },
            getIdField:function(){
                return this.up().getItems().items[1];
            },
            getPopupButton:function(){
                return this.up().getItems().items[2];
            },
            getClearButton:function(){
                return this.up().getItems().items[3];
            },
            afterRender:function(){
                this.setWidth( (parseInt(this.up('qlpopupfield').width) - 15+"%") );
                this.setLabel( this.up('qlpopupfield').label );
                this.setRequired( this.up('qlpopupfield').required );
                let name = this.up('qlpopupfield').name;
                this.up('qlpopupfield').setName(null);
                this.setName( name );
                this.up('qlpopupfield').getItems().items[1].setName( name+"_id" );
                this.setConfig( this.up('qlpopupfield').textFieldConfig );
            }
        },{
            xtype       : 'textfield',
            hidden      : true,
            customtype  : "hiddenpopupfield",
            afterRender : function(){
                this.setConfig( this.up('qlpopupfield').idFieldConfig );
            }
        },
        {
            xtype       : 'qlpopupbutton',
            ui          : 'confirm',
            text        : "",
            cls         : 'ql-btn--popup',
            isField     : false,
            width       : "33px",
            style       : {
                "margin-left":"2px"
            },
            //=====================================
            multiple: false,
            transform:function(data){
                return data;
            },
            storeId:null,
            primaryKeys:{},
            title:"Popup Field",
            action:function(form,rowData){
                let onSelect = this.up('qlpopupfield')['onSelect'];
                if(onSelect!==undefined && typeof(onSelect)=='function'){
                    var callbackSelect = onSelect( form, rowData );
                    if(typeof(callbackSelect)=='boolean' && callbackSelect===false ){
                        return;
                    }
                }
                this.up('qlpopupfield').getItems().items[0].setValue( (rowData[this.up('qlpopupfield').original.split(",")[1]]) );
                this.up('qlpopupfield').getItems().items[1].setValue( (rowData[this.up('qlpopupfield').original.split(",")[0]]) );
            },
            columns: [{
                text: "No",
                xtype: "rownumberer",
                align: "center",
                width: "5%"
            },{
                text: "Action",
                align: "center",
                action:true,
                width: "10%"
            }],
            //=====================================
            afterRender : function(){
                this.title = this.up('qlpopupfield').title;
                this.storeId = this.up('qlpopupfield').storeId;
                this.primaryKeys = this.up('qlpopupfield').primaryKeys;
                this.transform = this.up('qlpopupfield').transform;                
                this.setProxy(this.up('qlpopupfield').proxy)
                this.setConfig( this.up('qlpopupfield').popupButtonConfig );
            },
        },
        {
            xtype   : 'button',
            ui      : 'alt decline',
            title   : ' reset',
            width   : "33px",
            iconCls : 'x-fa fa-lg fa-eraser',
            cls     : 'ql-btn--popup',
            style   : {
                "margin-left":"2px"
            },
            afterRender : function(){
                this.setConfig( this.up('qlpopupfield').clearButtonConfig );
            },
            handler : function(el){
                let onClear = el.up('qlpopupfield')['onClear'];
                let form = el.up("formpanel");
                if(onClear!==undefined && typeof(onClear)=='function'){
                    var callbackClear = onClear( form, form.getValues() );
                    if(typeof(callbackClear)=='boolean' && callbackClear===false ){
                        return;
                    }
                }
                this.up('qlpopupfield').getItems().items[0].setValue(null);
                this.up('qlpopupfield').getItems().items[1].setValue(null);
                let vm = findParentModel(this.up('qlpopupfield'));
                if( vm===null || vm===undefined ){ return }
                let dependantForms = vm.get("dependants")===null?{}:vm.get("dependants");
                let formId = form.getId();
                Object.keys(dependantForms).forEach(function(formKey){
                    let dependantFields  = dependantForms[formKey];
                    dependantFields.forEach(function(dependantField){
                        let dependantArray = dependantField.split(".");
                        if(dependantArray[0]==formId && (dependantArray[1] == (column.name).replace("_id","")+"_id" || dependantArray[1] == (column.name).replace("_id","")) ){
                            let targetField = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]);
                            if( ['combobox','selectfield'].includes(targetField.xtype) ){
                                targetField.clearValue();
                                targetField.setInputValue(null);
                            }else{
                                targetField.reset();
                            }
                            targetField.setError(null);
                            if(dependantArray[3].includes("_id")){
                                let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3].replace("_id",""));
                                if(fieldLain!==undefined){
                                    if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                        fieldLain.clearValue();
                                        fieldLain.setInputValue(null);
                                    }else{
                                        fieldLain.reset();
                                    }
                                }
                                fieldLain.setError(null);
                            }else{
                                let fieldLain = Ext.getCmp(dependantArray[2]).getFields(dependantArray[3]+"_id");
                                if(fieldLain!==undefined){
                                    if( ['combobox','selectfield'].includes(fieldLain.xtype) ){
                                        fieldLain.clearValue();
                                        fieldLain.setInputValue(null);
                                    }else{
                                        fieldLain.reset();
                                    }
                                }
                                fieldLain.setError(null);
                            }
                        }
                    });
                });
            },
        }
    ]
})