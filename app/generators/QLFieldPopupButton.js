Ext.define('QL.popupbutton',{
    extend  : 'Ext.Button',
    xtype   : 'qlpopupbutton',
    text    : 'popup',
    ui      : 'confirm',
    multiple: true,
    iconCls : 'x-fa fa-search',
    isField : true,
    getProxy:function(){
        let el=this;
        return Object.assign(el.proxy,{
            getExtraParams:function(){
                return el.proxy.parameters;
            },
            setExtraParam:function(key,value){
                return el.proxy.parameters[key]=value;
            },
            getModel:function(){
                return el.proxy.model;
            },
            setModel:function(model){
                return el.proxy.model=model;
            }
        });
    },
    setProxy:function(config){
        this.proxy = config;
    },
    getMultiple:function(){
        return this.multiple;
    },
    setMultiple:function(val){
        if(typeof(val)!='boolean'){
            return;
        }
        return val;
    },
    listeners:{
        tap:function(me,e, opts){
            let liveparams = JSON.parse(JSON.stringify(me.getProxy().getExtraParams()));
            if(me.isField){
                let button = me;
                let dependantOk = true;
                let keys = Object.keys(liveparams);
                keys.forEach(function(key){
                    liveparams[key]=liveparams[key].toString();
                    if( liveparams[key].includes("{") && liveparams[key].includes("}")){
                        arrayParams = liveparams[key].split("}");
                        arrayParams.forEach(function(rawParam){
                            let rawParam2=rawParam.split("{");
                            let fixedParam = rawParam2[rawParam2.length-1];
                            if(fixedParam.includes(".") && fixedParam!==""){
                                let formId = fixedParam.split(".")[0];
                                let fieldName = fixedParam.split(".")[1];
                                let val = Ext.getCmp(formId).getFields(fieldName).getValue();
                                if(val==null || val==""){
                                    if(fieldName.includes("_id")){
                                        let fieldLain = Ext.getCmp(formId).getFields(fieldName.replace("_id",""));
                                        if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                    }else{
                                        let fieldLain = Ext.getCmp(formId).getFields(fieldName+"_id");
                                        if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                    }
                                    Ext.getCmp(formId).getFields(fieldName).setError("silahkan diisi dahulu");
                                    dependantOk=false;
                                    return false;
                                }
                                liveparams[key]=liveparams[key].replace(`{${fixedParam}}`,val);
                            }else if(fixedParam!==""){
                                try{
                                    let val = button.up("formpanel").getFields(fixedParam).getValue();
                                    if(val==null || val==""){
                                        if(fixedParam.includes("_id")){
                                            let fieldLain = button.up("formpanel").getFields(fixedParam.replace("_id",""));
                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                        }else{
                                            let fieldLain = button.up("formpanel").getFields(fixedParam+"_id");
                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                        }
                                        button.up("formpanel").getFields(fixedParam).setError("silahkan diisi dahulu");
                                        dependantOk=false;
                                        return false;
                                    }
                                    liveparams[key]=liveparams[key].replace(`{${fixedParam}}`,val);
                                }catch(e){}                            
                            }
                        });
                    }
                });
                if(!dependantOk){
                    Ext.Msg.alert("Wait","Please Fill Parent Filter");
                    return;
                }
            }
            let columns  = JSON.parse(JSON.stringify(me.columns));
            if(!this.multiple){
                let actionIndex = columns.findIndex(dt=>{
                    return dt.action===true;
                });
                if(actionIndex>-1){
                    columns[actionIndex]['cell']={
                        xtype: "widgetcell",
                        widget: {
                            xtype:"button",                  
                            ui: "confirm round",
                            iconCls: "x-fa fa-check",
                            handler:function(el){
                                if(typeof(me.action)=='function'){
                                    me.action(me.up('formpanel'),el.up('widgetcell').getRecord().getData());
                                }
                                el.up("dialog").destroy();
                            }
                        }
                    }
                }                
            }
            let proxy = JSON.parse(JSON.stringify(me.getProxy()));
            Object.assign(proxy.parameters,liveparams);
            new Ext.create({
                xtype: this.multiple ? "QLDatatablePopupMulti" : "QLDatatablePopupV1",
                transform: me.transform,
                title: me.title,
                gridHeight: "400px",
                storeId:me.storeId,
                primaryKeys:me.primaryKeys,
                action:me.action,
                store: {
                    proxy: proxy
                },
                columns: columns,
            }).show();
        },
    },
    proxy   :{
        model : "inv_mas_mtl_item_def",
        parameters:{
            paginate: 50
        }
    },
    storeId:null,
    primaryKeys:{},
    title:"Popup List",
    columns: [{
        text: "No",
        xtype: "rownumberer",
        align: "center",
        width: "5%"
    },{
        text: "Action",
        align: "center",
        width: "10%"
    }],
})