function klikTutorial(e){
    console.log(e);
}
Ext.create({
    xtype:'dialog',
    maximizable: true,
    draggable:false,
    title:"R&D",
    closable:true,
    id: "fieldTester",
    shadow:false,
    modal:true,
    scrollable:true,
    dismissHandler:function(){
        this.hide();
    },
    style:{
        background:'transparent'
    },
    width:1100,
    viewModel:{
        progress:0.0
    },
    items:[{
        xtype   : "formpanel",
        scrollable:true,
        items   : [{
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"checkboxfield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Checkbox.html' target='_blank'>Check One</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                items:[{
                    label:"A",value:"A"
                },{
                    label:"B",value:"B"
                }]
            },{
                xtype:"checkboxgroup",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.CheckboxGroup.html' target='_blank'>Check Many</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                items:[{
                    label:"A",value:"A"
                },{
                    label:"B",value:"B",checked:true
                }]
            }]
        },
        //
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"numberfield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Number.html' target='_blank'>Number</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            },{
                xtype:"qluploadfield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.File.html' target='_blank'>Upload</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            }]
        },
    //
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"datefield",
                name:"datefield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Date.html' target='_blank'>Datefield</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                value:new Date,
                width:"50%",
                dateFormat:'d/m/Y',
                validators: 'date'
            },{
                xtype:"radiogroup",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.RadioGroup.html' target='_blank'>Radiogroup</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                items:[{
                    label: "A",value:"A"
                },{
                    label: "B",value:"B", checked:true
                }]
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"qldomainfield",
                label:"Domain",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            },{
                xtype:"qlpopupfield",
                name:"mypopup",
                label:"Popup",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            },]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"combobox",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select2</a>",
                editable:true,
                autoComplete:false,
                labelWrap:false,
                width:"50%",
                displayField: 'text',
                valueField: 'value',
                store:[{
                    text: "Pilihan A",value:"A"
                },{
                    text: "Pilihan B",value:"B"
                }]
            },{
                xtype:"combobox",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select</a>",
                name:"Select",
                editable:false,
                autoSelect:true,
                forceSelection:true,
                labelWrap:false,
                width:"50%",
                displayField: 'text',
                valueField: 'value',
                store:[{
                    text: "Pilihan A",value:"A"
                },{
                    text: "Pilihan B",value:"B"
                }]
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"qltextnumberfield",
                label:"TextNumber",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                allow:[",","-","."]
            },{
                xtype:"timefield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Time.html' target='_blank'>Timefield</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                format:'H:i',
                value:"13:00"
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'togglefield',
                labelWrap:false,
                width:"50%",
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Toggle.html" target='_blank'>Toggle</a>`,
                value: true
            },{
                xtype: 'sliderfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Slider.html" target='_blank'>Slider</a>`,
                labelWrap:false,
                width:"50%",
                value: 20,
            }]
        },
        
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'textareafield',
                labelWrap:false,
                width:"50%",
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.TextArea.html" target="_blank">Textarea</a>`,
            },{
                xtype: 'searchfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Search.html" target="_blank">Searchfield</a>`,
                flex: 1,
                maxWidth: '50%',
                maxHeight:"38px",
                shadow: false,
                placeholder: 'Search',
            }]
        },
        // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'textfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Text.html" target="_blank">Textfield</a>`,
                flex: 1,
                maxWidth: '50%',
                placeholder: 'Text',
            },{
                xtype:"numberfield",
                label:"Number2",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            }]
        },
        // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:'containerfield',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan1</a>`,
                width:"50%",
                items: [{
                    xtype: 'numberfield',
                    flex: 1,
                    maxWidth: '48%',
                    shadow: false,
                    placeholder: 'Angka',
                    style:"margin-left:5px;margin-right:10px"
                },{
                    xtype: 'selectfield',
                    flex: 1,
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "CM",value:"cm"
                    },{
                        text: "KM",value:"km"
                    }],
                    value:"cm"
                }]
            },{
                xtype:'containerfield',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan2</a>`,
                width:"50%",
                items: [{
                    xtype: 'numberfield',
                    maxWidth: '35%',
                    placeholder: 'Angka',
                    style:"margin-right:10px"
                },{
                    xtype: 'label',
                    maxWidth: '25%',
                    html: 'Satuan',
                    style:"margin-right:10px;margin-top:10px;font-weight: bold;"
                },{
                    xtype: 'selectfield',
                    maxWidth: '30%',
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "CM",value:"cm"
                    },{
                        text: "KM",value:"km"
                    }],
                    value:"cm"
                },{
                    xtype: 'label',
                    html: 'Mantap',
                    style:"margin-left:10px;margin-top:10px;font-weight: bold;"
                },]
            }]
        },
        // =========================
        {
            xtype:'label',
            html : "<h3>Separator Saja</h3>"
        },
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:'containerfield',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan1</a>`,
                width:"50%",
                items: [{
                    xtype: 'numberfield',
                    flex: 1,
                    maxWidth: '48%',
                    shadow: false,
                    placeholder: 'Angka',
                    style:"margin-left:5px;margin-right:10px"
                },{
                    xtype: 'selectfield',
                    flex: 1,
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "CM",value:"cm"
                    },{
                        text: "KM",value:"km"
                    }],
                    value:"cm"
                }]
            },{
                xtype:'container',
                layout:'hbox',
                width:"50%",
                items: [{
                    xtype: 'selectfield',
                    flex: 1,
                    maxHeight:"38px",
                    shadow: false,
                    width: "27%",
                    maxWidth: '130px',
                    displayField: 'text',
                    valueField: 'text',
                    store:[{
                        text: "Mr",value:"mr"
                    },{
                        text: "Mrs",value:"mrs"
                    }],
                    value:"mr"
                },{
                    xtype: 'searchfield',
                    flex: 1,
                    maxHeight:"38px",
                    maxWidth: '32%',
                    shadow: false,
                    placeholder: 'Search',
                    style:"margin-left:5px;margin-right:10px"
                }, {
                    xtype: 'button',
                    height: 32,
                    iconCls: 'x-fa fa-arrow-right',
                    style:"margin-top:5px;margin-right:10px"
                },{
                    xtype: 'button',
                    height: 32,
                    iconCls: 'x-fa fa-arrow-left',
                    ui:'action',
                    style:"margin-top:5px;margin-right:10px"
                }]
            }]
        },

      ]
    }]
});

Ext.define("QL.FieldTester",{});