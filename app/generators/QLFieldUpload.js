Ext.define('QL.imagefield', {
    extend:"Ext.field.Container",
    xtype:"qluploadfield",
    field:false,
    readOnly:false,
    required:false,
    model:null,
    getModel:function(){
        return this.model;
    },
    setModel:function(model){
        this.model=model;
    },
    label: '',
    required:false,
    hidden: false,
    accept:null,
    propertyResponse : null,
    propertyFile : "file",
    propertyAdditional:{},
    items:[{
        xtype: 'progress',
        shadow: false,
        value:0,
        width:"80%",
        flex: 1,
        hidden:true
    },{
        xtype:"button", iconCls:"x-fa fa-window-close",
        ui:"alt decline",
        tooltip:"reset",
        style:"margin-left:2px;margin-top:2px;max-width:33px",
        hidden:true,
        disabled:false,
        handler:function(el){
            el.up("qluploadfield").down("progress").setHidden(true);
            el.setHidden(true);
            el.up("qluploadfield").getItems().items[2].setHidden(true);
            el.up("qluploadfield").down("filefield").reset();
            el.up("qluploadfield").down("filefield").setHidden(false);
            el.up('qluploadfield').down('hiddenfield').reset();
        }
    },{
        hidden:true,
        xtype:"button", iconCls:"fas fa-search-plus",
        tooltip:"preview",
        ui:'confirm',
        disabled:false,
        style:"margin-left:2px;margin-top:2px;max-width:33px",
        handler:function(el){
            let qluploadfield = el.up('qluploadfield');
            var x = Ext.create({
                xtype:'dialog',
                title: "Current Image",
                maximizable: false,
                closable:true,
                dismissHandler:function(){
                    this.hide();
                },
                shadow:true,
                modal:true,
                minWidth:500,
                minHeight:300,
                items:[{
                    xtype: 'panel',
                    hidden:true,
                    html: ``,
                    style:{
                        padding:10, "background-color":"white"
                    },
                    afterRender:function(){
                        var filefield=el.up("qluploadfield").down("filefield");
                        if( filefield.getFiles().length>0 && (filefield.getFiles()[0].type).includes("image")){
                            this.setHtml(`<canvas width=500 height=300></canvas>`);
                            var canvas = this.renderElement.dom.getElementsByTagName("canvas")[0];
                            var ctx=canvas.getContext("2d");
                            var maxW=canvas.width;
                            var maxH=canvas.height;
                            var img = new Image;
                            img.onload = function() {
                                    var iw=maxW;
                                    var ih=maxW;
                                    var scale=Math.min((maxW/iw),(maxH/ih));
                                    var iwScaled=iw*scale;
                                    var ihScaled=ih*scale;
                                    ctx.drawImage(img,0,0,maxW,maxH);
                                    // output.value = canvas.toDataURL("image/jpeg",0.5);
                            }
                            img.src = URL.createObjectURL(filefield.getFiles()[0]);
                            this.setHidden(false);
                            this.up().down("image").destroy();
                            this.up("dialog").setTitle("Accepted File...")
                        }else if( filefield.getFiles().length>0 && !(filefield.getFiles()[0].type).includes("image")){
                            this.setHtml(`<center><h1>File: ${ filefield.getFiles()[0].name }</h1></center>`);
                            this.setHidden(false);
                            this.up().down("image").destroy();
                            this.up("dialog").setTitle("Accepted File...")
                        }else{
                            let imagePanel = this.up().down("image");
                            if( qluploadfield['read']!==undefined && typeof(qluploadfield['read'])=='function' ){
                                let val = qluploadfield.read(qluploadfield.down("hiddenfield").getValue());
                                imagePanel.setSrc(val);
                            }else{
                                imagePanel.setSrc(qluploadfield.down("hiddenfield").getValue());
                            }
                            imagePanel.setHidden(false);
                        }
                    }
                },{
                    xtype:'image',
                    height: 280,
                    width: 500,
                    style:{
                        padding:10, "background-color":"white"
                    },
                    listeners:{
                        error:function(el,e){
                            try{
                                window.open(el.getSrc());
                                el.up("dialog").destroy();
                            }catch(e){}
                        }
                    }
                }]
            });
            x.show();
        }
    },{
        xtype:"hiddenfield",
        listeners:{
            blur:function(){
                if(this.getValue()===null || this.getValue()==""){return}
                let loading = this.up("qluploadfield").down("progress");
                let buttonReset = this.up("qluploadfield").getItems().items[1];
                let buttonView = this.up("qluploadfield").getItems().items[2];
                let filefield = this.up("qluploadfield").getItems().items[4];
                buttonView.setHidden(false);
                buttonReset.setHidden(false);
                buttonView.setDisabled(false);
                buttonReset.setDisabled(false);
                loading.setHidden(false);
                loading.setValue(1);
                loading.setText('File Exist');

                filefield.setHidden(true);
            }
        },
        afterRender:function(){
            let name = this.up('qluploadfield').name;
            this.up('qluploadfield').setName(null);
            this.setName(name+"_url");
            let buttonView = this.up("qluploadfield").getItems().items[2];
            buttonView.setIconCls(this.up('qluploadfield').accept=='image'?'x-fa fa-file-image':'x-fa fa-file-download')
        },
    },{
        xtype   : "filefield",
        width: "100%",
        // flex: self.getCols()==1?null:1,
        // readOnly: false,
        // disabled: column.disabled,
        // hidden: column.hidden,
        // required:column.required,
        // labelWidth: self.getLabelWidth(),
        // accept  : "image",
        // maxSize : 10,
        afterRender:function(){
            this.getFileButton().setConfig({
                iconCls: "fas fa-upload",
                text : null,
                ui:"confirm",
                accept:this.up('qluploadfield').accept,
                style:"margin-right:5px",
                disabled:this.up('qluploadfield').readOnly,
                listeners:{
                    tap:function(el,e,opt){
                        if(this.up('qluploadfield').getModel()==null){
                            Ext.Msg.alert('Warning',"please set the model to upload");
                            return false;
                        }
                    }
                }
            });
        },
        listeners:{
            change : function(el,v){
                let loading = el.up("qluploadfield").down("progress");
                let qluploadfield = el.up('qluploadfield');
                let buttonReset = el.up("qluploadfield").getItems().items[1];
                let buttonView = el.up("qluploadfield").getItems().items[2];
                var formData = new FormData();
                el.setHidden(true);

                if(qluploadfield.model===undefined){
                    Ext.Msg.alert("Warning!",`model property must be set`);
                    return false;
                }
                if(el.getValue()===""||el.getValue()===null){return true}
                if(qluploadfield['maxSize']!==undefined && el.getFiles().length>0){
                    let max = qluploadfield.maxSize * 1000000;
                    if(el.getFiles()[0].size>max){
                        el.reset();
                        buttonReset._handler(buttonReset);
                        Ext.Msg.alert("Disallowed!",`Sorry, max size must be less than ${qluploadfield.maxSize} MB`);
                        el.setError(`Sorry, max size must be less than ${qluploadfield.maxSize} MB`);
                        return false;
                    }
                }
                loading.setHidden(false);
                for(key in qluploadfield.propertyAdditional){
                    formData.append(key, qluploadfield.propertyAdditional[key] );
                }
                formData.append(qluploadfield.propertyFile, el.getFiles()[0] );
                api.createUpload({
                    model:qluploadfield.model,
                    data: formData
                },function(json){
                    buttonView.setHidden(false);
                    buttonReset.setHidden(false);
                    buttonView.setDisabled(false);
                    buttonReset.setDisabled(false);
                    let response = qluploadfield.propertyResponse===undefined||qluploadfield.propertyResponse===null?json:json[qluploadfield.propertyResponse];
                    qluploadfield.down('hiddenfield').setValue(response);
                    loading.setText(el.getFiles()[0].name);
                },function(failed){
                    Ext.Msg.alert("Failed!","error");
                    buttonReset._handler(buttonReset);
                },function(progress){
                    if (progress.lengthComputable)
                        {
                            var percentComplete = (progress.loaded / progress.total);
                            loading.setValue(percentComplete);
                            loading.setText(` uploading ${parseInt(percentComplete*100)}%`);
                            // console.log(percentComplete);
                            // if(percentComplete==1){
                            // }
                        }
                },false);
            }
        },
    }]
})