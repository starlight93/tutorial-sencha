Ext.define('QL.Helper',{
    constructor:function(){
        return this;
    },
    MenuArray:[],
    Date:function(){
        let tanggal = new Date();
        return `${tanggal.getDate()}/${tanggal.getMonth()+1}/${tanggal.getFullYear()}`;
    },
    formatUSD:function(value){
        Ext.util.Format.thousandSeparator=",";
        return Ext.util.Format.usMoney(value);
    },
    formatIDR:function(value){
        Ext.util.Format.thousandSeparator=".";
        return Ext.util.Format.currency(value,"Rp",".",""," ")
    },
    thousand:function(value,prefix="",postfix=""){
       return prefix+(parseFloat(parseFloat(value).toFixed(2))).toLocaleString(undefined,{ minimumFractionDigits: 2 }) + postfix;
        // Ext.util.Format.thousandSeparator=".";
        // return Ext.util.Format.number(value,"0,000.00")
    },
    upperWord:function(str,splitter="_",toSplitter=" "){
        var sentence = "";
        (str.split(splitter)).forEach(function(word){
            sentence+= (sentence!==""?toSplitter:"")+word.toLowerCase().charAt(0).toUpperCase() + word.substr(1).toLowerCase();
        });
        return sentence;
    },
    textOrNumber:function(value){
        return Ext.Number.from(value,null)==null?value:this.thousand(value);
    },
    toUpperCase:function(value){
        return value.toUpperCase();
    },
    toLowerCase:function(value){
        return value.toLowerCase();
    },
    toDateServer:function(value){
        let data = value.split("/");
        return data[2]+"-"+data[1]+"-"+data[0];
    },
    getNameToday: function(datestring = null){
        var presentDate;

        presentDate = (datestring === null || datestring === undefined)
                    ? new Date()
                    : new Date(datestring);

        let weekday = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'][ presentDate.getDay() ];

        return weekday;
    },
    toNumber:function(value){
        return value == null ? 0 : parseInt(value);
    },
    registerMenu:function(config){
        console.log(config)
        this.MenuArray.push(config);
    },
    registerMenuExecution:function(){
        let menuArray = this.MenuArray;
        let menuSidebar={};
        let subModul = [];
        menuArray.forEach(function(dt){
            if( typeof(dt.modul)=='object' ){
                qlconfig_sidebar.push( dt.modul );
            }
            if( typeof(dt.submodul)=='object' ){
                subModul.push( dt );
            }
            if( typeof(dt.data)=='object' ){
                if(menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase()]===undefined){
                    menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase()]=[];
                }
                menuSidebar[ (dt.modul).toLowerCase()+'-'+(dt.submodul).toLowerCase() ].push(dt.data);
            }
        });
        
        subModul.forEach(function(dt){
            if( typeof(dt.submodul)=='object' ){
                let modulIndex = qlconfig_sidebar.findIndex(function(dtSidebar){
                    return (dtSidebar.text).toLowerCase() == (dt.modul).toLowerCase();
                });
                qlconfig_sidebar[modulIndex].children.push(dt.submodul);
            }
        });
        console.log(subModul,menuArray)
        console.log(menuSidebar)
        console.log(qlconfig_sidebar)
        for(let label in menuSidebar){
            menuSidebar[label].sort( function(a,b){return a.seq-b.seq } );
            let modul = label.split('-')[0];
            let submodul = label.split('-')[1];
            let modulIndex = qlconfig_sidebar.findIndex(function(dt){
                return (dt.text).toLowerCase() == modul;
            });
            let subModulIndex = (qlconfig_sidebar[modulIndex].children).findIndex(function(dt){
                return (dt.text).toLowerCase() == submodul;
            });
            qlconfig_sidebar[modulIndex].children[subModulIndex].children = menuSidebar[label];
        }
        qlconfig_sidebar.sort( function(a,b){return a.seq-b.seq } );
        qlconfig_sidebar.forEach(function(dataSideBar,i){
            qlconfig_sidebar[i].children.sort( function(a,b){return a.seq-b.seq } );
        })
    }
});