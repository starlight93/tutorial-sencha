
Ext.create({
    xtype:'dialog',
    maximizable: false,
    draggable:false,
    title:"Loading Bill Of Materials....",
    // closable:false,
    id: "loadingDialogBOM",
    shadow:false,
    modal:true,
    style:{
        background:'transparent'
    },
    cls:"customLoader",
    width:300,
    viewModel:{
        progress:0.0
    },
    html: `<div class="loader"></div>`,
    listeners:{
        beforeshow:function(){
            // console.log('show');
        }
    }
});
Ext.create({
    xtype:'dialog',
    maximizable: false,
    draggable:false,
    title:"Generating Report....",
    // closable:false,
    id: "loadingDialogReport",
    shadow:false,
    modal:true,
    style:{
        background:'transparent'
    },
    cls:"customLoader",
    width:300,
    viewModel:{
        progress:0.0
    },
    html: `<div class="loader"></div>`,
    listeners:{
        beforeshow:function(){
            // console.log('show');
        }
    }
});
Ext.create({
    xtype:'dialog',
    maximizable: false,
    // closable:false,
    id: "loadingDialog",
    shadow:false,
    modal:true,
    style:{
        background:'transparent'
    },
    cls:"customLoader",
    width:300,
    viewModel:{
        progress:0.0
    },
    html: `<div class="loader"></div>`,
    listeners:{
        beforeshow:function(){
            // console.log('show');
        }
    }
});
Ext.create({
    xtype:'dialog',
    title:false,
    maximizable: false,
    closable:false,
    id: "QLReportPopupV2",
    shadow:true,
    modal:true,
    minWidth:950,
    minHeight:500,
    showData:function(data){
        this.getItems().items[0].pdfRefresh(data);
        this.show();
    },        
    buttons: {
        close: function(){
            this.up("dialog").hide();
        },
    },
    items:[{
        xtype: "QLPDFReportV2",
        height:490,
        withButtons:false
    }]
});

Ext.create({
    xtype:'dialog',
    title:false,
    maximizable: false,
    closable:false,
    id: "QLPreview",
    cls : "QLPreview",
    shadow:true,
    modal:true,
    minWidth:950,
    minHeight:510,
    showData:function(data){
        this.getItems().items[0].pdfRefresh(data);
        this.show();
    },        
    buttons: {
        close: function(){
            this.up("dialog").hide();
        },
    },
    items:[{
        xtype: "QLPreview",
        height:500,
        withButtons:false
    }]
});
Ext.create({
    xtype:'dialog',
    title:false,
    maximizable: false,
    closable: true,
    id: "QLPrint",
    cls : "QLPreview",
    shadow:true,
    modal:true,
    minWidth:950,
    minHeight:510,
    jsonData:{},
    postHandler:function(){},
    dismissHandler:function(){
        this.hide();
    },
    showData:function(data){
        this.jsonData=data;
        this.getItems().items[0].pdfRefresh(data);
        this.show();
    },
    items:[{
        xtype: "QLPreview",
        height:500,
        withButtons:false
    },{
        xtype :"formpanel",
        buttons:[{
            text:"cetak", iconCls:"x-fa fa-print",
            handler:function(el){
                // Ext.Msg.confirm("Warning", 'Pencetakan akan dihitung, Apakah anda yakin akan mencetak dokumen ini?', function (buttonId) {
                //     if((buttonId).toLowerCase()=='yes'){
                        let dialog = el.up("dialog");
                        try{
                            dialog.getItems().items[0].print(el.up("dialog").jsonData,function(){
                                dialog.hide();
                                dialog.postHandler();
                            });                            
                        }catch(e){
                            dialog.hide();
                        };
                //     }
                // });
            }
        }]
    }]
});
Ext.create({
    xtype:'dialog',
    title:false,
    maximizable: false,
    closable:true,
    dismissHandler:function(){
        // this.transaction_reason=null;
        this.hide();
        Ext.Msg.setWidth(250);
    },
    id: "QLApproval",
    cls : "QLPreview",
    shadow:true,
    modal:true,
    minWidth:950,
    minHeight:510,
    textButton:"Approving",
    transaction_id:null,
    transaction_api:null,
    transaction_success:function(){},
    transaction_payload:{},
    transaction_payload_fix:{},
    transaction_reason:null,
    submission:function(el){
        api.changeStatus(
            {
                model   : this.transaction_api,
                id      : this.transaction_id,
                data    : this.transaction_payload_fix
            }, 
            function(json)
            {
                el.up("dialog").hide();
                Ext.Msg.alert('Success', `${el.up("dialog").textbutton} Success`, Ext.emptyFn);
                el.up("dialog").transaction_success();
            },function(arrayError){
                el.up("dialog").hide();
                Ext.Msg.alert('Failed', 'Some errors happened unexpectedly', Ext.emptyFn);
            })
    
        },
    showData:function(data){
        this.getItems().items[0].pdfRefresh(data);
        this.show();
    },
    items:[{
        xtype: "QLPreview",
        height:500,
        withButtons:false
    },{
        xtype :"formpanel",
        buttons:[
            {
                text:"Approve",
                id:"QLApproveBtn",
                ui: 'alt confirm',
                handler:function(el){                
                Ext.Msg.confirm("Confirmation", 'Are you sure to approve this transaction?', function (buttonId) {
                    if((buttonId).toLowerCase()=='yes'){
                        el.up("dialog").textbutton='Approving';
                        el.up("dialog").transaction_payload_fix={};
                        el.up("dialog").transaction_payload_fix[el.up("dialog").transaction_payload['approveKey']]=el.up("dialog").transaction_payload['approveValue'];
                        el.up("dialog").submission(el);
                        
                    }
                });                  
                }
            },{
            text:"Revise",
            id:"QLReviseBtn",
            handler:function(el){
                    Ext.Msg.show({
                        title: 'Warning!',
                        message: '[REVISE] Type your reason:',
                        width: 475,
                        buttons: Ext.MessageBox.OKCANCEL,
                        multiLine: true,
                        prompt : { maxlength : 200, autocapitalize : false },
                        fn: function(buttonId,value) {
                            if( (buttonId).toLowerCase()=='ok'){
                            el.up("dialog").textbutton='Revise Assigning';
                            el.up("dialog").transaction_payload_fix={};
                            el.up("dialog").transaction_payload_fix[el.up("dialog").transaction_payload['reviseKey']]=el.up("dialog").transaction_payload['reviseValue'];
                            el.up("dialog").transaction_payload_fix[el.up("dialog").transaction_payload['reviseReason']]=value;                                
                            el.up("dialog").submission(el);
                            }
                            Ext.Msg.setWidth(250);
                        }
                    });                
            }
        },{
            text:"Reject",
            id:"QLRejectBtn",
            ui: 'alt decline',
            handler:function(el){
                    Ext.Msg.show({
                        title: 'Warning!',
                        message: '[REJECT] Type your reason:',
                        width: 500,
                        buttons: Ext.MessageBox.OKCANCEL,
                        multiLine: true,
                        prompt : { maxlength : 200, autocapitalize : false },
                        fn: function(buttonId,value) {
                            if( (buttonId).toLowerCase()=='ok'){
                            el.up("dialog").textbutton='Reject';
                            el.up("dialog").transaction_payload_fix={};
                            el.up("dialog").transaction_payload_fix[el.up("dialog").transaction_payload['rejectKey']]=el.up("dialog").transaction_payload['rejectValue'];
                            el.up("dialog").transaction_payload_fix[el.up("dialog").transaction_payload['rejectReason']]=value; 
                            el.up("dialog").submission(el);
                            }
                            Ext.Msg.setWidth(250);
                        }
                    });
                }
        }
        ]
    }]
});

var findParentModel = function(el) {
    var parent = el.getParent();
    if(parent===null || parent===undefined){
        return null;
    }
	if (parent.getViewModel() != undefined && parent.getViewModel() != null ) {
        return parent.getViewModel();
    } else {
        return findParentModel(parent);
    }
};
function getStoreData(storeId){
    var data = [];
    try{
        var rawData = Ext.getStore(storeId).getData().items;
        rawData.forEach(dt=>{
            data.push(dt.data);
        });
    }catch(e){}
    return data;
}
var keyPress =(e)=>{
    if(e.key === "Escape") {
        if(confirm('refresh anda setelah ini akan ke home?')){
            sessionStorage.clear();
            window.location.replace("/");
        }
    }
}
var updateHeight=(element)=>{
    try{
        var tabpanel = element.up("navigationview").getItems().items[2].getItems().items[1];
        tabpanel.getItems().items.forEach(function(children){
            let grid = children.down("grid");
            if(grid!==null){
                let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                let gridHeightBody   = 0;
                let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                Array.from(rows).forEach(function(row){
                    gridHeightBody+=row.offsetHeight;
                });
                grid.setHeight(gridHeightHeader+gridHeightBody+40);
            }
        });
        tabpanel.afterRender();
    }catch(e){
        console.log(e);
    }
}
var field = (name)=>{
    try{
        return Ext.getCmp(name.split(".")[0]).getFields(name.split(".")[1]);
    }catch(e){return null;}
}
Ext.define("QL.Others",{});