Ext.define('QL.exporterGrid', {
    override: 'Ext.exporter.Base',
    applyTitle: function(title) {
        return title ? 'EXCEL REPORT - ' + title : title;
    }
});
Ext.define("QL.overrideComponent",{
    override: "Ext.Component",

    beforeInitConfig: function(config) {
        if( this.xtype!==undefined && (this.xtype=='formpanel' || (this.xtype).includes("field")) ){
            if(document.getElementById(this.getId()) !==null ){
                Ext.getCmp(this.getId()).destroy();
            }
        }
        this.beforeInitialize.apply(this, arguments);
    },
});
Ext.define("QL.overrideFormPanel",{
    override:"Ext.form.Panel",
    fieldSet:[],
    reset: function(clearInvalid,afterRender=false) {
        this.getFields(false).forEach(function(field) {
            if(field["remember"]!==undefined && field["remember"]===true){
            }else{
                let canRemove=true;
                if(field.xtype=='checkbox'){
                    let group = field.up("checkboxgroup");
                    if(group!==undefined && group['remember']!==undefined && group['remember']===true ){
                        canRemove=false;
                    }
                }
                if(canRemove){
                    if( ['combobox','selectfield'].includes(field.xtype) ){
                        field.clearValue();
                        field.setInputValue(null);
                    }else{
                        field.reset();
                    }
                    if (clearInvalid) {
                        field.setError(null);
                    }
                    if(afterRender){
                        try{field.afterRender()}catch(e){}
                    }
                }
            }
        });
        return this;
    },
    resetExcept: function(array) {
        this.getFields(false).forEach(function(field) {
            if( !array.includes(field.getName()) ){
                if( ['combobox','selectfield'].includes(field.xtype) ){
                    field.clearValue();
                    field.setInputValue(null);
                }else{
                    field.reset();
                }
                field.setError(null);
            }
        });

        return this;
    },
    resetOnly: function(array) {
        this.getFields(false).forEach(function(field) {
            if( array.includes(field.getName()) ){
                if( ['combobox','selectfield'].includes(field.xtype) ){
                    field.clearValue();
                    field.setInputValue(null);
                }else{
                    field.reset();
                }
                field.setError(null);
            }
        });

        return this;
    },
    setReadOnly:function(isReadOnly=true,fieldsArray=[]){
        var fields = this.getFields();
        var keys = Object.keys(fields);
        if(fieldsArray.length>0){
            keys=keys.filter(dt=>fieldsArray.includes(dt));
        }
        keys.forEach(function(key){
            let field = fields[key];
            if(Array.isArray(field)){
                if(field.length>0){
                    if(field[0]['xtype']!==undefined && ["radiogroup","checkboxgroup"].includes(field[0].xtype)){
                        var radios = field[0].getItems().items;
                        radios.forEach(function(radio){
                            radio.setDisabled(isReadOnly);
                        });
                    }
                }
            }else if(["checkboxfield","checkboxgroup"].includes(field["xtype"])){
                field.setDisabled(isReadOnly);
            }else{
                try{
                    field.setReadOnly(isReadOnly);
                }catch(e){}
            }
        });
    },
     getValues: function(options, nameless) {
        var fields = this.getFields(),
            values = {},
            isArray = Ext.isArray,
            enabled = options,
            field, value, addValue, bucket, name, ln, i, serialize;

        if (Ext.isObject(options)) {
            enabled = options.enabled;
            nameless = options.nameless;
            serialize = options.serialize;
        }

        // Function which you give a field and a name, and it will add it into the values
        // object accordingly
        addValue = function(field, name) {
            if ((!nameless && (!name || name === 'null')) || field.isFile) {
                return;
            }

            // Checkboxes have a "value" but it is behaves differently than regular
            // fields. When the checkbox is checked, its value is returned and otherwise
            // it returns null. By default, that value is true. This is handled by
            // serialize/getSubmitValue so we always need to call it for checkboxes.
            // value = (serialize || field.isCheckbox) ? field.serialize() : field.getValue();
            if (serialize || field.isCheckbox) {
                value = field.serialize();
            } else if(typeof field.getDateFormat === "function" && field.getDateFormat()) {
                value = Ext.Date.format( field.getValue(), field.getDateFormat() );
            }else if(field.isXType("timefield")){
                value = field.getInputValue();
                if( qlconfig['auto_uppercase'] !==undefined && (qlconfig['auto_uppercase']===true) ){
                    try{value = value.toUpperCase();}catch(e){}
                };
            }else {
                value = field.getValue();
                if( field['xtype']!==undefined && (field.xtype).includes("qlnumberfield")){
                    if( isNaN(parseFloat(value)) ){
                        value = 0;
                    }else{
                        value=Ext.Number.from(value, value);
                    }
                }
                if( qlconfig['auto_uppercase'] !==undefined && (qlconfig['auto_uppercase']===true) ){
                    try{value = value.toUpperCase();}catch(e){}
                };
            }
            if (!(enabled && field.getDisabled())) {
                // RadioField is a special case where the value returned is the fields valUE
                // ONLY if it is checked
                if (field.isRadio) {
                    if (field.isChecked()) {
                        values[name] = value;
                        if( qlconfig['auto_uppercase'] !==undefined && (qlconfig['auto_uppercase']===true) ){
                            try{value = value.toUpperCase();}catch(e){}
                        };
                    }
                }
                else {
                    // Check if the value already exists
                    bucket = values[name];

                    if (!Ext.isEmpty(bucket)) {
                        if (!field.isCheckbox || field.isChecked()) {
                            // if it does and it isn't an array, we need to make it into an array
                            // so we can push more
                            if (!isArray(bucket)) {
                                bucket = values[name] = [bucket];
                            }

                            // Check if it is an array
                            if (isArray(value)) {
                                // Concat it into the other values
                                bucket = values[name] = bucket.concat(value);
                            }
                            else {
                                // If it isn't an array, just pushed more values
                                bucket.push(value);
                            }
                        }
                    }
                    else {
                        values[name] = value;
                        if( qlconfig['auto_uppercase'] !==undefined && (qlconfig['auto_uppercase']===true) ){
                            try{value = value.toUpperCase();}catch(e){}
                        };
                    }
                }
            }
        };

        // Loop through each of the fields, and add the values for those fields.
        for (name in fields) {
            if (fields.hasOwnProperty(name)) {
                field = fields[name];

                if (isArray(field)) {
                    ln = field.length;

                    for (i = 0; i < ln; i++) {
                        if(field[i]['xtype']!==undefined && field[i]['xtype']=='checkboxgroup'){
                            continue;
                        }
                        addValue(field[i], name);
                    }
                    // if(values[name])
                }
                else {
                    addValue(field, name);
                }
            }
        }
        for(name in values){
            if( isArray(fields[name])){
                if(!isArray(values[name])){
                    values[name] = [values[name]];
                }
                if( values[name][0] === false ){
                    values[name].shift();
                }
                if(fields[name][0]['xtype']!==undefined && fields[name][0]['xtype']=="radiogroup"){
                    values[name] = values[name][0]===undefined?"":values[name][0];
                }
            }else{
                if(fields[name]['xtype']!==undefined && fields[name]['xtype']=="combobox" && name.includes("_id")){
                    try{
                        values[name.replace("_id","")]=fields[name].getRawValue();
                    }catch(e){}
                }
            }
        }

        return values;
    },
    /* @return {Ext.field.Manager} this
    */
    setValues: function(values, loadStore=true) {
        let form= this;
        form['fieldSet']=Object.keys(values);
        var fields = this.getFields(),
            name, field, value, ln, i, f;

        values = values || {};
        for (name in values) {
            if (values.hasOwnProperty(name)) {
                field = fields[name];
                value = values[name];
                if(Ext.isArray(field) && typeof(value)=="string" && value.includes(",")){
                    value = value.split(",");
                }
                if (field) {
                    // If there are multiple fields with the same name. Checkboxes, radio
                    // fields and maybe event just normal fields..
                    if (Ext.isArray(field)) {
                        ln = field.length;

                        // Loop through each of the fields
                        for (i = 0; i < ln; i++) {
                            f = field[i];

                            if (f.isRadio) {
                                // If it is a radio field just use setGroupValue which
                                // will handle all of the radio fields
                                f.setGroupValue(value);
                                break;
                            }
                            else if (f.isCheckbox) {
                                if (Ext.isArray(value)) {
                                    f.setChecked(value.indexOf(f._value) !== -1);
                                }
                                else {
                                    f.setChecked(value === f._value);
                                }
                            }
                            else {
                                // If it is a bunch of fields with the same name, check
                                // if the value is also an array, so we can map it to
                                // each field
                                if (Ext.isArray(value)) {
                                    f.setValue(value[i]);
                                }
                            }
                        }
                    }
                    else {
                        if (field.isRadio || field.isCheckbox) {
                            // If the field is a radio or a checkbox
                            field.setChecked(value);
                        }
                        else {
                            // If just a normal field
                            if( (field.xtype).includes("combobox")){
                                try{
                                    if( (field.getStore().loadCount<1 || value!=field.getValue()) && loadStore===true && field.getStore().getProxy().type=="ajax" ){
                                        if(field!==undefined){
                                            field['gelondongan']=form['fieldSet'];
                                        }
                                        field.getStore().load(function(thisRecords,operation){
                                            if(field!==undefined){
                                                field.setValue(value);
                                            }
                                        });
                                    }
                                }catch(e){}
                            }
                            if( (field.xtype).includes("qlnumberfield") ){
                                value=parseFloat(value);
                            }
                            field.setValue(value);
                            if( (field.xtype).includes("number") ){
                                field.onBlur();
                            }else if( ['hiddenfield'].includes(field.xtype)||((field.xtype).includes("textfield") && field['isChildCombo']===undefined) ){
                                field.onBlur();
                            }else if( (field.xtype).includes("combobox") ){
                                if(fields[ name.replace("_id","") ] !==undefined){
                                    fields[ name.replace("_id","") ].setValue(field.getInputValue());
                                }
                                try{field.fireEvent("change");field.fireEvent("select");}catch(e){}
                            }else if( (field.xtype).includes("filefield") ){
                                try{fields[field.getName()+"_url"].setValue(value);}catch(e){}
                            }else{
                                if(field['isChildCombo']!==undefined && fields[field.getName()+"_id"]!==undefined && values[field.getName()+"_id"]===undefined){
                                    var name = field.getName();
                                    var parent = fields[name+"_id"];
                                    if(parent!==undefined && parent.getValueField()=='value' ){
                                        parent.setValue( value );
                                    }else if(parent!==undefined){
                                        var key = parent.getDisplayField();
                                        try{
                                            if( loadStore===true  && parent.getStore().getProxy().type=="ajax"  && parent.getValue()!=value){
                                                parent.getStore().load(function(thisRecords,operatoin){
                                                    for(let y=0; y<thisRecords.length;y++){
                                                        if(thisRecords[y]['data'][key] == value){
                                                            parent.setValue( thisRecords[y]['data'][parent.getValueField() ] );
                                                            break;
                                                        };
                                                    }
                                                });
                                            }
                                        }catch(e){}
                                    }
                                }
                            }
                        }
                    }

                    if (this.getTrackResetOnLoad && this.getTrackResetOnLoad()) {
                        field.resetOriginalValue();
                    }
                }
            }
        }
        try{findParentModel(this).notify();}catch(e){}
        return this;
    }
});
Ext.define('QL.checkboxgroupField', {
    override: 'Ext.field.CheckboxGroup',
    getValue: function() {
        var items = this.getGroupItems(),
            ln = items.length,
            values = {},
            item, name, value, bucket, b;

        for (b = 0; b < ln; b++) {
            item = items[b];
            name = item.getName();
            value = item.getValue();

            if (value && item.getChecked()) {
                if (values.hasOwnProperty(name)) {
                    bucket = values[name];

                    if (!Ext.isArray(bucket)) {
                        bucket = values[name] = [bucket];
                    }

                    bucket.push(value);
                }
                else {
                    values[name] = value;
                }
            }
        }

        return values;
    }
});
Ext.define('QL.checkboxField', {
    override: 'Ext.field.Checkbox',
    getSubmitValue: function() {
        return this.getChecked() ? Ext.isEmpty(this._value) ? true : this._value : false;
    },
});
Ext.define('QL.fieldInput',{
    override: 'Ext.field.Input',
    updateReadOnly: function(readOnly) {
        this.setInputAttribute('readonly', readOnly ? true : null);
        let el = this;
        if(el['xtype']!==undefined && el['xtype']=='combobox'){
            if(el.getReadOnly()){
                el.setCls('customreadonly');
            }else{
                el.setCls('customreadonlyfalse');
            }
        }else{
            try{
                let groupingItems = el.up().getItems().items;
                if(groupingItems.length>1){
                    groupingItems.forEach(item=>{
                        if(item['xtype']!==undefined && item['xtype']=='button'){
                            item.setDisabled(readOnly);
                        }else if(el['xtype']!==undefined && el['xtype']!=='combobox' && ( (el['cls']!==undefined && el['cls']=='qltextfield')) || ['checkboxfield','checkbox','radiofield'].includes(el['xtype']) ){
                            item.setReadOnly(readOnly);
                        }
                    });
                }
            }catch(e){}
        }
    },
    afterHide: function(callback, scope) {
        var me = this,
            container = me.ownerFocusableContainer;

        // Top level focusEnter is only valid when a floating component stack is visible.
        delete me.getInherited().topmostFocusEvent;

        me.hiddenByLayout = null;

        // Only lay out if there is an owning layout which might be affected by the hide
        if (me.ownerLayout) {
            me.updateLayout({ isRoot: false });
        }

        if (container && !container.onFocusableChildHide.$nullFn) {
            container.onFocusableChildHide(me);
        }

        // me.fireHierarchyEvent('hide');
        me.fireEvent('hide', me);

        // Have to fire callback the last, because it may destroy the Component
        // and firing subsequent events will become impossible. Strictly speaking,
        // hide event handler above could have destroyed the Component too, but
        // in such case it is the responsibility of the callback to accommodate.
        Ext.callback(callback, scope || me);
    },
})
Ext.define('QL.textfield',{
    override: 'Ext.field.Text',
    onKeyDown: function(event) {
        if (event.browserEvent.keyCode === 13) {
            event.preventDefault();
            return false;
        }
        var me = this,
            inputMask = me.getInputMask();
        me.lastKeyTime = Date.now();
        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }
        me.ignoreInput = true;

        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        }
        me.fireAction('keydown', [me, event], 'doKeyDown');
    },
});
Ext.define('QL.validatorRange',{
    override:'Ext.data.validator.Range',
    validateValue: function(value) {
        value  = parseFloat(value.replace(/,/g,""));
        var msg = this.callParent([value]);

        if (msg === true && isNaN(value)) {
            msg = this.getNanMessage();
        }
        return msg;
    },
    getValue:function(val){
       return parseFloat(val.replace(/,/g,""))
    }
});
Ext.define('QL.numberfield', {
    extend: 'Ext.field.Text',
    xtype: 'qlnumberfield',
    clearable:false,
    isInput:false,
    textAlign:"right",
    decimals:2,
    autoComplete:false,
    alternateClassName: 'Ext.form.NumberNew',
    onFocusLeave: function(event) {
        this.isInput=false;
        var me = this,
            inputMask = me.getInputMask();

        me.callParent([event]);

        me.removeCls(me.focusedCls);
        me.syncLabelPlaceholder(true);

        if (inputMask) {
            inputMask.onBlur(me, me.getValue());
        }
        if(this.getInputValue()!==null && this.getInputValue()!=""&& this.getInputValue()!="."){
            let rawValue =this.getInputValue();
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString('en');
            // this.setValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
            this.setInputValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
        }
    },
    onBlur: function() {
        this.isInput=false;
        if(this.getInputValue()!==null && this.getInputValue()!=""&& this.getInputValue()!="."){
            let rawValue =this.getInputValue();
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString('en');
            // this.setValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
            this.setInputValue( formattedVal+((rawValue).includes(".")?`.${decimal}`:"") );
            if(this.getInputValue()==NaN || this.getInputValue()=="NaN" ){
                this.setInputValue(0);
            }
        }
    },
    doKeyUp: function(me, e=null) {
        me.syncEmptyState();
        if(me.getInputValue()!==null && me.getInputValue()!=""&& me.getInputValue()!="."){
            let rawValue =me.getInputValue();
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"":(rawValue).split(".")[1];
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString('en');
            me.setInputValue(formattedVal+((rawValue).includes(".")?`.${decimal}`:""));
        }
        if (e!==null && e.browserEvent.keyCode === 13) {
            me.fireAction('action', [me, e], 'doAction');
        }
    },

    onKeyDown: function(event) {
        var me = this,
            inputMask = me.getInputMask();
        this.isInput=true;
        me.lastKeyTime = Date.now();

        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }

        // tell the class to ignore the input event. this happens when we want to listen
        // to the field change when the input autocompletes
        me.ignoreInput = true;
        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        }

        me.fireAction('keydown', [me, event], 'doKeyDown');
        let key = event.event.key;
        // console.log(event.event.key);
        if(key=="."){
            if( me.getValue()===null || (me.getValue().toString()).length==0 || (me.getValue().toString()).includes(".") ){
                event.preventDefault();
                return true;
            }
        }
        if( !(/^[0-9.]{1,}$/).test(key)
            && !["ArrowLeft","ArrowRight","Backspace","Delete","Tab"].includes(key)
            && !(event.event.ctrlKey===true && ["v","c","x","a"].includes(key)) ){

            event.preventDefault();
            return true;

        }
    },

    updateInputValue: function(value) {
        var inputElement = this.inputElement.dom;
        if (inputElement.value !== value) {
            inputElement.value = value;
        }

        if(!this.isInput&&value!==null &&value!=""&& value!="."){
            var me = this;
            let rawValue =value;
            var val,decimal;
            if( (rawValue).includes(".") ){
                val = ( (rawValue).split(".")[0] ).replace(/,/g,"");
                decimal = ((rawValue).split(".")[1]).length==0?"00000":(rawValue).split(".")[1];
                decimal=decimal.slice(0,this.decimals);
            }else{
                val = (rawValue).replace(/,/g,"");
                decimal="00";
            }
            let formattedVal = parseFloat(val).toLocaleString('en');
            me.setInputValue(formattedVal+((rawValue).includes(".")?`.${decimal}`:""));
            // inputElement.value=formattedVal+((rawValue).includes(".")?`.${decimal}`:"");
        }
    },
    updateValue: function(value, oldValue) {
        if (this.canSetInputValue()) {
            this.setInputValue(value);
        }
        this.callParent([value, value]);
    },

    applyValue: function(value) {
        if (this.isConfiguring) {
            this.originalValue = value;
        }
        value = Ext.Number.from(value, 0);
        return value;
    },

    applyInputValue: function(value) {
        try{
            this.setValue(value.replace(/,/g,""));
        }catch(e){}
        return (value != null) ? (value + '') : '';
    },

});
Ext.define('QL.textnumberfield', {
    extend: 'Ext.field.Text',
    xtype: 'qltextnumberfield',
    autoComplete:false,
    alternateClassName: 'Ext.form.TextNumberNew',
    allow:"",
    onKeyDown: function(event) {
        var me = this,
            inputMask = me.getInputMask();
        this.isInput=true;
        me.lastKeyTime = Date.now();
        if (inputMask) {
            inputMask.onKeyDown(me, me.getValue(), event);
        }
        me.ignoreInput = true;
        if (Ext.supports.SpecialKeyDownRepeat) {
            me.fireKey(event);
        }
        if( typeof( me.allow ) !== "object" ){
            me.allow="Tab,"+me.allow;
            me.allow = (me.allow).split(",");
        }
        (me.allow).push("Tab");
        me.fireAction('keydown', [me, event], 'doKeyDown');
        let key = event.event.key;
        if( !(me.allow).includes(key)){
            if( !(/^[0-9]{1,}$/).test(key)
                && !["ArrowLeft","ArrowRight","Backspace","Delete"].includes(key)
                && !(event.event.ctrlKey===true && ["v","c","x","a"].includes(key)) ){
                event.preventDefault();
                return true;
            }
        }
    }
});
Ext.define('QL.overrideComboBox',{
    override: "Ext.field.ComboBox",
    onFocus: function(e) {
        var me = this, container = me.ownerFocusableContainer;
        if(me['hasDependant']!==undefined  && me.getEditable()===true){
            me.getStore().load();
        }
        me["oldValue"]=me.getValue();
        if (me.canFocus()) {
            if (me.beforeFocus && !me.beforeFocus.$emptyFn) {
                me.beforeFocus(e);
            }
            if (container) {
                container.beforeFocusableChildFocus(me, e);
            }
            me.addFocusCls(e);
            if (!me.hasFocus) {
                me.hasFocus = true;
                me.fireEvent('focus', me, e);
            }
            if (me.postFocus && !me.postFocus.$emptyFn) {
                // me.postFocus(e);
            }
            if (container) {
                container.afterFocusableChildFocus(me, e);
            }
        }
    },
    applyPrimaryFilter: function(filter, oldFilter) {
        var me = this,
            store = me.getStore() && me._pickerStore,
            isInstance = filter && filter.isFilter,
            methodName;

        // If we have to remove the oldFilter, or reconfigure it...
        if (store && oldFilter) {
            // We are replacing the old filter
            if (filter) {
                if (isInstance) {
                    store.removeFilter(oldFilter, true);
                }
                else {
                    oldFilter.setConfig(filter);

                    return;
                }
            }
            // We are removing the old filter
            else if (!store.destroyed) {
                // store.getFilters().remove(oldFilter);
            }
        }

        // There is a new filter
        if (filter) {
            if (filter === true) {
                filter = {
                    id: me.id + '-primary-filter',
                    anyMatch: me.getAnyMatch(),
                    caseSensitive: me.getCaseSensitive(),
                    root: 'data',
                    property: me.getDisplayField(),
                    value: me.inputElement.dom.value,
                    disabled: true
                };
            }

            // If it's a string, create a function which calls it where it can be found
            // but using the Filter as the scope.
            if (typeof filter === 'string') {
                methodName = filter;
                filter = {
                    filterFn: function(rec) {
                        var methodOwner = me.resolveListenerScope(me);

                        // Maintainer: MUST pass "this" as the scope because the method must
                        // be executed as a Filter method for access to the filter configurations.
                        return methodOwner[methodName].call(this, rec);
                    }
                };
            }

            // Ensure it's promoted to an instance
            if (!filter.isFilter) {
                filter = new Ext.util.Filter(filter);
            }

            // Primary filter serialized as simple value by default
            filter.serialize = function() {
                return me.serializePrimaryFilter(this);
            };

            // Add filter if we have a store already
            if (store) {
                store.addFilter(filter, true);
            }
        }

        return filter;
    },
    updateStore: function(store, oldStore) {
        let storeOnline = store.getStoreId()===null || ( typeof(store.getStoreId())=='string' && !(store.getStoreId()).includes('online') )?false:true;
        let extraParams = this["extraParams"];
        let field  = this;
        let vm;
        try{
            vm = findParentModel(field);
        }catch(e){
            return;
        }
        vm = vm===null?undefined:vm;
        if(storeOnline){
            store.getProxy().setExtraParam("origin_type","dropdown");
            if(field.up("formpanel")!==undefined){
                store.getProxy().setExtraParam("origin",field.up("formpanel").getId());
            }
        }

        if(storeOnline && extraParams!==undefined){
            let params = JSON.parse(JSON.stringify(extraParams));
            let keys = Object.keys(params);
            let keyLama = vm.get("dependants")===null?{}:vm.get("dependants");

            keys.forEach(function(key){
                if( params[key].toString().includes("{") && params[key].toString().includes("}")){
                    field['hasDependant']=true;
                    arrayParams = params[key].toString().split("}");
                    arrayParams.forEach(function(rawParam){
                        let rawParam2=rawParam.split("{");
                        let fixedParam = rawParam2[rawParam2.length-1];
                        if(fixedParam.includes(".") && fixedParam!==""){
                            let form = fixedParam.split(".")[0];
                            if(keyLama[form]===undefined){
                                keyLama[form]=[];
                            }
                            if(!keyLama[form].includes(fixedParam+"."+field.up("formpanel").getId()+"."+field.getName())){
                                keyLama[form].push(fixedParam+"."+field.up("formpanel").getId()+"."+field.getName());
                            }
                        }else if(fixedParam!==""){
                            if(keyLama[field.up("formpanel").getId()]===undefined){
                                keyLama[field.up("formpanel").getId()]=[];
                            }
                            if(!keyLama[field.up("formpanel").getId()].includes(field.up("formpanel").getId()+"."+fixedParam+"."+field.up("formpanel").getId()+"."+field.getName())){
                                keyLama[field.up("formpanel").getId()].push(field.up("formpanel").getId()+"."+fixedParam+"."+field.up("formpanel").getId()+"."+field.getName());
                            }
                        }
                    });
                }
            });
            vm.set("dependants",keyLama);
            field.getStore().setListeners({
                beforeload:function(currentStore,operation){
                    let dependantOk = true;
                    let liveparams = JSON.parse(JSON.stringify(extraParams));
                    keys.forEach(function(key){
                        if( liveparams[key].toString().includes("{") && liveparams[key].toString().includes("}") ){
                            arrayParams = liveparams[key].toString().split("}");
                            arrayParams.forEach(function(rawParam){
                                let rawParam2=rawParam.split("{");
                                let fixedParam = rawParam2[rawParam2.length-1];
                                if(fixedParam.includes(".") && fixedParam!==""){
                                    let formId = fixedParam.split(".")[0];
                                    let fieldName = fixedParam.split(".")[1];
                                    let val=null;
                                    try{
                                        val = Ext.getCmp(formId).getFields(fieldName).getValue();
                                        if(val===null || val==""){
                                            dependantOk=false;
                                            Ext.getCmp(formId).getFields(fieldName).setError("silahkan diisi dahulu");
                                            if(fieldName.includes("_id")){
                                                let fieldLain = Ext.getCmp(formId).getFields(fieldName.replace("_id",""));
                                                if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                            }else{
                                                let fieldLain = Ext.getCmp(formId).getFields(fieldName+"_id");
                                                if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                            }
                                            Ext.getCmp(formId).getFields(fieldName).setError("silahkan diisi dahulu");
                                            return false;
                                        }
                                    }catch(e){ return false;}
                                    liveparams[key]=liveparams[key].toString().replace(`{${fixedParam}}`,val);
                                }else if(fixedParam!==""){
                                    let dependants = vm.get("dependants");
                                    let val = null;
                                    Object.keys(dependants).forEach(function(formName){
                                        dependants[formName].forEach(function(relation){
                                            let arrayRelation = relation.split(".");
                                            if(arrayRelation[0]==arrayRelation[2] && fixedParam==arrayRelation[1] && field.getName()==arrayRelation[3]){
                                                try{
                                                    let formParent = Ext.getCmp(arrayRelation[0]);
                                                    val = formParent.getFields(fixedParam).getValue();
                                                    if(val===null || val==""){
                                                        dependantOk=false;
                                                        if(fixedParam.includes("_id")){
                                                            let fieldLain = formParent.getFields(fixedParam.replace("_id",""));
                                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                        }else{
                                                            let fieldLain = formParent.getFields(fixedParam+"_id");
                                                            if(fieldLain!==undefined){fieldLain.setError("silahkan diisi dahulu");}
                                                        }
                                                        formParent.getFields(fixedParam).setError("silahkan diisi dahulu");
                                                        return false;
                                                    }
                                                }catch(e){
                                                    return false;
                                                }
                                            }
                                        });
                                    });
                                    liveparams[key]=liveparams[key].replace(`{${fixedParam}}`,val);
                                }
                            });
                            if( field['oldValue']!==undefined ){
                                currentStore.getProxy().setExtraParam(key,liveparams[key]);
                            }
                        }else{
                            currentStore.getProxy().setExtraParam(key,liveparams[key]);

                        }
                    });
                    let proxy = currentStore.getProxy();
                    let params = proxy.getExtraParams();
                    Object.keys(params).forEach(function(key){
                        if( (params[key].toString().includes("{") && params[key].toString().includes("}"))|| params[key].toString().includes("null") ){
                            delete params[key];
                        }
                    });

                    if(!dependantOk && field['oldValue']!==undefined){
                        Ext.Msg.alert("Wait","Please Fill Parent Filter");
                        currentStore.setData([]);
                        return false;
                    }
                    proxy.setExtraParams(params);
                }
            });
        }else{
            if(store.getProxy().type=="ajax"){
                store.load(function(records,operation){
                    if(records.length>0 && field.getAutoSelect() && (vm!==undefined && !vm.get("isUpdating")) ){
                        let firstData = records[0].getData();
                        if(firstData!==undefined && firstData[field.getValueField()]!==undefined ){
                           field.setValue(firstData[field.getValueField()]);
                        }
                    }
                });
            }
        }
        if(this.getItemTpl()!==undefined && !(this.getItemTpl()).includes(`<span class="x-list-label">`)){
            var newData = this.getItemTpl();
            store.getProxy().getReader().setTransform(function(data){

                var fixedData = data[store.getProxy().getReader().getRootProperty()];
                if(field['transform']!==undefined && typeof(field['transform'])=='function'){
                    let functiontransform = field.transform;
                    fixedData.forEach((dt,idx)=>{
                        fixedData[idx]=functiontransform(dt);
                    });
                }
                fixedData.forEach(function(perData,index){
                    let dataBaru=newData;
                    Object.keys( perData ).forEach(function(key){
                        dataBaru = (dataBaru).replace(`{${ key }}`, perData[key]);
                        fixedData[index]['itemTpl'] = dataBaru;
                    });
                });
                return fixedData;
            });
            field.setDisplayField("itemTpl");
        }else if(field['transform']!==undefined && typeof(field['transform'])=='function'){
            store.getProxy().getReader().setTransform(function(data){
                var fixedData = data[store.getProxy().getReader().getRootProperty()];
                    let functiontransform = field.transform;
                    fixedData.forEach((dt,idx)=>{
                        fixedData[idx]=functiontransform(dt);
                    });
                return fixedData;
            });
        }
        var me = this,
            isRemote = me.getQueryMode() === 'remote',
            primaryFilter,
            proxy, oldFilters;
        if (isRemote) {
            store.setRemoteFilter(true);
            proxy = store.getProxy();

            if (proxy.setFilterParam) {
                proxy.setFilterParam(me.getQueryParam());
            }
        }
        me.callParent([store, oldStore]);
        primaryFilter = me.getPrimaryFilter();

        if (primaryFilter) {
            if (oldStore && !oldStore.destroyed) {
                oldFilters = oldStore.getFilters();
                if (oldFilters) {
                    oldFilters.remove(primaryFilter);
                }
            }
            me._pickerStore.addFilter(primaryFilter, true);
        }
        if (me.getQueryMode() === 'local') {
            store.on({
                filterchange: 'onStoreFilterChange',
                scope: me
            });
        }
    },
});
Ext.define('QL.tabpanel', {
    override : "Ext.tab.Panel",
    afterRender:function(){
        var fixedHeight=0; let lastHeight=[];;
        this.getItems().items.forEach(function(childPanel){
            if(childPanel['lastHeight']!==undefined){
                lastHeight.push(childPanel['lastHeight']);
            }
            var tabHeight = 0;
            var children = childPanel.getItems().items;
            children.forEach(function(child){
                let height = child.renderElement.dom.offsetHeight;
                tabHeight+=height;
            });
            if(tabHeight>fixedHeight){
                fixedHeight=tabHeight;
            }
        });
        lastHeight = Ext.Array.max(lastHeight);
        lastHeight  = typeof(lastHeight)==="undefined"?0:(lastHeight);
        this.setHeight( (fixedHeight>lastHeight?fixedHeight:lastHeight)+35 );
    },

    doTabChange: function(tabBar, newTab) {
        var oldActiveItem = this.getActiveItem(),
            newActiveItem;

        this.setActiveItem(tabBar.indexOf(newTab));
        newActiveItem = this.getActiveItem();

        this.afterRender();
        return this.forcedChange || oldActiveItem !== newActiveItem;
    },
});

Ext.define("QL.viewModel",{
    override:"Ext.app.ViewModel",
    constructor:function(config){
        this.bindings = {};
        this.initConfig(config);
        let vm = this;
        let data = this.getData();
        Object.keys(data).forEach(key=>{
            if(data['modelApi']!==undefined && data[key]!==null && data[key]['storeId']!==undefined){
                let store = data[key];
                store.setListeners({
                    datachanged:function(storeInit,data){
                        let firstData={};
                        let storeData = store.getData().items;
                        firstData = storeData.length>0?JSON.parse(JSON.stringify(storeData[0].data)):{};
                        let keys = Object.keys(firstData);
                        keys.forEach(key=>{
                            let isNumber=Ext.Number.from(firstData[key], null);
                            if(isNumber==null){
                                delete firstData[key];
                            }else{
                                firstData[key]= isNumber;
                            }
                        });
                        storeData.forEach(function(dt,i){
                            if(i>0){
                                let row = dt.data;
                                keys.forEach(key=>{
                                    let isNumber=Ext.Number.from(row[key], null);
                                    if(isNumber!==null){
                                        firstData[key]= firstData[key]+isNumber;
                                    }
                                });
                            }
                        });
                        vm.set("count_"+key,storeData.length);
                        vm.set("sum_"+key,firstData);
                    }
                });
            }
        });
    }
});
Ext.define('QL.grid',{
    override:'Ext.grid.Grid',
    itemConfig: {
        viewModel: true,
        xtype: 'gridrow'
    },
    plugins:{
        gridexporter: true,
        gridfilters: true,
        gridcellediting: {
            selectOnEdit: true
        }
    },
    selectable:{
        rows: false,
        cells: true
    }
});
Ext.define('QL.gridColumn',{
    override:'Ext.grid.column.Column',
    constructor: function(config) {
        var me = this,
            isHeaderGroup, menu;
        if (config.columns || me.columns) {
            isHeaderGroup = me.isHeaderGroup = true;
        }
        else {
            me.isLeafHeader = true;
        }

        me.callParent([config]);

        me.addCls(isHeaderGroup ? me.groupCls : me.leafCls);

        menu = me.getConfig('menu', /* peek= */true);

        if (!menu && me.getMenuDisabled() === null) {
            me.setMenuDisabled(true);
        }
        if(this.getEditable()!==null && this.getEditable()===true){
            this.setCell({
                style:{"background-color":"#b9e2e6"}
            });
        };
    },
});
Ext.define("QL.groupStore",{
    override: 'Ext.dataview.GroupStore',
    load: function() {
        try{
            this.getSource().load();
        }catch(e){}
    },
    getProxy:function(){
        try{
            return this.getSource().getProxy();
        }catch(e){}
    }
})

Ext.define('QL.navigationview', {
    override:"Ext.navigation.View",
    cls:"goes-content",
    listeners:{
        push :function( el, view) {
            if(el['initializePage']!==undefined && el['initializePage']===false){return;}
            if(qlconfig['initializePerPage']!==undefined && qlconfig['initializePerPage']){
                try{
                    api.read({
                        model:el.getId().replace("nav_",""),
                        id :"initialize"
                    }, function(script){
                        try{
                            var fn = new Function(atob(atob(script)));
                            fn();
                        }catch(e){}
                    },function(errors){
                        // window.console.clear();
                    });
                }catch(e){}
            }
        }
    }

});

Ext.define('QL.gridSelection',{
    override:"Ext.grid.selection.Cells",
    isSelected: function(recordIndex, columnIndex) {
        var range;

        if (this.startCell) {
            try{
                if (recordIndex.isGridLocation) {
                    columnIndex = recordIndex.columnIndex;
                    recordIndex = recordIndex.recordIndex;
                }
                if (!(Ext.isNumber(recordIndex) && Ext.isNumber(columnIndex))) {
                    Ext.raise(
                        'Cells#isSelected must be passed either a GridLocation of ' +
                        'a row and column index'
                    );
                }
                range = this.getRowRange();

                if (recordIndex >= range[0] && recordIndex <= range[1]) {
                    // get start and end columns in the range
                    range = this.getColumnRange();

                    return (columnIndex >= range[0] && columnIndex <= range[1]);
                }
            }catch(e){return false;}

        }
        return false;
    },
});
Ext.define('QL.datefield',{
    override: "Ext.field.Date",
    picker:{
        xtype: 'datepanel',
        autoConfirm: true,
        floated: true,
        listeners: {
            tabout: 'onTabOut',
            select: 'onPickerChange',
            scope: 'owner'
        },
        keyMap: {
            ESC: 'onTabOut',
            scope: 'owner'
        }
        // slotOrder:[ "month","year",]
    },
    floatedPicker: {
        xtype: 'datepanel',
        autoConfirm: true,
        floated: true,
        listeners: {
            tabout: 'onTabOut',
            select: 'onPickerChange',
            scope: 'owner'
        },
        keyMap: {
            ESC: 'onTabOut',
            scope: 'owner'
        }
    },

    edgePicker: {
        xtype: 'datepicker',
        cover: true
    },
    // applyPicker: function(picker, oldPicker) {
    //     var me = this;

    //     picker = me.callParent([picker, oldPicker]);

    //     if (picker) {
    //         me.pickerType = picker.xtype === 'datepicker' ? 'edge' : 'floated';
    //         picker.ownerCmp = me;
    //     }

    //     return picker;
    // },
});
Ext.define('QL.formbutton',{
    extend:'Ext.form.Panel',
    xtype: 'formbutton',
    cls:"qlformbutton",
    bbar:[{
        xtype:'container',
        layout:'vbox',
        cls:"metaform",
        items:[{
            xtype:'displayfield',
            labelWidth:"40%",
            label:"Information:",
            value:'Edited By Fajar On 12/12/12 12:12:12',

        },{
            xtype:'displayfield',
            label:"Reason Revise:",
            labelWidth:"40%",
            value:'Tolong ditambahkan sesuatu yang sekiranya dibutuhkan oleh transaksi SEMUANYA, jika dipahami katakan sesuatu saja yang anda tahu bro',
        }]
    }],
});
Ext.define('QL.barisfield',{
    extend:'Ext.Container',
    xtype: 'qlbarisfield',
    cls:"qlfieldperbaris",
    layout: 'fit',
    autoSize:true,
    defaults: {
        xtype: 'textfield',
        border: true,
        width: 450,
        cls: "qlfieldperbaris",
        margin: "5 0 5 0"
    },
});


Ext.define('QL.gridFilterMenuBase', {
    override:'Ext.grid.filters.menu.Base',
    syncQuery: function() {
        var me = this,
            dataIndex = me.column.getDataIndex(),
            plugin = me.plugin,
            query = plugin.getQuery(),
            added = 0,
            removed = 0,
            filters, i, item, items, value;
        if (dataIndex) {
            dataIndex = me.column['dataIndexReal']!==undefined ? me.column['dataIndexReal'] : dataIndex;
            filters = Ext.clone(query.getFilters());
            items = me.getMenu().getItems().items;
 
            for (i = filters && filters.length; i-- > 0; /* empty */) {
                if (filters[i].property === dataIndex) {
                    filters.splice(i, 1);
                    ++removed;
                }
            }
 
            if (me.getChecked()) {
                for (i = items.length; i-- > 0;) {
                    item = items[i];
 
                    if (item.operator) {
                        value = item.getValue();
 
                        if (value !== null && value !== '') {
                            ++added;
 
                            if (Ext.isDate(value)) {
                                value = Ext.Date.format(value, 'C');
                            }
 
                            (filters || (filters = [])).push({
                                property: dataIndex,
                                operator: item.operator,
                                value: value
                            });
                        }
                    }
                }
            }
 
            if (!added) {
                me.setChecked(false);
            }
 
            if (added || removed) {
                plugin.setActiveFilter(filters);
            }
        }
    }
})

Ext.define('QL.gridFilterPlugins', {
    override:'Ext.grid.filters.Plugin',
    privates:{
        onFilterItemCheckChange: function(item) {
            item.syncQuery();
        },
        queryModified: function() {
            var filters = this.cmp.getStore();
            filters = filters && filters.getFilters();
            let filterProps = this.getActiveFilter();
            if(qlconfig.backend_where_and!==undefined && typeof(qlconfig.backend_where_and)=='function'){
                let functionFilter = qlconfig.backend_where_and;
                functionFilter(this.getOwner().getStore(), filterProps);
            }
            return;
            if (filters) {
                filters.beginUpdate();
                ++filters.generation;
                filters.endUpdate();
            }
        }
    }
})
