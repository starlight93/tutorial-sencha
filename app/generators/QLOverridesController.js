
Ext.define('QL.controller',{
    override: 'Ext.app.ViewController',
	QLLoginExecuting: function (el) {
        var form = el.up("formpanel");
        var formData = form.getValues();
        var data = {
            data:formData
        }
        if(qlconfig.use_login){
            api.login(data, function(json){
                qlproject.getApplication().loginSuccess(json);
            },function(error){
                console.log(error);
            });
        }else{
            var json = {
                user : "anonymous"
            }
            qlproject.getApplication().loginSuccess(json);
        }
    },

    QLNavigationBack:function(el){
        let VM = this.getViewModel();
        VM.set("isUpdating",false);
        VM.set("isApproval",false);
        VM.set("resource_id",null);
        (Object.keys(VM.getData())).forEach( isi =>{
            if( VM.get(isi)!==null && (VM.get(isi)) ['isStore']!==undefined &&  (VM.get(isi)) ['isStore']===true){
                (VM.get(isi)).removeAll();
            }
        });
        if(el.getText().toLowerCase()!="save" || el['close']!==undefined){
            el.up("navigationview").getItems().items[2].getItems().items[0].reset(true);
            el.up("navigationview").down("QLDatatableV1").down("grid").getStore().load();
            el.up("navigationview").pop();
        }else{
            el.up("navigationview").getItems().items[2].getItems().items[0].reset(true,true);
        }
    },

    QLApiDelete:function(el,row){                             
        Ext.Msg.confirm('Confirmation', 'Are you sure you want to delete?',
        function(answer) {
            if(answer=="yes"){
                api.delete({
                    model: row.grid.apiModel, 
                    id :row.record.data.id
                }, function(json){
                    row.record.store.load();
                });
                
            }
        });
    },
    QLDetailDelete:function(el,row){
        var controller = this;
        function hapus(){
            let values = row.record.data;
                if(controller.detailDeleteBefore !== undefined ){
                    var trigger = controller.detailDeleteBefore(row.grid.getStore(),values);
                    if(typeof(trigger)=='boolean' && !trigger){
                        return;
                    }
                }
                row.grid.getStore().remove(row.record);
                if(controller.detailDeleteAfter !== undefined ){
                    controller.detailDeleteAfter(row.grid.getStore(),values);
                }
                //update height
                try{
                    var tabpanel = el.up("tabpanel");
                    tabpanel.getItems().items.forEach(function(children){
                        let grid = children.down("grid");
                        let before = children['lastHeight'];
                        if(before===undefined){
                            before = (children.renderElement.dom.offsetHeight);
                        }
                        let after=before;
                        if(grid!==null && (grid['heightAuto']!==undefined && grid['heightAuto']===true)){
                            if(row.grid.getStore().getStoreId() == grid.getStore().getStoreId()){
                                after-= isNaN(grid.getHeight())?0:grid.getHeight();
                                let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                                let gridHeightBody   = 0;
                                let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                                Array.from(rows).forEach(function(row){
                                    gridHeightBody+=row.offsetHeight;
                                });
                                grid.setHeight(gridHeightHeader+gridHeightBody+40);
                                after+=grid.getHeight();
                                children['lastHeight'] = after;
                            }
                        }
                    });
                    tabpanel.afterRender();

                }catch(e){}
        }
        if(row.tool['confirmation']!==undefined && row.tool['confirmation']===false){
            hapus();
            return;
        }
        Ext.Msg.confirm("Confirmation", 'Yakin dihapus?', function (answer) {
            if (answer=='yes') {
                hapus();
            }
        })
    },
    QLResetForm:function(el){
        el.up("formpanel").reset(true); // CLEAR FORM DETAIL
    },
    QLAddDetail:function(el){
        let controller = this;
        var form = el.up("formpanel");
        if(form.getFields("___")!==undefined){
            form.getFields("___").up("container").getItems().items[2]._handler(form.getFields("___"));
            return ;
        }
        if(!form.validate()){
            Ext.Msg.alert("Invalid!","Cek Ulang Form Anda");
            return true;
        }
        let values = form.getValues();
        if(el['caster']!==undefined){
            values = el.caster(values);
        }
        values['meta_update']=true;
        values['meta_delete']=true;
        var store = el.up("formpanel").up().down("grid").getStore();
        if(el['resourceId']!==undefined && el['resourceId']!=null){
            if(controller.detailUpdateBefore !== undefined ){
                var trigger = controller.detailUpdateBefore(store,values);
                if(typeof(trigger)=='boolean' && !trigger){
                    return;
                }else if(typeof(trigger)=='object'){
                    values=trigger;
                };
            }
            var oldData = store.getById(el['resourceId']).getData();
            Object.assign(oldData,values);
            values = oldData;
            store.getById(el['resourceId']).set(values);
            if(controller.detailUpdateAfter !== undefined ){
                controller.detailUpdateAfter(store,values);
            }
            el.setConfig(
                {text:"Add To List",iconCls:"x-fa fa-plus", ui:"action", resourceId:null}
            )
            if(form['nocreate']!==undefined && form['nocreate']){
                try{
                    form.setHidden(true);     
                    var tabpanel = form.up("tabpanel");
                    tabpanel.afterRender();
                }catch(e){}
            }       
        }else{
            values['unique_id'] = Math.random().toString(36).substr(2, 9);
            if(controller.detailAddBefore !== undefined ){
                var trigger = controller.detailAddBefore(store,values);
                if(typeof(trigger)=='boolean' && !trigger){
                    return;
                }else if(typeof(trigger)=='object'){
                    values=trigger;
                };
            }
            store.add( values );
            if(controller.detailAddAfter !== undefined ){
                controller.detailAddAfter(store,values);
            }
        }
        form.reset( true );
        try{
            var tabpanel = el.up( "tabpanel" );
            tabpanel.getItems().items.forEach(function(children){
                let grid = children.down("grid");
                let before = children['lastHeight'];
                if(before===undefined){
                    before = (children.renderElement.dom.offsetHeight);
                }
                let after=before;
                if(grid!==null && (grid['heightAuto']!==undefined && grid['heightAuto']===true)){
                    if(store.getStoreId() == grid.getStore().getStoreId()){
                        after-= isNaN(grid.getHeight())?0:grid.getHeight();
                        let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                        let gridHeightBody   = 0;
                        let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                        Array.from(rows).forEach(function(row){
                            gridHeightBody+=row.offsetHeight;
                        });
                        grid.setHeight(gridHeightHeader+gridHeightBody+40);
                        after+=grid.getHeight();
                        children['lastHeight'] = after;
                    }
                }
            });
            tabpanel.afterRender();
        }catch(e){}
    },
    QLAddDetailPopup:function(el, values = null){
        let controller = this;
        var form = el.up("formpanel");
        try{
            let dialog  = Ext.getCmp("popup_"+form.rawFields.id);
            dialog.setViewModel( findParentModel(form) );
            dialog.setTitle(form.up().title);
            dialog.setItems([form.rawFields,{
                xtype   : "formpanel",
                buttons : [
                    {
                        text:"Add to List",
                        handler:function(button){
                            var dialogForm = button.up("dialog").down("formpanel");
                            if(!dialogForm.validate()){
                                let stringValidation="";
                                let fields = dialogForm.getFields();
                                let errors = dialogForm.getErrors();
                                Object.keys(errors).forEach(key=>{
                                    if(errors[key]!==null){
                                        let errorString = "";
                                        errors[key].forEach(err=>{
                                            errorString+=" "+err;
                                        });
                                        stringValidation+=(fields[key].getLabel()+" : "+errorString+"<br>");
                                    }
                                });;
                                Ext.Msg.alert("Invalid!",stringValidation);
                                return true;
                            }
                            var store   = form.up().down("grid").getStore();
                            var values = dialogForm.getValues()
                            if( (button.getText()).toLowerCase() =='update'){

                                if(controller.detailUpdateBefore !== undefined ){
                                    var trigger = controller.detailUpdateBefore(store,values);
                                    if(typeof(trigger)=='boolean' && !trigger){
                                        return;
                                    }else if(typeof(trigger)=='object'){
                                        values=trigger;
                                    };
                                }                                
                                var oldData = store.getById(button['idDetail']).getData();
                                Object.assign(oldData,values);
                                values = oldData;
                                store.getById(button['idDetail']).set( values );
                                button.setText("Add to List");
                                if(controller.detailUpdateAfter !== undefined ){
                                    controller.detailUpdateAfter(store,values);
                                }
                            }else{
                                if(controller.detailAddBefore !== undefined ){
                                    var trigger = controller.detailAddBefore(store,values);
                                    if(typeof(trigger)=='boolean' && !trigger){
                                        return;
                                    }else if(typeof(trigger)=='object'){
                                        values=trigger;
                                    };
                                }
                                store.add( values );
                                if(controller.detailAddAfter !== undefined ){
                                    controller.detailAddAfter(store,values);
                                }
                            }
                            dialogForm.reset(true);
                            button.up("dialog").hide();
                            try{
                                var tabpanel = el.up("tabpanel");
                                tabpanel.getItems().items.forEach(function(children){
                                    let grid = children.down("grid");
                                    let before = children['lastHeight'];
                                    if(before===undefined){
                                        before = (children.renderElement.dom.offsetHeight);
                                    }
                                    let after=before;
                                    if(grid!==null && (grid['heightAuto']!==undefined && grid['heightAuto']===true)){
                                        if(store.getStoreId() == grid.getStore().getStoreId()){
                                            after-= isNaN(grid.getHeight())?0:grid.getHeight();
                                            let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                                            let gridHeightBody   = 0;
                                            let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                                            Array.from(rows).forEach(function(row){
                                                gridHeightBody+=row.offsetHeight;
                                            });
                                            grid.setHeight(gridHeightHeader+gridHeightBody+40);
                                            after+=grid.getHeight();
                                            children['lastHeight'] = after;
                                        }
                                    }
                                });
                                tabpanel.afterRender();
                            }catch(e){}
                        }
                    }
                ]
            }]);
            if(form.rawFields['popup']!==undefined && form.rawFields['popup']){
                dialog.getItems().items[0].setHeader(null);
            }
            if( values!==null && values['id']!==undefined ){
                dialog.getItems().items[0].setValues(values);
                dialog.getItems().items[1].getButtons().getItems().items[0].setText('Update');
                dialog.getItems().items[1].getButtons().getItems().items[0].setUi('confirm');
                dialog.getItems().items[1].getButtons().getItems().items[0]['idDetail']=values.id;
            }
            dialog.show();
        }catch(e){
            console.log(e);
        }
        return ;
    },

    QLEditDetail:function(el,row){
        let detailForm = el.up().down("formpanel");
        if(detailForm['rawFields']!==undefined){
            this.QLAddDetailPopup( detailForm.getButtons().getItems().items[0], row.record.data );
            
            return;
        }
        if(row.record.data['meta_create']!==undefined && !row.record.data['meta_create']){
            try{
                detailForm['nocreate']=true;
                detailForm.setHidden(false);
                var tabpanel = detailForm.up("navigationview").getItems().items[2].getItems().items[1];
                tabpanel.afterRender();            
            }catch(e){}
        }
        let buttonAdd = detailForm.getButtons().getItems().items[0];
        buttonAdd.setConfig(
            {text:"Update",iconCls:"x-fa fa-check", ui:"confirm"}
        );
        buttonAdd['resourceId']=row.record.getId();
        detailForm.setValues(row.record.data)
    },
    QLSaveOrUpdate:function(el){
        var controller = this;
        var isMultipart=false;
        var form = el.up("formpanel").up().getItems().items[0].getItems().items[0];
        var model = this.getViewModel().get('modelApi');
        if(!form.validate()){
            let stringValidation="";
            let fields = form.getFields();
            let errors = form.getErrors();
            console.log(errors)
            Object.keys(errors).forEach(key=>{
                if(errors[key]!==null){
                    let errorString = "";
                    errors[key].forEach(err=>{
                        errorString+=" "+err;
                    });
                    stringValidation+=(fields[key].getLabel()+" : "+errorString+"<br>");
                }
            });;
            Ext.Msg.alert("Invalid!",stringValidation);
            return true;
        }
        
        var fields = form.getFields();
        var values = form.getValues();
        if(el['mergeData']!==undefined){
            Object.assign(values,el['mergeData']);
        }
        var formData = new FormData();
        var VM = this.getViewModel();
        (Object.keys(VM.getData())).forEach( isi =>{
            if( VM.get(isi)!==null && (VM.get(isi)) ['isStore']!==undefined &&  (VM.get(isi)) ['isStore']===true){                
                values[ ((VM.get(isi)).getStoreId()).split("___")[0] ] = [];
                ((VM.get(isi)).getData().items).forEach(function (dt) {
                   values[ ((VM.get(isi)).getStoreId()).split("___")[0] ].push(dt.data);
                });
            }
        });
        if(controller.casterApiPost !== undefined ){
            values = controller.casterApiPost(values);
        }
        Object.keys(values).forEach(function(field){
            if( fields[field]!==undefined && fields[field]['xtype']!==undefined && ((fields[field].xtype).includes('number')) && parseFloat(values[field])==NaN  ){
                values[field] = 0;
            }
        });
        
        Object.keys(fields).forEach(function(field){
           if(fields[field].isFile && fields[field].getFiles().length>0 ){
               formData.append( field,fields[field].getFiles()[0] );
               isMultipart=true;
           } 
        });
        if(isMultipart){
            Object.keys(values).forEach(function(field){
                if( Array.isArray(values[field])  ){
                    values[field] = JSON.stringify(values[field]);
                }
                formData.append( field,values[field] );
            });
            values=formData;
        }
        if(controller.saveBefore !== undefined ){
            if(!controller.saveBefore(values)){
                return;
            };
        }
        if(VM.get("isUpdating")){
            api.update(
                {
                    model   : model,
                    id      : VM.get("resource_id"),
                    data    : values,
                    multipart : isMultipart
                }, 
                function(json){
                    Ext.Msg.alert('Success', 'Data has been updated successfully', Ext.emptyFn);
                    controller.QLNavigationBack(el);
                },function(arrayError){
                    try{
                        var fieldList = Object.keys(values);
                        arrayError.forEach(function(dt){
                            for(let i =0; i<fieldList.length; i++){
                                if(dt.includes(fieldList[i])){form.getFields(fieldList[i]).setError(dt);}
                            }
                        }); }catch(e){}
                }
            )
        }else{
            api.create(
                {
                    model   : model,
                    data    : values,
                    multipart : isMultipart
                }, 
                function(json){ 
                    Ext.Msg.alert('Success', 'Data has been created successfully', Ext.emptyFn);
                    controller.QLNavigationBack(el);
                },function(arrayError){
                    try{
                        var fieldList = Object.keys(values);
                        arrayError.forEach(function(dt){
                            for(let i =0; i<fieldList.length; i++){
                                if(dt.includes(fieldList[i])){form.getFields(fieldList[i]).setError(dt);}
                            }
                        }); }catch(e){}
                }
            )
        }
    },
    QLResetDetail:function(el){
        el.up("formpanel").reset(true);
        let buttonAdd = el.up().getItems().items[0];
        buttonAdd.setConfig(
            {text:"Add To List",iconCls:"x-fa fa-plus", ui:"action", resourceId:null}
        );
    },
    resetField:function(el,currentEl){        
        el.up("container").getItems().items[0].reset();
        el.up("container").getItems().items[1].reset();
    },

    viewReport: function(el){
        var report = el.up("formpanel").up().getItems().items[2];
        var form = el.up("formpanel").up().getItems().items[0];
        var model = this.getViewModel().get('modelApi');
        if(!form.validate()){
            Ext.Msg.alert("Invalid!","Cek Ulang Form Anda");
            return true;
        }
        let params = form.getValues();        
        api.getArrayData({
            model: model, 
            params : params
        },function(arrayData){
            var config = [];
            arrayData.forEach(function(data,i){
                var reportData = report.formatter(data);
                if(i>0){
                    reportData['title'] = "";
                }
                config.push(reportData);
            });
            config[ config.length-1 ]['break'] = false;
            report.preview=false;
            report.pdfRefresh(config);
        },function(errors){
            console.log(errors);
        });
    },
    QLDownloadReport:function(el){
        var report = el.up("formpanel").up().getItems().items[2];
        var form = el.up("formpanel").up().getItems().items[0];
        report.pdfDownload(form.getTitle());
    },
    QLViewReportOnTab:function(el){
        var report = el.up("formpanel").up().getItems().items[2];
        report.handlerNewTab();
    },
    QLDownloadExcel:function(el){
        var report = el.up("formpanel").up().getItems().items[2];
        var form = el.up("formpanel").up().getItems().items[0];
        report.downloadXls(form.getTitle());
    },
    QLViewReport: function(el){
        var report = el.up("formpanel").up().getItems().items[2];
        var form = el.up("formpanel").up().getItems().items[0];
        var model = this.getViewModel().get('modelApi');
        if(!form.validate()){
            Ext.Msg.alert("Invalid!","Cek Ulang Form Anda");
            return true;
        }
        let params = form.getValues();        
        api.getArrayData({
            model: model, 
            params : params
        },function(arrayData){
            var config = [];
            arrayData.forEach(function(data,i){
                var reportData = (params['type']).toLowerCase()=='summary'?report.formatterSummary(data,params):report.formatterDetail(data,params);
                if(i>0){
                    reportData['title'] = "";
                }
                config.push(reportData);
            });
            config[ config.length-1 ]['break'] = false;
            report.pdfRefresh(config);
        },function(errors){
            console.log(errors);
        });
    },
    QLDatatablePreview:function(el,row){
        let fungsi = el['varconfig']!==undefined?el['varconfig']:el.up("navigationview").up("panel").preview;
        api.read({
            model:row.grid.apiModel,
            id :row.record.id,
            headers : {
                READHEADER : 'preview'
            }
        }, function(json){
            let rootKey = api.rootProperty;
            json = json[rootKey]===undefined?json:json[rootKey];
            var config = fungsi(json);
            Ext.getCmp("QLPrint").postHandler = function () {}
            Ext.getCmp(row.tool['print']!==undefined?'QLPrint':'QLPreview').showData(config);
        });
    },
    QLDatatablePreviewForm:function(el,row){
        this.QLDatatableRowEdit(el,row,true);
    },
    QLDatatablePrint:function(el,row){
        let fungsi = el['varconfig']!==undefined?el['varconfig']:el.up("navigationview").up("panel").preview;
        api.read({
            model:row.grid.apiModel,
            id :row.record.id
        }, function(json){
            let rootKey = api.rootProperty;
            json = json[rootKey]===undefined?json:json[rootKey];
            var config = fungsi(json);
            if(el['postHandler']!==undefined){
                Ext.getCmp('QLPrint').postHandler=el['postHandler'];
            }else{
                Ext.getCmp('QLPrint').postHandler=function(){};
            };
            Ext.getCmp('QLPrint').showData(config);
        });
    },
    QLDatatableRowEdit:function(el,row,isView=false){
        let controller = this;
        let event='edit';
        let tool = row.tool;
        if(tool.eventName!==undefined && typeof(tool.eventName)=='string'){
            event=tool.eventName;
        }
        let VM = el.up("navigationview").up("panel").getViewModel();
        var details = [];
        VM.set("isApproval",isView);
        (Object.keys(VM.getData())).forEach( isi =>{
            if( VM.get(isi)!==null && (VM.get(isi)) ['isStore']!==undefined &&  (VM.get(isi)) ['isStore']===true){
                details.push( (VM.get(isi)) );
            }
        });
        var viewModel = this.getViewModel();
        api.read({
            model:row.grid.apiModel,
            id :row.record.id,
            headers : {
                READHEADER : isView?'preview':'edit'
            }
        }, function(json){
            let originalJson = json;
            let rootKey = api.rootProperty;
            json = json[rootKey]===undefined?json:json[rootKey];
            if( (tool['caster'] !== undefined) ){
                json = tool.caster(json);
            }
            let handlerCreate = el.up("navigationview").getItems().items[0].getItems().items[0]._handler; 
            if(typeof(handlerCreate)=='string'){
                controller.createNew(el.up("navigationview").getItems().items[0].getItems().items[0]);
            }else{
                el.up("navigationview").getItems().items[0].getItems().items[0]._handler();
            }
            viewModel.set("isUpdating",true);
            viewModel.set("resource_id",row.record.id);
            if(originalJson['meta']!==undefined){
                viewModel.set("meta", originalJson['meta']);
            }
            var header = el.up("navigationview").getItems().items[2].getItems().items[0];
            details.forEach(detail=>{
                if(json[detail.getStoreId()]!==undefined){
                    detail.setData( json[detail.getStoreId()] );
                    if(json[detail.getStoreId()].length>0 && json[detail.getStoreId()][0]['meta_create']!==undefined && !json[detail.getStoreId()][0]['meta_create'] || isView){
                        try{
                            Ext.getCmp(detail.getStoreId()).setHidden(true);
                            Ext.getCmp(detail.getStoreId()).up("formpanel").setHidden(true);
                        }catch(e){}
                    }
                }
            });
            if(details.length>0){
                try{
                    var tabpanel = el.up("navigationview").getItems().items[2].getItems().items[1];
                    tabpanel.getItems().items.forEach(function(children){
                        let grid = children.down("grid");
                        if(grid!==null&& (grid['heightAuto']!==undefined && grid['heightAuto']===true) && (grid['heightAuto']!==undefined && grid['heightAuto']===true)){
                            let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                            let gridHeightBody   = 0;
                            let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                            Array.from(rows).forEach(function(row){
                                gridHeightBody+=row.offsetHeight;
                            });
                            grid.setHeight(gridHeightHeader+gridHeightBody+40);
                        }
                    });
                    tabpanel.afterRender();
                }catch(e){
                    console.log(e)
                }
            }
            header['defaultValues']=json;
            header.setValues(json);
            // akuel=header;
            // bbar:[{
            //     xtype:'container',
            //     layout:'vbox',
            //     cls:"metaform",
            //     items:[{
            //         xtype:'displayfield',
            //         labelWidth:"70%",
            //         label:"Information:",
            //         value:'Edited By Fajar On 12/12/12 12:12:12',
                    
            //     },{
            //         xtype:'displayfield',
            //         label:"Reason Revise:",
            //         labelWidth:"70%",
            //         value:'Tolong ditambahkan sesuatu yang sekiranya dibutuhkan oleh transaksi SEMUANYA, jika dipahami katakan sesuatu saja yang anda tahu bro',
            //     }]
            // }],
            if(originalJson['meta']!==undefined && originalJson['meta']){
                let metaForms = originalJson['meta'];
                let formKeys = Object.keys(metaForms);
                formKeys.forEach(formKey=>{
                    let form = Ext.getCmp(formKey);
                    let fields = [];
                    if(form!==undefined && metaForms[formKey]!==undefined){      
                        fields = form.getFields();                  
                        Object.keys(fields).forEach(key=>{
                            if(metaForms[formKey][key]!==undefined){
                                let field = fields[key];
                                switch(metaForms[formKey][key]) {
                                    case "disabled":
                                        fields[key].setDisabled(true);
                                    break;
                                    case "readonly":
                                        if(Array.isArray(field)){
                                            if(field.length>0){
                                                if(field[0]['xtype']!==undefined && ["radiogroup","checkboxgroup"].includes(field[0].xtype)){
                                                    var radios = field[0].getItems().items;
                                                    radios.forEach(function(radio){
                                                        radio.setDisabled(true);
                                                    });
                                                }
                                            }
                                        }else if(["checkboxfield","checkboxgroup"].includes(field["xtype"])){
                                            field.setDisabled(true);
                                        }else{
                                            field.setReadOnly(true);
                                        }
                                    break;
                                    case "hidden":
                                        fields[key].setHidden(true);
                                    break;
                                    default:                            
                                }
                            }
                        });

                    }
                });
                if(isView){
                    header.setReadOnly(true);
                    let formbawah = header.up("panel").getItems().items[2];
                    // let buttons = formbawah.getButtons().getItems().items;
                    formbawah.setButtons([
                        {
                        text: 'Back to Front',
                        alignSelf: 'center',
                        margin: '5 5 5 5',
                        iconCls: "x-fas fa-angle-double-left",
                        ui: 'decline',
                        handler: 'QLNavigationBack'
                    }]);
                }
            }
            if(originalJson["metaScript"]!==undefined && originalJson['metaScript']!==null){                
                try{
                    var fn = new Function(atob(atob(originalJson["metaScript"])));
                    fn();
                }catch(e){
                    console.log(e);
                }
            }
            if(controller.onFormOpen!==undefined && typeof(controller.onFormOpen)=='function'){
                try{
                    controller.onFormOpen(event,header,json);
                }catch(error){console.log(error);}
            }
        }); 
    },
	formFocusErrors: function (el) {
        
        var form = (el.up("formpanel") ==null)?form = this.getView():el.up("formpanel");
		var	errors = [],
			data = {
				errors: errors
			};
            var firstFieldError=null;

		form.getFields(false).forEach(function (field) {
			var error;
			if (!field.validate() && (error = field.getError())) {
				errors.push({
					errors: error,
					name: field.getLabel()
                });
                if(firstFieldError==null){
                    firstFieldError = field;
                }
			}
        });
        if(firstFieldError!=null){
            firstFieldError.focus(false,1200);
            console.log(data);
        }
	},
	formFieldErrors: function () {
        
    },
    
    maxRows:function(dataView){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();
        thisModel.set('currentRows',dataView.value);
        gridStore.getProxy().setExtraParam( parent.configParams["pagePaginate"], dataView.value=='All'?9999:dataView.value);        
        gridStore.getProxy().setExtraParam( parent.configParams["page"],1);
        gridStore.load();
    },
    pressPage: function(element){
        let parent = element.up("QLDatatableV1")===undefined?element.up("QLDatatablePopupV1"): element.up("QLDatatableV1");
        if(parent===undefined){ parent = element.up("QLDatatablePopupMultiV1")===undefined?element.up("QLDatatablePopupMulti"):element.up("QLDatatablePopupMultiV1")}
        if(parent===undefined){ parent = element.up('toolbar').up(); }
        var gridStore = parent.down("grid").getStore();
        gridStore.getProxy().setExtraParam(parent.configParams["page"],element.getText());
        gridStore.load();
    },
    pressNext: function(element){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();

        gridStore.getProxy().setExtraParam(parent.configParams["page"], 1 + parseInt(thisModel.get("gridCurrentPage")));
        gridStore.load();
    },
    pressPrev: function(element){
        let parent = this.getView();
        var thisModel = parent.getViewModel();
        var gridStore = parent.down("grid").getStore();

        gridStore.getProxy().setExtraParam(parent.configParams["page"], parseInt(thisModel.get("gridCurrentPage")-1));
        gridStore.load();
    },
    createNew:function(el){
        let controller = this;
        var nav = el.up("navigationview");
        let event = 'create';
        if(el.eventName!==undefined && typeof(el.eventName)=='string'){
            event=el.eventName;
        }
        nav.push(window[ (nav.getId()).replace('nav_','')+"_panel" ]);
        if(controller.onFormOpen!==undefined && typeof(controller.onFormOpen)=='function'){
            try{
                controller.onFormOpen(event,Ext.getCmp((nav.getId()).replace('nav_','')),{});
            }catch(error){console.log(error);}
        }
    }
});