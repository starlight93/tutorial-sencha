Ext.define('QL.PDFReportV1',{
    extend: 'Ext.Panel',
    requires:['Ext.Panel'],
    xtype: 'QLPDFReportV1',    
    alias: 'widget.QLPDFReportV1',
    height: "400px",
    pdfDownload:function(){
        let url = this.down("panel").renderElement.dom.getElementsByTagName("embed")[0].getAttribute("src");
        if(url==null || url===undefined || url==""){
            Ext.Msg.alert("Report Empty!","Please View Report First!");
            return false;
        }
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        a.href = url;
        a.download = this.getId()+".pdf";
        a.click();
        window.URL.revokeObjectURL(url);
    },
    pdfRefresh:function(arrayData){
        let loading = Ext.getCmp("loadingDialogReport");
        loading.show();
        let panelku=this;
        var template = `<div>
            <h5 style="text-align:right;font-weight:bold;margin-bottom:1px">
            {perusahaan}
            </h5>
            <h5 style="text-align:center;margin-top:1px;margin-bottom:1px">
                {judul}
            </h5>
            <p style="text-align:center;font-size:10px">
                {periode}
            </p>
        <div style="display:flex;margin-bottom:30px">
        <table>
            <tr>
                <td>
                    {headerKiri}
                </td>
                <td style="width:33%"> </td>
                <td>
                    {headerKanan}
                </td>
            </tr>
        </table>
        </div>

        <table style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;border-collapse: collapse; width: 100%;margin:auto;">
            <tr style="text-align:center; font-size: 8px;font-weight:bold;">
                {tableTH}    
            </tr>
            {C3}
        </table></div>`;
        
        var tableTH = `
            <th style="border: 1px solid black;
                padding: 8px;
                text-align: center;padding-top: 12px;
                padding-bottom: 12px;
                text-align: center;width:{width}"><br/><br/>{name}<br/>
            </th>`;
            
        var tableTHeader = `                
            <th colspan="{length}" style="border: 1px solid black;width:{width}"> 
                <table>
                    <tr>
                        <td colspan="2" style="border-bottom:1pt solid black;"><br/><br/>
                            {name}
                        </td>
                    </tr>    
                    <tr>
                        {children}
                    </tr>
                </table>
            </th>`;
        var pages = [];
        arrayData.forEach(function(mydata){
            var mytemplate = template;
            var keys = Object.keys(mydata);
            var thead = "";
            var tbody = "";
            var tdsData = [];
            for(let i=0;i<keys.length;i++){
                if( !['C1','C2','C3','format','gridStore','gridTable'].includes( keys[i]) ){
                    var rgx = new RegExp(`{${keys[i]}}`, "g");
                    mytemplate = mytemplate.replace(rgx, mydata[keys[i]]);
                }else if(keys[i]=='gridTable'){
                    for(let j=0; j<mydata[keys[i]].length; j++){
                        if(mydata[keys[i]][j]['children'] !==undefined){
                            var children = "";
                            for(let k=0;k<(mydata[keys[i]][j].children).length; k++){
                                children += `
                                    <td>
                                        ${mydata[keys[i]][j].children[k].text}
                                    </td>
                                `;
                                tdsData.push({
                                    'dataIndex': mydata[keys[i]][j].children[k]['dataIndex'],
                                    'style'    : mydata[keys[i]][j].children[k]['style'],
                                    'renderer' : mydata[keys[i]][j].children[k]['renderer']
                                });
                            }
                            thead += tableTHeader.replace("{width}", mydata[keys[i]][j].width)
                                        .replace("{name}", mydata[keys[i]][j].text)
                                        .replace("{length}", (mydata[keys[i]][j].children).length )
                                        .replace("{children}", children);
                        }else{
                            thead += (tableTH.replace("{width}", mydata[keys[i]][j].width)).replace("{name}", mydata[keys[i]][j].text);
                            tdsData.push({
                                'dataIndex': mydata[keys[i]][j]['dataIndex'],
                                'style'    : mydata[keys[i]][j]['style'],
                                'renderer' : mydata[keys[i]][j]['renderer']
                            });
                        }
                    }
                    mytemplate = mytemplate.replace("{tableTH}",thead);
                }
            }
            
            if( mydata['gridStore']!==undefined && tdsData.length>0 ){
                let rows = mydata['gridStore'];
                var tds = tdsData; 
                for(let j=0; j<rows.length; j++){
                    var td = "";
                    tds.forEach(function(v){
                        var rumus = v['renderer']==undefined?(data)=>{return data;}: v['renderer'];
                        var style = v['style']==undefined?'border:1px solid black;font-size:8px;':v['style']; 
                        var value = v['dataIndex']==undefined? rumus(rows[j]):rumus(rows[j][v.dataIndex]);
                        // var value = rumus(rows[j]);
                        td += `<td style="${style}">${value}</td>`;
                    });
                    tbody += `<tr>${td}</tr>`;
                }
                mytemplate = mytemplate.replace("{C3}",tbody);
            }

            pages.push({
                template : mytemplate,
                format   : mydata['FORMAT']
            });
        });
        let mode = "pdf";
        let url = "https://qazwsxedc.mtbalitours.com/trial/pdf";
        if(mode=='excel'){
            url = "https://qazwsxedc.mtbalitours.com/trial/excel";
        }
        var xhr =  new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            if (this['status'] == 200) {        
            var blob = new Blob([this['response']], {type: 'application/pdf'});
            var url = URL.createObjectURL(blob);
                panelku.down("panel").renderElement.dom.getElementsByTagName("embed")[0].setAttribute("src",url);
            }
            loading.hide();
        };
        let data = {
            pages : pages
        };
        xhr.send(JSON.stringify(data));
    },
    tbar:[
        {   text : "Export PDF", iconCls:"far fa-file-pdf" },
        {   text : "Export Excel", iconCls:"far fa-file-excel" } 
    ],
    items:[{
        xtype: "panel",
        html:`<embed width="100%" type="application/pdf"/>`,
        afterRender:function(){
            this.renderElement.dom.getElementsByTagName("embed")[0].setAttribute("height",this.up("panel").getHeight());
        }
    }],
    withButtons:true,
    afterRender:function(){
        if(!this.withButtons){
            this.setTbar([])
        }
        // console.log(this.withButtons);
    }
});