Ext.define('QL.PDFReportV2',{
    extend: 'Ext.Panel',
    requires:['Ext.Panel'],
    xtype: 'QLPDFReportV2',    
    alias: 'widget.QLPDFReportV2',
    height: "400px",
    pdfDownload:function(){
        let url = this.down("panel").renderElement.dom.getElementsByTagName("embed")[0].getAttribute("src");
        if(url==null || url===undefined || url==""){
            Ext.Msg.alert("Report Empty!","Please View Report First!");
            return false;
        }
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        a.href = url;
        a.download = this.getId()+".pdf";
        a.click();
        window.URL.revokeObjectURL(url);
    },
    getPage(config){
        var data = config['data'];
        var templateRow = `<tr>
            <td style="width:4%"> </td>
            <td style="text-decoration:underline;font-size:{fontSize}px;width:20%">{leftkey}</td><td style="width:2%">:</td><td style="width:26%">{leftvalue}</td>
            <td style="width:2%"> </td>
            <td style="text-decoration:underline;font-size:{fontSize}px;width:20%">{rightkey}</td><td style="width:2%">{titikduaright}</td><td style="width:25%">{rightvalue}</td>
            <td style="width:3%"> </td>
        </tr>`;
        var templatePage=`
            <table>
                <tr><td></td><td></td><td></td><td></td></tr>
                <tr><td style="width:7%"> </td>
                    <td>
                        {leftHeader}
                    </td>
                    <td style="width:33%"> </td>
                    <td>
                        {rightHeader}
                    </td>
                </tr>
            </table>
            <div style="display:flex;margin-bottom:20px;font-size:{fontSize}px;">
                <h4 style="text-align:center">{title}</h4>
                <table>
                    {headerData}
                </table>
            </div>`;
        var stringData = "";
        var dataKeys = Object.keys(data);
        if(config['only']!==undefined){
            dataKeys=config['only'].filter(function(min){
                if((  Object.keys(data) ).includes(min)){
                    return min;
                }
            });
        }
        
        if(config['except']!==undefined){
            dataKeys=dataKeys.filter(function(min){
                if(!(config['except']).includes(min)){
                    return min;
                }
            });
        }
        for(let i = 0; i< dataKeys.length-1;i += 2){
            var key = dataKeys[i];
            var keyRight = dataKeys[i+1];
            stringData += templateRow
                        .replace("{leftkey}", (key.charAt(0).toUpperCase() + key.slice(1)).replace(/\_/g," "))
                        .replace("{leftvalue}", data[key]==null||data[key]===undefined?"-":data[key])
                        .replace("{rightkey}", (keyRight.charAt(0).toUpperCase() + keyRight.slice(1)).replace(/\_/g," "))
                        .replace("{rightvalue}", data[keyRight]==null||data[keyRight]===undefined?"-":data[keyRight])
                        .replace("{titikduaright}",":");
        }
        if(dataKeys.length%2!=0){
            var key = dataKeys[dataKeys.length-1];
            stringData += templateRow
                        .replace("{leftkey}", (key.charAt(0).toUpperCase() + key.slice(1)).replace(/\_/g," "))
                        .replace("{leftvalue}", data[key]==null||data[key]===undefined?"-":data[key])
                        .replace("{titikduaright}","")
                        .replace("{rightkey}", "")
                        .replace("{rightvalue}", "");
        }
        let stringTemplate = templatePage
                        .replace("{headerData}",stringData)
                        .replace("{title}",config['title']==undefined?"":config['title'])
                        .replace("{rightHeader}",config['rightHeader']==undefined?"":config['rightHeader'])
                        .replace("{leftHeader}",config['leftHeader']==undefined?"":config['leftHeader']);
        stringTemplate = stringTemplate
                        .replace(/\{fontSize\}/g,data['fontSize']==undefined?"12":data['fontSize']);
        return (config['break']!==undefined && config['break']===true)? stringTemplate+"<<BREAK>>":stringTemplate;
    },
    getDetail(config){
            var mydata = config;
            var tableTHeader = `                
                <th colspan="{length}" style="border: 1px solid black;width:{width}"> 
                    <table>
                        <tr>
                            <td colspan="2" style="border-bottom:1pt solid black;"><br/><br/>
                                {name}
                            </td>
                        </tr>    
                        <tr>
                            {children}
                        </tr>
                    </table>
                </th>`;
            var tableTH = `
                <th style="border: 1px solid black;
                    padding: 8px;
                    text-align: center;padding-top: 12px;
                    padding-bottom: 12px;
                    text-align: center;width:{width}"><br/><br/>{name}<br/>
                </th>`;
            var mytemplate = `
                <h4 style="text-align:center">{title}</h4>
                <table>
                    <tr>
                        <td>
                            {leftHeader}
                        </td>
                        <td style="width:33%"> </td>
                        <td>
                            {rightHeader}
                        </td>
                    </tr>
                </table>
                </div>
        
                <table style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;border-collapse: collapse; width: 100%;margin:auto;">
                    <tr style="text-align:center; font-size: {fontSize}px;font-weight:bold;">
                        {tableTH}    
                    </tr>
                    {tableTR}
                    <tr>{tableFooter}</tr>
                </table></div>`;
            var keys = Object.keys(mydata);
            var thead = "";
            var tbody = "";
            var tdsData = [];
            for(let i=0;i<keys.length;i++){
                if( !['tableTR','format','gridStore','gridTable'].includes( keys[i]) ){
                    var rgx = new RegExp(`{${keys[i]}}`, "g");
                    mytemplate = mytemplate.replace(rgx, mydata[keys[i]]);
                }else if(keys[i]=='gridTable'){
                    for(let j=0; j<mydata[keys[i]].length; j++){
                        if(mydata[keys[i]][j]['children'] !==undefined){
                            var children = "";
                            for(let k=0;k<(mydata[keys[i]][j].children).length; k++){
                                children += `
                                    <td>
                                        ${mydata[keys[i]][j].children[k].text}
                                    </td>
                                `;
                                tdsData.push({
                                    'dataIndex': mydata[keys[i]][j].children[k]['dataIndex'],
                                    'style'    : mydata[keys[i]][j].children[k]['style'],
                                    'renderer' : mydata[keys[i]][j].children[k]['renderer']
                                });
                            }
                            thead += tableTHeader.replace("{width}", mydata[keys[i]][j].width)
                                        .replace("{name}", mydata[keys[i]][j].text)
                                        .replace("{length}", (mydata[keys[i]][j].children).length )
                                        .replace("{children}", children);
                        }else{
                            thead += (tableTH.replace("{width}", mydata[keys[i]][j].width)).replace("{name}", mydata[keys[i]][j].text);
                            tdsData.push({
                                'dataIndex': mydata[keys[i]][j]['dataIndex'],
                                'style'    : mydata[keys[i]][j]['style'],
                                'renderer' : mydata[keys[i]][j]['renderer'],
                                'align' : mydata[keys[i]][j]['align']==undefined?"left":mydata[keys[i]][j]['align']
                            });
                        }
                    }
                    mytemplate = mytemplate.replace("{tableTH}",thead);
                }
            }
            
            if( mydata['gridStore']!==undefined && tdsData.length>0 ){
                let rows = mydata['gridStore'];
                var tds = tdsData; 
                for(let j=0; j<rows.length; j++){
                    var td = "";
                    tds.forEach(function(v){
                        var rumus = v['renderer']==undefined?(data)=>{return data;}: v['renderer'];
                        var style = v['style']==undefined?`text-align:${v['align']};border:1px solid black;font-size:{fontSize}px;`:v['style'];
                        var value;
                        if(v['dataIndex']=='rownumberer'){
                            value = j+1;
                            style = 'text-align:center;border:1px solid black;font-size:8px;'
                        }else{
                            value = v['dataIndex']==undefined? rumus(rows[j]):rumus(rows[j][v.dataIndex]);
                        }
                        // var value = rumus(rows[j]);
                        td += `<td style="${style}">${value}</td>`;
                    });
                    tbody += `<tr>${td}</tr>`;
                }
                mytemplate = mytemplate.replace("{tableTR}",tbody);
            }
            if( mydata['gridFooter']!==undefined){
                let tds = mydata['gridFooter'];
                var td = "";
                tds.forEach(function(v){
                    td += `${v==null?"<td></td>":'<td style="text-align:right;font-size:'+v["fontSize"]+';">'+v["value"]+'</td>'}`;
                });
                mytemplate = mytemplate.replace("{tableFooter}",td);
            }else{
                mytemplate = mytemplate.replace("{tableFooter}","");
            }
            mytemplate = mytemplate
                            .replace(/\{fontSize\}/g,mydata['fontSize']==undefined?"12":mydata['fontSize']);
            return (config['break']!==undefined && config['break']===true)? mytemplate+"<<BREAK>>":mytemplate;
    },
    pdfRefresh(arrayData){
        let loading = Ext.getCmp("loadingDialogReport");
        loading.show();
        try{
            var me = this;
            var stringTemplate="";
            if(!Array.isArray(arrayData)){
                if(arrayData.type=='detail'){
                    stringTemplate= JSON.stringify(config['format']) + "<<FORMATTER>>" + me.getDetail(arrayData);
                }else{
                    stringTemplate= JSON.stringify(config['format']) + "<<FORMATTER>>" + me.getPage(arrayData);
                }
            }else{
                var me = this;
                arrayData.forEach(function(config,index){
                    if(index>0 && arrayData[index-1]['break']===false){
                        var formatter = "";
                    }else{
                        var formatter = JSON.stringify(config['format']) + "<<FORMATTER>>";                    
                    }
                    if(config.type=='detail'){
                        stringTemplate+= formatter + me.getDetail(config);
                    }else{
                        stringTemplate+= formatter + me.getPage(config);
                    }
                });
            }
        }catch(e){
            console.log(e); 
            Ext.Msg.alert("Failed!","Maybe Data Error");
        }
        
        let xhr = new XMLHttpRequest();
        let url = qlconfig.backend_pdf_renderer;
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            if (this['status'] == 200) {          
            var blob = new Blob([this['response']], {type: 'application/pdf'});
            var url = URL.createObjectURL(blob);
                me.down("panel").renderElement.dom.getElementsByTagName("embed")[0].setAttribute("src",url);
            }
            loading.hide();
        };
        let data = {
            data : stringTemplate
        };
        xhr.send(JSON.stringify(data));
    },
    tbar:[
        {   text : "Export PDF", iconCls:"far fa-file-pdf" },
        {   text : "Export Excel", iconCls:"far fa-file-excel" } 
    ],
    items:[{
        xtype: "panel",
        html:`<embed width="100%" type="application/pdf"/>`,
        afterRender:function(){
            this.renderElement.dom.getElementsByTagName("embed")[0].setAttribute("height",this.up("panel").getHeight());
        }
    }],
    withButtons:true,
    afterRender:function(){
        if(!this.withButtons){
            this.setTbar([])
        }
    }
});