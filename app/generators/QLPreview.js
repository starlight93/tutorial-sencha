Ext.define('QL.Preview',{
    extend: 'Ext.Panel',
    requires:['Ext.Panel'],
    xtype: 'QLPreview',
    alias: 'widget.QLPreview',
    height: "400px",
    htmlString :null,
    preview:true,
    pdfDownload:function(title="report"){
        let url = this.down("panel").renderElement.dom.getElementsByTagName("iframe")[0].getAttribute("src");
        if(url==null || url===undefined || url==""){
            Ext.Msg.alert("Stop!","Please Click View Report First!");
            return false;
        }
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.style = "display: none";
        a.href = url;
        a.download = title+".pdf";
        a.click();
        window.URL.revokeObjectURL(url);
    },
    getPage(config){
        var data = config['data'];
        var templateRow = `<tr>
            <td style="width:4%"> </td>
            <td style="text-decoration:underline;font-size:{fontSize}px;width:20%">{leftkey}</td><td style="width:2%;font-size:{fontSize}px;">:</td><td style="width:26%;font-size:{fontSize}px;">{leftvalue}</td>
            <td style="width:2%"> </td>
            <td style="text-decoration:underline;font-size:{fontSize}px;width:20%">{rightkey}</td><td style="width:2%;font-size:{fontSize}px;">{titikduaright}</td><td style="width:25%;font-size:{fontSize}px;">{rightvalue}</td>
            <td style="width:3%"> </td>
        </tr>`;
        var templatePage=`
            ${ (config['leftHeader'] !== undefined && config['leftHeader'] !=="" && config['leftHeader'] !== null) || (config['rightHeader'] !== undefined && config['rightHeader'] !=="" && config['rightHeader'] !== null)?
                `<table>
                    <tr><td></td><td></td><td></td><td></td></tr>
                    <tr><td style="width:7%"> </td>
                        <td>
                            {leftHeader}
                        </td>
                        <td style="width:31%"> </td>
                        <td>
                            {rightHeader}
                        </td>
                    </tr>
                </table>`:''
            }
            <div style="display:flex;margin-bottom:2px;font-size:{fontSize}px;">
                ${config['title']!==undefined?`<h4 style="text-align:center">`+config['title']+`</h4>`:""}
                <table>
                    {headerData}
                </table>
            </div>`;
        var stringData = "";
        if(config['extendData'] !==undefined ){
            data = config.extendData(data);
        }

        var dataKeys = Object.keys(data);
        if(config['only']!==undefined){
            dataKeys=config['only'].filter(function(min){
                if((  Object.keys(data) ).includes(min.includes(":")?min.split(':')[1]:min)){
                    return min;
                }
            });
        }

        if(config['except']!==undefined){
            dataKeys=dataKeys.filter(function(min){
                if(!(config['except']).includes(min.includes(":")?min.split(':')[1]:min)){
                    return min;
                }
            });
        }
        for(let i = 0; i< dataKeys.length-1;i += 2){
            var key = dataKeys[i].includes(":")?dataKeys[i].split(':')[0]: HELPER.upperWord(dataKeys[i]);
            var value =  data[dataKeys[i].includes(":")?dataKeys[i].split(':')[1]:dataKeys[i]]==null||data[dataKeys[i].includes(":")?dataKeys[i].split(':')[1]:dataKeys[i]]===undefined?"-":data[ dataKeys[i].includes(":")?dataKeys[i].split(':')[1]:dataKeys[i] ];
            var keyRight = dataKeys[i+1].includes(":")?dataKeys[i+1].split(':')[0]:HELPER.upperWord(dataKeys[i+1]);
            var valueRight = data[dataKeys[i+1].includes(":")?dataKeys[i+1].split(':')[1]:dataKeys[i+1]]==null||data[dataKeys[i+1].includes(":")?dataKeys[i+1].split(':')[1]:dataKeys[i+1]]===undefined?"-":data[dataKeys[i+1].includes(":")?dataKeys[i+1].split(':')[1]:dataKeys[i+1]];
            stringData += templateRow
                        .replace("{leftkey}", key)
                        .replace("{leftvalue}", value)
                        .replace("{rightkey}", keyRight)
                        .replace("{rightvalue}", valueRight)
                        .replace("{titikduaright}",":");
        }
        if(dataKeys.length%2!=0){
            var key = dataKeys[dataKeys.length-1].includes(":")?dataKeys[dataKeys.length-1].split(':')[0]: HELPER.upperWord(dataKeys[dataKeys.length-1]);
            var value = data[dataKeys[dataKeys.length-1].includes(":")?dataKeys[dataKeys.length-1].split(':')[1]:dataKeys[dataKeys.length-1]]==null||data[dataKeys[dataKeys.length-1].includes(":")?dataKeys[dataKeys.length-1].split(':')[1]:dataKeys[dataKeys.length-1]]===undefined?"-":data[dataKeys[dataKeys.length-1].includes(":")?dataKeys[dataKeys.length-1].split(':')[1]:dataKeys[dataKeys.length-1]];
            stringData += templateRow
                        .replace("{leftkey}", key)
                        .replace("{leftvalue}", value)
                        .replace("{titikduaright}","")
                        .replace("{rightkey}", "")
                        .replace("{rightvalue}", "");
        }
        let stringTemplate = templatePage
                        .replace("{headerData}",stringData)
                        .replace("{title}",config['title']==undefined?"":config['title'])
                        .replace("{rightHeader}",config['rightHeader']==undefined?"":config['rightHeader'].replace("sampleRight",""))
                        .replace("{leftHeader}",config['leftHeader']==undefined?"":config['leftHeader'].replace("sampleHeader","") );
        stringTemplate = stringTemplate
                        .replace(/\{fontSize\}/g,config['fontSize']==undefined?"12":config['fontSize']);
        return (config['break']!==undefined && config['break']===true)? stringTemplate+"<<BREAK>>":stringTemplate;
    },
    getDetail(config){
            var mydata = config;
            var tableTH = `
                <th rowspan="{rowspan}" colspan="{colspan}" style="{border}
                    text-align: center;padding-top: 12px;
                    padding-bottom: 12px;text-align: center;width:{width}">{tambahan}{name}
                </th>`;
            var mytemplate = `
                ${mydata['title']!==undefined?`<h4 style="text-align:center">`+mydata['title']+`</h4>`:""}
                <table style="width:100%">
                    <tr>
                        <td>
                            {leftHeader}
                        </td>
                        <td style="width:32%"> </td>
                        <td>
                            {rightHeader}
                        </td>
                    </tr>
                </table>
                <table  cellspacing="0" cellpadding="3" style="border-collapse: collapse; width: 100%;margin:auto;">
                    <tr style="text-align:center; font-size: {fontSize}px;font-weight:bold;">
                        {tableTH}
                    </tr>
                    {tableTR}
                    <tr>{tableFooter}</tr>
                </table></div>`;

            var borderStyle = "border: 1px solid black;";
            if(mydata["borderHeader"]!==undefined){
                borderStyle = mydata["borderHeader"]+";";
            }
            tableTH = tableTH.replace(/{border}/g,borderStyle);
            var keys = Object.keys(mydata);
            var thead = "";
            var tbody = "";
            var tdsData = [];
            var footers = [];
            for(let i=0;i<keys.length;i++){
                if( !['tableTR','format','data','gridTable'].includes( keys[i]) ){
                    var rgx = new RegExp( `{${keys[i]}}`, "g" );
                    mytemplate = mytemplate.replace( rgx, mydata[keys[i]] );
                }else if(keys[i]=='gridTable'){
                    let rowspan = 1;
                    var children = "";
                    for(let j=0; j<mydata[keys[i]].length; j++){
                        if(mydata[keys[i]][j]['children'] !==undefined){
                            for(let k=0;k<(mydata[keys[i]][j].children).length; k++){
                                if(rowspan ==1 ){rowspan = 2;}
                                mydata[keys[i]][j].children[k]['width']===undefined?"":("width:"+mydata[keys[i]][j].children[k]['width']+";");
                                let child = `
                                    <td style="border:1px solid black;text-align:center;font-weight:bold;font-size:{fontSize}px;{width}">
                                        ${mydata[keys[i]][j].children[k].text}
                                    </td>
                                `;
                                children += child;
                            }
                        }
                    }
                    for(let j=0; j<mydata[keys[i]].length; j++){
                        if(mydata[keys[i]][j]['children'] !==undefined){
                            thead += (tableTH.replace("{width}", mydata[keys[i]][j].width)).replace("{name}", mydata[keys[i]][j].text)
                            .replace("{rowspan}","1").replace("{colspan}", mydata[keys[i]][j]['children'].length)
                            .replace("{tambahan}","");
                            for(let k=0;k<(mydata[keys[i]][j].children).length; k++){
                                tdsData.push({
                                    'dataIndex': mydata[keys[i]][j].children[k]['dataIndex'],
                                    'style'    : mydata[keys[i]][j].children[k]['style'],
                                    'renderer' : mydata[keys[i]][j].children[k]['renderer'],
                                    'align'    : mydata[keys[i]][j].children[k]['align']==undefined?"left":mydata[keys[i]][j].children[k]['align']
                                });
                                footers.push(mydata[keys[i]][j].children[k]['footer']!==undefined?
                                    { 'footer':mydata[keys[i]][j].children[k]['footer'], 'footerBorder': mydata[keys[i]][j].children[k]['footerBorder'] }:undefined);
                            }
                        }else{
                            thead += (tableTH.replace("{width}", mydata[keys[i]][j].width)).replace("{name}", mydata[keys[i]][j].text)
                            .replace("{rowspan}",rowspan).replace("{colspan}", "1")
                            .replace("{tambahan}",rowspan>1?'<div style="font-size:9pt">&nbsp;</div>':"");
                            tdsData.push({
                                'dataIndex' : mydata[keys[i]][j]['dataIndex'],
                                'style'     : mydata[keys[i]][j]['style'],
                                'renderer'  : mydata[keys[i]][j]['renderer'],
                                'align'     : mydata[keys[i]][j]['align']==undefined?"left":mydata[keys[i]][j]['align']
                            });
                            footers.push( mydata[keys[i]][j]['footer']!==undefined?
                                { 'footer': mydata[keys[i]][j]['footer'], 'footerBorder': mydata[keys[i]][j]['footerBorder'] }:undefined);
                        }
                    }
                    if(rowspan>1){
                        thead+=`</tr><tr>${children}`;
                    }
                    mytemplate = mytemplate.replace("{tableTH}",thead);
                }
            }

            if( mydata['data']!==undefined && tdsData.length>0 ){
                let rows = mydata['data'];
                var tds = tdsData;
                for(let j=0; j<rows.length; j++){
                    if(mydata['extendData'] !==undefined ){
                        rows[j] = mydata.extendData(rows[j]);
                    }
                    var td = "";
                    tds.forEach(function(v,index){
                        if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='thousand'){
                            v['renderer'] = HELPER.thousand;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='idr'){
                            v['renderer'] = HELPER.formatIDR;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='usd'){
                            v['renderer'] = HELPER.formatUSD;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='upperword'){
                            v['renderer'] = HELPER.upperWord;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='uppercase'){
                            v['renderer'] = HELPER.toUpperCase;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string' && v['renderer']=='lowercase'){
                            v['renderer'] = HELPER.toLowerCase;
                        }else if(v['renderer']!==undefined && typeof(v['renderer'])=='string'){
                            v['renderer']=undefined;
                        }

                        var rumus = v['renderer']==undefined?(data)=>{return data;}: v['renderer'];
                        var style = v['style']==undefined?`text-align:${v['align']};border:1px solid black;font-size:{fontSize}px;`:v['style'];
                        var value;
                        if(v['dataIndex']=='rownumberer'){
                            value = j+1;
                            style = 'text-align:center;border:1px solid black;font-size:8px;'
                        }else{
                            value = v['dataIndex']==undefined? rumus(rows[j]):rumus( rows[j][v.dataIndex] );
                        }
                        // var value = rumus(rows[j]);
                        if(footers[index]!==undefined){
                            if( typeof(footers[index]['footer'])=='string' ){
                                if( (footers[index]['footer']).toLowerCase() == 'summary' ){
                                    footers[index]['value']=(footers[index]['value'] ===undefined)? parseFloat(rows[j][v.dataIndex]):(footers[index]['value']+parseFloat(rows[j][v.dataIndex]));
                                    if(j==rows.length-1){
                                        footers[index]['value'] = HELPER.thousand(footers[index]['value']);
                                    }
                                }else if( (footers[index]['footer']).toLowerCase() == 'count' ){
                                    if(j==rows.length-1){
                                        footers[index]['value'] = ( rows.length ).toLocaleString()+" rows";
                                    }
                                }
                            }
                        }
                        td += `<td style="${style}">${value===null||value=='null'||value===undefined||value=='undefined'?"":value}</td>`;
                    });
                    tbody += `<tr>${td}</tr>`;
                }
                mytemplate = mytemplate.replace("{tableTR}",tbody.replace(/border:1px solid black;/g,mydata['borderBody']!==undefined&&mydata['borderBody']===false?"":"border:1px solid black;"));
            }
            if( mydata['gridFooter']!==undefined){
                tds = mydata['gridFooter'];
                var td = "";
                tds.forEach(function(v){
                    td += `${v==null?'<td style="{border};"></td>':'<td style="text-align:'+v["align"]+';'+(v["border"]===undefined||v["border"]===true?'{border};':'')+'font-size:{fontSize}px;">'+v["value"]+'</td>'}`;
                });
                var borderStyle = "border-top:1px solid black;border-bottom:1px solid black";
                if(mydata["borderFooter"]!==undefined){
                    borderStyle = mydata["borderFooter"];
                }
                td = td.replace(/{border}/g,borderStyle);
                mytemplate = mytemplate.replace("{tableFooter}",td);
            }else{
                if(footers.length>0){
                    var td = "";
                    footers.forEach(function(v,index){
                        td += `${v===undefined?'<td></td>':'<td style="text-align:'+tdsData[index]["align"]+';'+(v["footerBorder"]===undefined||v["footerBorder"]===true?'{border};':'')+'font-size:{fontSize}px;">'+v["value"]+'</td>'}`;
                    });
                    var borderStyle = "border-top:1px solid black;border-bottom:1px solid black";
                    if(mydata["borderFooter"]!==undefined){
                        borderStyle = mydata["borderFooter"];
                    }
                    td = td.replace(/{border}/g,borderStyle);
                    mytemplate = mytemplate.replace("{tableFooter}",td);
                }else{
                    mytemplate = mytemplate.replace("{tableFooter}","");
                }
            }
            var tup = ""; var tdown = "";
            if(mydata['signatures']!==undefined){
                if(typeof(mydata['signatures'])==='function'){
                    let func = mydata['signatures'];
                    mydata['signatures'] = func();
                }
                mydata['signatures'].forEach(function(sign){
                    tup+=`<th style="text-align:center;font-size:{fontSize}px;">${sign['header']}</th>`;
                    tdown+=`<td style="text-align:center;text-decoration: underline;font-size:{fontSize}px;">${sign['value']}</td>`;
                });
            }
            mytemplate += `
                ${mydata['signaturesBefore']!==undefined?mydata['signaturesBefore']+"<br>":""}
                    <table cellspacing="0" cellpadding="7" style="margin:0px;width:100%">
                        <thead>
                            <tr style="height:10px;">
                                ${tup}
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height:10px;">
                                ${tdown}
                            </tr>
                        </tbody>
                    </table>
                `;
            mytemplate = mytemplate
                        .replace(/{fontSize}/g,mydata['fontSize']==undefined?"12":mydata['fontSize']);
            return (config['break']!==undefined && config['break']===true)? mytemplate+"<<BREAK>>":mytemplate;
    },
    pdfRefresh(arrayData,preview=true,print=false){
        let loading = Ext.getCmp("loadingDialogReport");
        loading.show();
        let footerPage = null;
        try{
            var me = this;
            me.preview=preview;
            var stringTemplate="";
            var htmlTemplate = "";
            if(!Array.isArray(arrayData)){
                if(arrayData.type=='detail'){
                    var stringData = me.getDetail(arrayData);;
                    stringTemplate= JSON.stringify(arrayData['format']) + "<<FORMATTER>>" + stringData;
                    htmlTemplate +=stringData;
                }else{
                    var stringData = me.getPage(arrayData);
                    stringTemplate= JSON.stringify(arrayData['format']) + "<<FORMATTER>>" + stringData;
                    htmlTemplate +=stringData;
                }
                if(arrayData['footerPage']!==undefined){
                    footerPage=arrayData['footerPage'];
                }
                if(arrayData['preview']!==undefined){
                    me.preview=arrayData['preview'];
                }
            }else{
                var me = this;
                arrayData.forEach(function(config,index){
                    if(index>0 && arrayData[index-1]['break']===false){
                        var formatter = "";
                    }else{
                        var formatter = JSON.stringify(config['format']) + "<<FORMATTER>>";
                    }
                    if(config.type=='detail'){
                        var stringData = me.getDetail(config);
                        stringTemplate += formatter + stringData;
                        htmlTemplate += stringData;
                    }else{
                        var stringData = me.getPage(config);
                        stringTemplate+= formatter + stringData;
                        htmlTemplate += stringData;
                    }
                    if(config['footerPage']!==undefined){
                        footerPage=config['footerPage'];
                    }

                    if(config['preview']!==undefined){
                        me.preview=config['preview'];
                    }
                });
            }
        }catch(e){
            loading.hide();
            console.log(e);
            Ext.Msg.alert("Failed!","Maybe Data Error");
        }

        let xhr = new XMLHttpRequest();
        let url = qlconfig['backend_pdf_renderer']===undefined?"https://qqltech.com/git_pdf_renderer/one.php":qlconfig['backend_pdf_renderer'];
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            loading.hide();
            if (this['status'] == 200) {
            var blob = new Blob([this['response']], {type: 'application/pdf'});
            var url = URL.createObjectURL(blob);
                if(print){
                    let elementPrint = me.down("panel").renderElement.dom.getElementsByTagName("iframe")[0];
                    elementPrint.setAttribute("src",url+"#toolbar=0");
                    if(Ext.browser.is.firefox){
                        elementPrint.setAttribute("style","margin-top:-60px");
                    }
                    elementPrint =  elementPrint.contentWindow;
                    setTimeout(function(){
                        elementPrint.print();
                        me.handlerBeforePrint();
                    }, 1000);
                }else{
                    me.down("panel").renderElement.dom.getElementsByTagName("iframe")[0].setAttribute("src",url+"#toolbar=0");
                    if(Ext.browser.is.firefox){
                        me.down("panel").renderElement.dom.getElementsByTagName("iframe")[0].setAttribute("style","margin-top:-60px");
                    }
                }
            }
        };
        let data = {
            data : stringTemplate.replace(/null/g,"-").replace(/undefined/g,"-")
        };
        if(qlconfig["report_header_design"]!==undefined){
            Object.assign(data,{
                header : qlconfig["report_header_design"]
            });
        }
        if(footerPage!==null){
            Object.assign(data,{
                footer : footerPage
            });
        }
        Object.assign(data,{
            preview : me.preview
        });
        xhr.send(JSON.stringify(data));
        this.htmlString = htmlTemplate.replace(/<<BREAK>>/g,"");

    },
    handlerBeforePrint:function(){},
    print:function(data,handlerBeforePrint){
        let me = this;
        me.handlerBeforePrint=handlerBeforePrint;
        me.pdfRefresh(data,false,true);
    },
    handlerNewTab:function(){
        if(this.htmlString==null){
            Ext.Msg.alert("Stop!","Please Click View Report First!");
            return true;
        }
        var newWindow = window.open();
        newWindow.document.write("<html>"+this.htmlString+"</html>");
        // newWindow.document.title="Report";
        newWindow.document.close();
        // var data = "<p>This is 'myWindow'</p>";
        // window.open("data:text/html," + encodeURIComponent(this.htmlString),"_blank");
    },
    downloadXls:function(title="file"){
        if(this.htmlString==null){
            Ext.Msg.alert("Stop!","Please Click View Report First!");
            return true;
        }
        let loading = Ext.getCmp("loadingDialogReport");
        loading.show();
        let xhr = new XMLHttpRequest();
        let url = qlconfig['backend_xls_renderer']===undefined?"https://backend.dejozz.com/pdfrenderer/excel.php":qlconfig['backend_xls_renderer'];
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.responseType = 'blob';
        xhr.onload = function(e) {
            if (this.status === 200) {
                var blob = this.response;
                var contentTypeHeader = xhr.getResponseHeader("Content-Type");
                var downloadLink = window.document.createElement('a');
                downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                downloadLink.download = title+".xls";
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
               }
            loading.hide();
        };
        let data = {
            data : this.htmlString
        };
        xhr.send(JSON.stringify(data));
    },
    tbar:[
        {   text : "Export PDF", iconCls:"far fa-file-pdf" },
        {   text : "Export Excel", iconCls:"far fa-file-excel" }
    ],
    items:[{
        xtype: "panel",
        html:`<iframe width="100%" type="application/pdf" frameborder="0"/>`,
        afterRender:function(){
            this.renderElement.dom.getElementsByTagName("iframe")[0].setAttribute("height",this.up("panel").getHeight());
        }
    }],
    withButtons:true,
    afterRender:function(){
        if(!this.withButtons){
            this.setTbar([])
        }
    }
});