Ext.define("practice_1_layout",{
    extend:"Ext.panel.Panel",
    xtype:"practice_1_layout",
    fbodyPadding: 10,
    defaultType: 'panel',
    scrollable:true,
    layout: {
        type: 'vbox'
    },

    defaults: {
        bodyPadding: 10,
        border: true
    },

    items: [{
        title:"<a href='https://examples.sencha.com/extjs/7.2.0/examples/kitchensink/#layouts' target='_blank'>Panel Tutorial</a>",
        flex: 1,
        margin: '0 0 10 0',
        html: 'Panel Saja'
    },{
        title:"panel 2",
        height: 200,
        layout: {
            type: 'hbox'
        },    
        defaultType: 'panel',
        defaults: {
            bodyPadding: 10,
            border: true
        },
    
        items: [{
            title: 'Panel 1',
            flex: 1,
            margin: '0 10 0 0',
            html: 'Panel Saja'
        }, {
            title: 'Panel 2',
            width: 100,
            margin: '0 10 0 0',
            html: 'width : 100'
        }, {
            title: 'Panel 3',
            flex: 2,
            html: 'Panel Saja'
        }]
    }, {
        title:"panel 2",
        height: 300,
        layout: {
            type: 'hbox'
        },    
        defaultType: 'panel',
        defaults: {
            bodyPadding: 10,
            border: true
        },
        items: [{
            title: 'Panel 1',
            margin: '0 10 0 0',
            html: 'Panel Saja'
        },{
            height:300,
            flex: 1,
            title:"panel 2",
            layout: {
                type: 'vbox'
            },    
            defaultType: 'panel',
            defaults: {
                bodyPadding: 10,
                border: true
            },
        
            items: [{
                title: 'Panel 1',
                margin: '0 10 0 0',
                html: 'Panel Saja'
            },{
                title: 'Panel 2',
                margin: '0 10 0 0',
                html: 'width : 100'
            }]
        }]
    }]
})