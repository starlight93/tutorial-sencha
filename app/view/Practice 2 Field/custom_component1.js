Ext.define('customComponentClass',{
    extend:"Ext.Container",
    xtype:'customComponent1',
    layout:'hbox',
    items:[{
        xtype:"checkboxfield",
        name:"checkboxfield",
        label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Checkbox.html' target='_blank'>Check One</a>",
        // style:"margin-left:3px",
        labelWrap:false,
        width:"50%",
        items:[{
            label:"A",value:"A"
        },{
            label:"B",value:"B"
        }]
    },{
        xtype:"checkboxgroup",
        name:"checkboxgroup",
        label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.CheckboxGroup.html' target='_blank'>Check Many</a>",
        // style:"margin-left:3px",
        labelWrap:false,
        width:"50%",
        items:[{
            label:"A",value:"A"
        },{
            label:"B",value:"B",checked:true
        }]
    }]
})