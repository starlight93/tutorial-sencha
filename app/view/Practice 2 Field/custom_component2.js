Ext.define('customComponentClass2',{
    extend:"Ext.Container",
    xtype:'customComponent2',
    layout:'hbox',
    style:{
        "margin-top":"5px"
    },
    defaults:{
        style:{
            "margin-left":"5px"
        }
    },
    items:[{
        xtype:'button', 
        text:'Alert',
        iconCls:"x-fa fa-asterisk",
        ui:'decline',
        handler:function(el){
            Ext.Msg.alert('JUDUL ALERT','ISI ALERT')
        }
    },{
        xtype:'button', 
        text:'Confirmation',
        iconCls:"x-fa fa-bolt",
        ui:'confirm',
        handler:function(el){            
            Ext.Msg.confirm('Confirmation', 'Yakin?',
                function(answer) {
                    if(answer=="yes"){
                       Ext.Msg.alert('Confirm', 'anda memilih yes');                        
                    }else{
                        Ext.Msg.alert('Confirm', 'anda memilih no');         
                    }
                }
            );
        }
    },{
        xtype:'button', 
        text:'Dialog',
        iconCls:"x-fa fa-battery-half",
        ui:'action',
        handler:function(el){
            Ext.create({
                xtype:"dialog",
                title:"judul dialog",
                width: '600px',
                closable:true,
                layout:"fit",
                items:[{
                    xtype:"customComponent3"
                }]
            }).show();
        }
    },]
})