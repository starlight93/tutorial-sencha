Ext.define('customComponentClass3',{
    extend:"Ext.form.Panel",
    xtype:'customComponent3',
    items:[{
        required:true,
        name:"username",
        xtype:'textfield',
        label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Email.html' target='_blank'>Email</a>"
    },{
        required:true,
        name:"password",
        xtype:'passwordfield',
        label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Password.html' target='_blank'>Password</a>"
    }],
    buttons:[{
        text:"Gaya Login",
        iconCls:'x-fa fa-lock',
        handler:function(currentElement){
            let aksesForm =  currentElement.up('formpanel');
            if(!aksesForm.validate()){
                Ext.Msg.alert('Salah Rek', 'ada yang tidak beres, lihat CONSOLE (F12)');
            }else{
                Ext.Msg.alert('Selamat', 'anda Gaya Login, lihat CONSOLE (F12)');
            }
            let formValues = aksesForm.getValues();
            console.log(formValues);
        }
    }]
})