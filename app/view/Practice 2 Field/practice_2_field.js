Ext.define("practice_2_field",{
    extend:"Ext.panel.Panel",
    xtype:"practice_2_field",
    fbodyPadding: 10,
    defaultType: 'panel',
    scrollable:true,
    defaults: {
        bodyPadding: 10,
        // border: true
    },

    items: [{
        xtype   : "formpanel",
        title:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.form.Panel.html' target='_blank'>Formpanel</a>",
        border:true,
        scrollable:true,
        items   : [{
            xtype:"customComponent1"
        },
        //
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"numberfield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Number.html' target='_blank'>Number</a>",
                maxHeight:"38px",
                labelWrap:false,
                width:"50%"
            },{
                xtype:"qluploadfield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.File.html' target='_blank'>Upload</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            }]
        },
    //
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"datefield",
                name:"datefield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Date.html' target='_blank'>Datefield</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                value:new Date,
                width:"50%",
                dateFormat:'d/m/Y',
                validators: 'date'
            },{
                xtype:"radiogroup",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.RadioGroup.html' target='_blank'>Radiogroup</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                items:[{
                    label: "A",value:"A"
                },{
                    label: "B",value:"B", checked:true
                }]
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"qldomainfield",
                label:"Domain",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            },{
                xtype:"qlpopupfield",
                name:"mypopup",
                label:"Popup",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            },]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"combobox",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select2</a>",
                editable:true,
                autoComplete:false,
                labelWrap:false,
                width:"50%",
                displayField: 'text',
                valueField: 'value',
                store:[{
                    text: "Pilihan A",value:"A"
                },{
                    text: "Pilihan B",value:"B"
                }]
            },{
                xtype:"combobox",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select</a>",
                name:"Select",
                editable:false,
                autoSelect:true,
                forceSelection:true,
                labelWrap:false,
                width:"50%",
                displayField: 'text',
                valueField: 'value',
                store:[{
                    text: "Pilihan A",value:"A"
                },{
                    text: "Pilihan B",value:"B"
                }]
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:"qltextnumberfield",
                label:"TextNumber",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                allow:[",","-","."]
            },{
                xtype:"timefield",
                label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Time.html' target='_blank'>Timefield</a>",
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%",
                format:'H:i',
                value:"13:00"
            }]
        },
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'togglefield',
                labelWrap:false,
                width:"50%",
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Toggle.html" target='_blank'>Toggle</a>`,
                value: true
            },{
                xtype: 'sliderfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Slider.html" target='_blank'>Slider</a>`,
                labelWrap:false,
                width:"50%",
                value: 20,
            }]
        },
        
    // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'textareafield',
                labelWrap:false,
                width:"50%",
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.TextArea.html" target="_blank">Textarea</a>`,
            },{
                xtype: 'searchfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Search.html" target="_blank">Searchfield</a>`,
                flex: 1,
                maxWidth: '50%',
                maxHeight:"38px",
                shadow: false,
                placeholder: 'Search',
            }]
        },
        // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype: 'textfield',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Text.html" target="_blank">Textfield</a>`,
                flex: 1,
                maxWidth: '50%',
                placeholder: 'Text',
            },{
                xtype:"qlnumberfield",
                label:"Number2",
                decimals:3,
                // style:"margin-left:3px",
                labelWrap:false,
                width:"50%"
            }]
        },
        // =========================
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:'containerfield',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan1</a>`,
                width:"50%",
                items: [{
                    xtype: 'numberfield',
                    flex: 1,
                    maxWidth: '48%',
                    shadow: false,
                    placeholder: 'Angka',
                    style:"margin-left:5px;margin-right:10px"
                },{
                    xtype: 'selectfield',
                    flex: 1,
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "CM",value:"cm"
                    },{
                        text: "KM",value:"km"
                    }],
                    value:"cm"
                }]
            },{
                xtype:'containerfield',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan2</a>`,
                width:"50%",
                items: [{
                    xtype: 'numberfield',
                    maxWidth: '35%',
                    placeholder: 'Angka',
                    style:"margin-right:10px"
                },{
                    xtype: 'label',
                    maxWidth: '25%',
                    html: 'Satuan',
                    style:"margin-right:10px;margin-top:10px;font-weight: bold;"
                },{
                    xtype: 'selectfield',
                    maxWidth: '30%',
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "CM",value:"cm"
                    },{
                        text: "KM",value:"km"
                    }],
                    value:"cm"
                },{
                    xtype: 'label',
                    html: 'Mantap',
                    style:"margin-left:10px;margin-top:10px;font-weight: bold;"
                },]
            }]
        },
        // =========================
        {
            xtype:'label',
            html : "<h3>Sandingan 50%</h3>"
        },
        {
            xtype:'container',
            layout:'hbox',
            items:[{
                xtype:'container',
                layout:'hbox',
                width:"50%",
                items: [{
                    xtype: 'selectfield',
                    flex: 1,
                    maxHeight:"38px",
                    shadow: false,
                    width: "27%",
                    maxWidth: '130px',
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "Mr",value:"mr"
                    },{
                        text: "Mrs",value:"mrs"
                    }],
                    value:"mr"
                },{
                    xtype: 'searchfield',
                    flex: 1,
                    maxHeight:"38px",
                    maxWidth: '32%',
                    shadow: false,
                    placeholder: 'Search',
                    style:"margin-left:5px;margin-right:10px"
                }, {
                    xtype: 'button',
                    height: 32,
                    iconCls: 'x-fa fa-arrow-right',
                    style:"margin-top:5px;margin-right:10px"
                },{
                    xtype: 'button',
                    height: 32,
                    iconCls: 'x-fa fa-arrow-left',
                    ui:'action',
                    style:"margin-top:5px;margin-right:10px"
                }]
            }]
        },
    // =========================
        {
            xtype:'label',
            html : "<h3>Sandingan Melintang 100%</h3>"
        },
        {
            xtype:'container',
            layout:'hbox',
            items: [{
                xtype: 'selectfield',
                label: "Panjang",
                shadow: false,
                width: "35%",
                displayField: 'text',
                valueField: 'value',
                store:[{
                    text: "Mr",value:"mr"
                },{
                    text: "Mrs",value:"mrs"
                }],
                value:"mr"
            },{
                xtype: 'searchfield',
                width: '50%',
                shadow: false,
                placeholder: 'Search',
                style:"margin-left:5px;margin-right:10px"
            }, {
                xtype: 'checkboxfield',
                style:"margin-top:5px;margin-right:5px"
            }, {
                xtype: 'label',
                html:"centang",
                style:"margin-top:13px;font-weight:bold"
            }]
            
        },// =========================
        {
            xtype:'label',
            html : "<h3>BUTTONS</h3>"
        },
    // =========================
        {
            xtype:'label',
            html : "<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.Button.html' target='_blank'>Button Doc</a>"
        },
        // =========================
        {
            xtype:'label',
            html : "<a href='https://examples.sencha.com/extjs/7.2.0/examples/kitchensink/#all' target='_blank'>Button Examples</a>"
        },
        // =========================
        {
            xtype:'label',
            html : "<a href='https://fontawesome.com/cheatsheet' target='_blank'>Font Awesome</a>"
        },
        {
            xtype:'customComponent2'
        },
        
      ]
    }]
})