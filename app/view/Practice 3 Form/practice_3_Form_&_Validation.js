Ext.define("practice_3_form",{
    extend:"Ext.panel.Panel",
    xtype:"practice_3_form",
    fbodyPadding: 10,
    defaultType: 'panel',
    collapsible:true,
    scrollable:true,
    defaults: {
        bodyPadding: 10,
        // border: true
    },

    items: [{
        titleCollapse:true,
        titleAlign  : "right",
        xtype   : "formpanel",
        id      : "formId",
        title   : "<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.form.Panel.html' target='_blank'>Formpanel <b>PELAJARI METHODS</b></a>",
        border  : true,
        scrollable  : true,
        items   : [{
                xtype:"customComponent1"
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:"numberfield",
                    name:"number",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Number.html' target='_blank'>Number</a>",
                    maxHeight:"38px",
                    labelWrap:false,
                    width:"50%"
                },{
                    xtype:"qluploadfield",
                    name:"upload",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.File.html' target='_blank'>Upload</a>",
                    // style:"margin-left:3px",
                    labelWrap:false,
                    width:"50%"
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:"datefield",
                    name:"date",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Date.html' target='_blank'>Datefield</a>",
                    // style:"margin-left:3px",
                    labelWrap:false,
                    value:new Date,
                    width:"50%",
                    dateFormat:'d/m/Y',
                    validators: 'date'
                },{
                    xtype:"radiogroup",
                    name:"radiogroup",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.RadioGroup.html' target='_blank'>Radiogroup</a>",
                    // style:"margin-left:3px",
                    labelWrap:false,
                    width:"50%",
                    items:[{
                        label: "A",value:"A"
                    },{
                        label: "B",value:"B", checked:true
                    }]
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:"qldomainfield",
                    label:"Domain",
                    name:"domain",
                    labelWrap:false,
                    width:"50%"
                },{
                    xtype:"qlpopupfield",
                    name:"popup",
                    label:"Popup",
                    labelWrap:false,
                    width:"50%"
                },]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:"combobox",
                    name:"combo_search",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select2</a>",
                    editable:true,
                    autoComplete:false,
                    labelWrap:false,
                    width:"50%",
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "Pilihan A",value:"A"
                    },{
                        text: "Pilihan B",value:"B"
                    }]
                },{
                    xtype:"combobox",
                    name:"combo_select",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.ComboBox.html' target='_blank'>Select</a>",
                    name:"Select",
                    editable:false,
                    autoSelect:true,
                    forceSelection:true,
                    labelWrap:false,
                    width:"50%",
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "Pilihan A",value:"A"
                    },{
                        text: "Pilihan B",value:"B"
                    }]
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:"qltextnumberfield",
                    label:"TextNumber",
                    name:"textnumber",
                    // style:"margin-left:3px",
                    labelWrap:false,
                    width:"50%",
                    allow:[",","-","."]
                },{
                    xtype:"timefield",
                    name:"time",
                    label:"<a href='https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Time.html' target='_blank'>Timefield</a>",
                    // style:"margin-left:3px",
                    labelWrap:false,
                    width:"50%",
                    format:'H:i',
                    value:"13:00"
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype: 'togglefield',
                    name:"toggle",
                    labelWrap:false,
                    width:"50%",
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Toggle.html" target='_blank'>Toggle</a>`,
                    value: true
                },{
                    xtype: 'sliderfield',
                    name:"slider",
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Slider.html" target='_blank'>Slider</a>`,
                    labelWrap:false,
                    width:"50%",
                    value: 20,
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype: 'textareafield',
                    name:"textarea",
                    labelWrap:false,
                    width:"50%",
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.TextArea.html" target="_blank">Textarea</a>`,
                },{
                    xtype: 'searchfield',
                    name:"search",
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Search.html" target="_blank">Searchfield</a>`,
                    flex: 1,
                    maxWidth: '50%',
                    maxHeight:"38px",
                    shadow: false,
                    placeholder: 'Search',
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype: 'textfield',
                    name:"text",
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Text.html" target="_blank">Textfield</a>`,
                    flex: 1,
                    maxWidth: '50%',
                    placeholder: 'Text',
                },{
                    xtype:"qlnumberfield",
                    name:"decimal",
                    label:"Number2",
                    labelWrap:false,
                    width:"50%"
                }]
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:'containerfield',
                    layout:'hbox',
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan1</a>`,
                    width:"50%",
                    items: [{
                        xtype: 'numberfield',
                        name:"number_sandingan1",
                        flex: 1,
                        maxWidth: '48%',
                        shadow: false,
                        placeholder: 'Angka',
                        style:"margin-left:5px;margin-right:10px"
                    },{
                        xtype: 'selectfield',
                        name:"select_sandingan",
                        flex: 1,
                        displayField: 'text',
                        valueField: 'value',
                        store:[{
                            text: "CM",value:"cm"
                        },{
                            text: "KM",value:"km"
                        }],
                        value:"cm"
                    }]
                },{
                    xtype:'containerfield',
                    layout:'hbox',
                    label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.field.Container.html" target="_blank">Sandingan2</a>`,
                    width:"50%",
                    items: [{
                        xtype: 'numberfield',
                        name:"number_sandingan2",
                        maxWidth: '35%',
                        placeholder: 'Angka',
                        style:"margin-right:10px"
                    },{
                        xtype: 'label',
                        maxWidth: '25%',
                        html: 'Satuan',
                        style:"margin-right:10px;margin-top:10px;font-weight: bold;"
                    },{
                        xtype: 'selectfield',
                        name:"select_sandingan2",
                        maxWidth: '30%',
                        displayField: 'text',
                        valueField: 'value',
                        store:[{
                            text: "CM",value:"cm"
                        },{
                            text: "KM",value:"km"
                        }],
                        value:"cm"
                    },{
                        xtype: 'label',
                        html: 'Mantap',
                        style:"margin-left:10px;margin-top:10px;font-weight: bold;"
                    },]
                }]
            },{
                xtype:'label',
                html : "<h3>Sandingan 50%</h3>"
            },{
                xtype:'container',
                layout:'hbox',
                items:[{
                    xtype:'container',
                    layout:'hbox',
                    width:"50%",
                    items: [{
                        xtype: 'selectfield',
                        name:   "select_sandingan3",
                        flex: 1,
                        maxHeight:"38px",
                        shadow: false,
                        width: "27%",
                        maxWidth: '130px',
                        displayField: 'text',
                        valueField: 'value',
                        store:[{
                            text: "Mr",value:"mr"
                        },{
                            text: "Mrs",value:"mrs"
                        }],
                        value:"mr"
                    },{
                        xtype: 'searchfield',
                        name:   "search_sandingan3",
                        flex: 1,
                        maxHeight:"38px",
                        maxWidth: '32%',
                        shadow: false,
                        placeholder: 'Search',
                        style:"margin-left:5px;margin-right:10px"
                    }, {
                        xtype: 'button',
                        height: 32,
                        iconCls: 'x-fa fa-arrow-right',
                        style:"margin-top:5px;margin-right:10px"
                    },{
                        xtype: 'button',
                        height: 32,
                        iconCls: 'x-fa fa-arrow-left',
                        ui:'action',
                        style:"margin-top:5px;margin-right:10px"
                    }]
                }]
            },{
                xtype:'label',
                html : "<h3>Sandingan Melintang 100%</h3>"
            },{
                xtype:'container',
                layout:'hbox',
                items: [{
                    xtype: 'selectfield',
                    name:   "select_sandingan_melintang",
                    label: "Panjang",
                    shadow: false,
                    width: "35%",
                    displayField: 'text',
                    valueField: 'value',
                    store:[{
                        text: "Mr",value:"mr"
                    },{
                        text: "Mrs",value:"mrs"
                    }],
                    value:"mr"
                },{
                    xtype: 'searchfield',
                    name:   "search_sandingan4",
                    width: '50%',
                    shadow: false,
                    placeholder: 'Search',
                    style:"margin-left:5px;margin-right:10px"
                }, {
                    xtype: 'checkboxfield',
                    name:   "check_sandingan4",
                    style:"margin-top:5px;margin-right:5px"
                }, {
                    xtype: 'label',
                    html:"centang",
                    style:"margin-top:13px;font-weight:bold"
                }]
                
            },{
                xtype:'container',
                layout:'hbox',
                label: `<a href="https://docs.sencha.com/extjs/7.2.0/modern/Ext.Button.html#cfg-ui" target="_blank">UI STYLE BUTTON</a>`,
                width:"50%",
            }
            
        ],
        buttons:[{
            text:'Reset', ui:"decline",handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        },{
            text:'Set Values', ui:"confirm",handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        },{
            text:'Get Values', ui:"action",handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        },{
            text:'Set ReadOnly', ui: 'plain', style: 'background-color: orange', handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        },{
            text:'Validate', ui: 'plain', style: 'background-color: black', handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        },{
            text:'Sulap', ui: 'plain', style: 'background-color: pink;color:black;', handler:function(button){
                Ext.Msg.alert('alert','garapen dewe')
            }
        }]
    }]
})