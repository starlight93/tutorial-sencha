Ext.define("practice_3_grid_table",{
    extend:"Ext.panel.Panel",
    xtype:"practice_3_grid_table",
    bodyPadding: 10,
    defaultType: 'panel',
    scrollable:true,
    // html:"hello world!"
    items: [{
        id:"gridsiswa",
        scrollable:true,
        xtype:"grid",
        itemConfig:{
            viewModel:true
        },
        title:"table saya",
        fullscreen:true,
        rowLines:true,
        columnLines:true,
        store:{
            // storeId:"store_data_siswa",
            data:[{
                tgl_lahir:"01/02/1990", nama:"Fajar Firmansyah", kelas:"12 tkj 1", hobi:"main bola", namaOptions: [
                    {text:"Fajar Firmansyah"},{text:"Fajar2"},{text:"Fajar3"}
                ]
            },{
                tgl_lahir:"01/05/1990", nama:"Budiono", kelas:"12 tkj 2", hobi:"main badminton & catur", namaOptions: [
                    {text:"Budiono"},{text:"Budiono2"},{text:"Budiono3"}
                ]
            },{
                tgl_lahir:"01/06/1990", nama:"Ariono", kelas:"12 rpl 1", hobi:"main ML", namaOptions: [
                    {text:"Ariono"},{text:"Ariono2"},{text:"Ariono3"}
                ]
            }]
        },
        columns:[{
            width:"10%",
            text:"Nomor",
            xtype:"rownumberer",
            align:"center"
        },{
            width:"20%",
            text:"Nama Lengkap",
            dataIndex:"nama",
            cell:{
                xtype:"widgetcell",
                // style:"padding:5px",
                widget:{
                    style:"max-height:30px;padding-bottom:2px;",
                    xtype:"selectfield",
                    // store:[
                    //     {text:"Fajar Firmansyah"},{text:"Budiono"},{text:"Ariono"}
                    // ],
                    bind:{
                        store:"{record.namaOptions}"
                    },
                    valueField:"text",
                    displayField:"text",
                    label:null
                }
            }
           
        },{
            width:"20%",
            text:"Kelas",
            dataIndex:"kelas",
            cell:{
                xtype:"widgetcell",
                // style:"padding:5px",
                widget:{
                    style:"max-height:30px;padding-bottom:2px;",
                    xtype:"textfield",
                    bind:{
                        readOnly:"{record.kelas=='12 rpl 1'}"
                    },
                    label:null
                }
            }
        },{
            width:"15%",
            text:"Tanggal",
            cell:{
                xtype:"widgetcell",
                widget:{
                    style:"max-height:30px;padding-bottom:2px;",
                    xtype:"datefield",
                    bind:{
                        value:"{record.tgl_lahir}"
                    },
                    dateFormat:"d/m/Y",
                    listeners:{
                        change:function(el,val){            
                            el.setBind({
                                value:null
                            });           
                            let dateOk =    Ext.Date.format(val,"d/m/Y")
                            el.up('widgetcell').getRecord().set('tgl_lahir',dateOk)
                        }
                    }
                }
            }
        },{
            width:"15%",
            text:"Nama & Hobi",
            tpl: '{nama} & {hobi}'
        },{
            width:"20%",
            text:"Action",
            align:'center',
            cell:{
                tools:{
                    delete:{
                        iconCls:"x-fa fa-trash",
                        bind:{
                            hidden:"{record.kelas=='12 rpl 1'}"
                        },
                        handler:function(el,row){
                            let record = row.record;
                            let store = row.record.store;
                            store.remove(record);
                            Ext.Msg.alert("Berhasil",`Penghapusan data ${ record.data.nama }`);
                        }
                    }
                }
            }
        }]
    }]
})