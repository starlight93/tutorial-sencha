
Ext.define('qlproject.view.login.login',{
    extend: 'Ext.Container',
    xtype: 'Login',
    cls: 'login',
    requires: [
        'qlproject.view.login.loginController',
        'qlproject.view.login.loginModel',
        'Ext.form.Panel'
    ],

    controller: 'login-login',
    viewModel: {
        type: 'login-login'
    },

    layout: {
        type:'vbox',
        align: 'middle',
        pack: 'center',
    },
    cls: 'go-login',
    renderTo: Ext.getBody(),
    items: [
        {
            cls:'go-login-box',
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: "container",
                    cls: "go-login-box__art",
                    items:[
                        {
                            html:`
                                <div class="go-login-box__art-desc">
                                    
                                </div>
                            `,
                        }
                    ]
                },{
                    xtype: 'formpanel',
                    cls:'go-login-box__form',
                    items:[
                        {  
                            cls: 'go-login-logo',
                            html: '<h1><img src=resources/images/developer.png></h1>',
                        },  
                        {
                            xtype: 'textfield',
                            label: 'Name',
                            id: 'name',
                            name:"email",
                            value: '',
                            // autoComplete:false,
                            required: true,
                            clearable: true,
                            placeholder: 'Name',
                        },
                        {
                            xtype: 'passwordfield',
                            label: 'Password',
                            id: 'password',
                            name: "password",
                            value: '',
                            required: true,
                            clearable: true,
                            revealable: true,
                            placeholder: 'Password',
                            listeners:{
                                action:'QLLoginExecuting'
                            }
                        },
                         {
                             xtype: 'checkbox',
                             boxLabel: 'Remember me',
                             name: 'remember'
                         },
                        {
                            xtype: 'button',
                            text : 'Login',
                            ui: 'action',
                            cls: 'go-login-btn',
                            width: '100%',
                            handler: 'QLLoginExecuting'
                        },
                        {  
                            cls: 'menu-setting',
                            xtype: 'button',
                            iconCls :'x-fa fa-cog',
                            handler : 'openMenu'
                        },  

                    ]
                }
            ]

        },
    ]
});
