Ext.define('qlproject.view.login.loginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login-login',
    openMenu:function(){
        try{Ext.getCmp('serverConfigurationForm').destroy()}catch(e){}
        var menu = Ext.create({
            xtype: 'actionsheet',   
            autoSize: true,
            bodyPadding: 10,
            scrollable: true,
            width: "30%",
            items: [{
                xtype:'formpanel',
                id: "serverConfigurationForm",
                title:'Server Configuration',
                items:[
                    {
                        xtype : 'selectfield',
                        label : 'Server Name',
                        name:'serverName',
                        required:true,
                        labelWidth:"100%",
                        labelAlign:'left',
                        store : 'store_offline__server',
                        valueField : 'serverName',
                        displayField : 'serverName',
                        autoSelect:true,
                        listeners:{
                            select:function(el,val){
                                el.up('formpanel').setValues(val.data);
                                return;
                                localStorage.server_configuration=JSON.stringify(val.data);
                            }
                        }
                    },
                    {
                        xtype : 'textfield',
                        label : 'Server URL',
                        name : "serverUrl",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Login URL',
                        name : "serverAuthLogin",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Auth URL',
                        name : "serverAuthCheck",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Public Path',
                        name : "serverPublicPath",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Server PDF',
                        name : "serverPdf",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Server XLS',
                        name : "serverXls",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Server Barcode',
                        name : "serverBarcode",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                    {
                        xtype : 'textfield',
                        label : 'Server WebSock',
                        name : "serverWebsock",
                        readOnly    : true,
                        required : true,
                        labelWidth:"100%",
                        labelAlign  :'left'
                    },
                ],
                buttons:[
                    {
                        text : 'Save',
                        ui : 'action',
                        handler:function(el){
                            localStorage.server_configuration=JSON.stringify(el.up('formpanel').getValues());
                            Ext.Viewport.hideMenu('right'); 
                            api.refreshServer();
                            Ext.Msg.alert("Saved!","Default Server has been saved");            
                            return;
                            let form = this.up('formpanel');
                            let arrayAddress = form.getValues();
                            for(x in arrayAddress){
                                if(!arrayAddress[x].includes('http')){
                                    continue;
                                }
                                Ext.Ajax.request({
                                    url: arrayAddress[x],
                                    method:"GET",
                                    success: function(response, opts) {
                                        console.log('sukses')
                                    },    
                                    failure: function(response, opts) {                
                                        if( ['404','0'].includes((response.status).toString()) ){
                                            console.log('gagal')
                                        }else{
                                            console.log('dianggap ada')
                                        };
                                    }
                                });
                            }

                        }
                    }
                ],
            }],
        });
        
        
        Ext.Viewport.setMenu(menu, {
            side: 'right',
            reveal: true
        });
        
        Ext.Viewport.showMenu('right');        
        if(localStorage.server_configuration!==undefined){
            Ext.getCmp('serverConfigurationForm').setValues(JSON.parse(localStorage.server_configuration));
        }else{
            Ext.getCmp('serverConfigurationForm').setValues(getStoreData('store_offline__server')[0])
        }
    }
});
