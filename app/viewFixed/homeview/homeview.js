
Ext.define('qlproject.view.homeview.homeview',{
    extend: 'Ext.tab.Panel',
    xtype: "Home",
    requires: [
        'qlproject.view.homeview.homeviewController',
        'qlproject.view.homeview.homeviewModel'
    ],

    controller: 'homeview-homeview',
    // controller: 'homeview-homeview',
    width: 650,
    height: 400,

    viewModel: {
    },

    items: [  {
        title: 'Modules',
        layout: 'fit',
        items: [{
            xtype: 'panel',
            layout: 'fit',
            ui: 'default',
            bodyPadding: 10,
            padding:10,
            items:[
                {
                    xtype: 'dataview',
                    // layout: 'fit',
                    ui: 'default',
                    inline: true,
                    store: {
                        fields: ['name'],
                        autoLoad: true,
                        data :[
                            {name: 'Master',img: "resources/images/modules.png"},
                            {name: 'Inventory',img: "resources/images/modules.png"},
                            {name: 'Marketing',img: "resources/images/modules.png"},
                            {name: 'Purchasing',img: "resources/images/modules.png"},
                            {name: 'Production',img: "resources/images/modules.png"},
                            // {name: 'Fixed Asset',img: "resources/images/modules.png"},
                            // {name: 'Accounting',img: "resources/images/modules.png"}
                        ]
                    },
                    itemTpl: '<div class="dataviewmultisortitem2" style="padding:50 50 50 50">' +
                                '<img draggable="false" class="dataviewimage2" src="{img}"/>' +
                                '<center><strong>{name}</strong></center>' +
                            '</div>',
                    listeners:{
                        childtap:function(view,location){
                        }
                    }
                }
            ]
        }]
    }]
    
});
