Ext.define('project.MainView', {
    extend: 'Ext.Container',
    id:"projectmainview",
    xtype: 'projectmainview',
    controller: 'projectmainviewcontroller',
    viewModel: {
        type: 'projectmainviewmodel'
    },
    requires: [
    'Ext.layout.Fit'
    ],
    layout: 'fit',
    items: [
        { xtype: 'projectnavview',    reference: 'projectnavview',    docked: 'left',   bind: {width:  '{navview_width}'}, listeners: { select: "onMenuViewSelectionChange"} },
        { xtype: 'projectheaderview', reference: 'projectheaderview', docked: 'top',    bind: {height: '{headerview_height}'} },
        // { xtype: 'projectfooterview', reference: 'projectfooterview', docked: 'bottom', bind: {height: '{footerview_height}'} },
        { xtype: 'projectcenterview', reference: 'projectcenterview' },
        { xtype: 'projectdetailview', reference: 'projectdetailview', docked: 'right',  bind: {width:  '{detailview_width}'}  },
    ]
});
