Ext.define('project.MainViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.projectmainviewcontroller',

	routes: { 
		':xtype': {action: 'mainRoute'}
	},

	mainRoute:function(xtype) {
		localStorage.xtype = xtype;
		if(qlconfig.use_login && (qlconfig['always_check_auth']!==undefined && qlconfig['always_check_auth']) ){
			api.getMe();
		}
		//var menuview = this.lookup('projectmenuview');
		var navview = this.lookup('projectnavview');
		var menuview = navview.items.items[0]

		var centerview = this.lookup('projectcenterview');
		var exists = Ext.ClassManager.getByAlias('widget.' + xtype);
		if (exists === undefined) {
			console.log(xtype + ' does not exist');
			return;
		}
		try{
			var node = menuview.getStore().findNode('xtype', xtype);
			if (node == null) {
				console.log('unmatchedRoute: ' + xtype);
				return;
			}
			
			if (!centerview.getComponent(xtype)) {
				centerview.add({ xtype: xtype,  itemId: xtype, heading: node.get('text') });
			}
			Ext.Anim.run(centerview, 'cube', {
				out: false,
				autoClear: true,
				duration:450
			});
			centerview.setActiveItem(xtype);
			menuview.setSelection(node);
			var vm = this.getViewModel(); 
			vm.set('heading', node.get('text'));	
		}catch(e){}
			
		// window.history.pushState("", "", "/"+(node.get('text'))).replace(/ /g,"_") ;
		// console.log("/"+(node.get('text')).replace(/ /g,"_"));
	},

	onMenuViewSelectionChange: function (tree, node) {
		if (node == null) { return }
		var vm = this.getViewModel();
		if (node.get('xtype') != undefined) {
			this.redirectTo( node.get('xtype') );
		}
	},

	onTopViewNavToggle: function () {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
	},

	onHeaderViewDetailToggle: function (button) {
		var vm = this.getViewModel();
		vm.set('detailCollapsed', !vm.get('detailCollapsed'));
		if(vm.get('detailCollapsed')===true) {
			button.setIconCls('x-fa fa-arrow-left');
		}
		else {
			button.setIconCls('x-fa fa-arrow-right');
		}
	},

	onBottomViewlogout: function () {
		localStorage.setItem("LoggedIn", false);
		this.getView().destroy();
		Ext.Viewport.add([{ xtype: 'Login'}]);
	},


//	onActionsViewLogoutTap: function( ) {
//		var vm = this.getViewModel();
//		vm.set('firstname', '');
//		vm.set('lastname', '');
//
//		Session.logout(this.getView());
//		this.redirectTo(AppCamp.getApplication().getDefaultToken().toString(), true);
//	}

});
