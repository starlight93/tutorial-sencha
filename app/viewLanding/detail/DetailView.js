Ext.define('project.DetailView', {
	extend: 'Ext.Container',
	xtype: 'projectdetailview',
  cls: 'detailview',
  layout: 'fit',
  items: [
    {
      xtype: 'container', 
      style: 'background:white', 
      html: '<div style="padding:10px;font-size:24px;">detailview</div>'
    }
  ]
})