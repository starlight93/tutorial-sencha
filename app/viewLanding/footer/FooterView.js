Ext.define('project.FooterView', {
    extend: 'Ext.Toolbar',
    xtype: 'projectfooterview',
    cls: 'footerview',
    viewModel: {},
    items: [
        {
            xtype: 'container',
            cls: 'footerviewtext',
        html: 'Ext JS version: ' + Ext.versions.extjs.version
            //bind: { html: '{name} footer' }
        }
    ]
});
