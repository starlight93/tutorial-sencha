Ext.define('project.HeaderView', {
    extend: 'Ext.Toolbar',
    xtype: 'projectheaderview',
	cls: 'headerview',
	shadow: true,
    viewModel: {},
	// layout: {
    //     type: 'vbox'
    // },
    items: [		
        { 
			xtype: 'container',
            cls: 'headerviewtext',
            bind: { html: '{heading}' }
        },
        '->',{
			xtype	: "container",
			html 	: "<span></span>",
			afterRender:function(){
				let me = this;
				const d = new Date();
				const year = d.getFullYear();
				const date = d.getDate();
				const months = [
					'Jan','Febr','Mar','Apr',
					'Mei','Jun','Jul','Agu', 'Sep','Okt','Nov','Des'
				];
				const monthName = months[d.getMonth()];
				const days = ['Min','Sen','Sel','Rab','Kam','Jum','Sab'];
				const dayName = days[d.getDay()];
				const format = `${dayName}, ${date} ${monthName} ${year}`;
				var timer = setInterval(
					function() {
						var tanggal = new Date();
						try{
							x = me.element.dom;;
							h = tanggal.getHours();
							m = tanggal.getMinutes();
							s = tanggal.getSeconds();
							x.innerHTML = format+" "+h + ":" + m + ":" + s;							
						}catch(e){
							clearInterval(timer);
						}
					},1000
				);
				
			}
		},{
		xtype: 'button',
		ui: 'confirm',
		id:"header-btn-maximize",
		tooltip:'Maximize',
		arrow:false,
		iconCls: "x-fas fa-desktop",
		handler:function(el){
			let elem=document.body;
			if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
				
				el.setTooltip("Minimize");
				el.setIconCls("fas fa-angle-double-down");
				if (elem.requestFullScreen) {
					elem.requestFullScreen();
				} else if (elem.mozRequestFullScreen) {
					elem.mozRequestFullScreen();
				} else if (elem.webkitRequestFullScreen) {
					elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
				} else if (elem.msRequestFullscreen) {
					elem.msRequestFullscreen();
				}
			} else {
				el.setTooltip("Maximize");
				el.setIconCls("x-fas fa-desktop");
				if (document.cancelFullScreen) {
					document.cancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}
			}
		}
	},{
		xtype: 'button',
		ui: 'confirm',
		cls:'notif-btn',
		tooltip:'Notifications',
		arrow:false,
		iconCls: "x-fas fa-bell",		
		badgeText: '2'
	},{
		xtype: 'button',
		alignSelf: 'right',
		style:"background-color:white",
		text: 'User',			
		afterRender:function(){
			if(localStorage.me!==undefined){
				this.setText(JSON.parse(localStorage.me)["username"]);
			}
		},
		arrow:true,
		id:"header-btn-profile",
		iconCls: 'x-fa fa-user',
		menu: {
			anchor: true,
			items: [
				{
					xtype:'container',
					layout:'hbox',
					style:{
						margin:'auto',
					},
					items:[
						{
							xtype:'image',
							src:'resources/images/developer.png',
							height: 80,
							width: 80,
							style:{
								padding:10
							}
						}

					]
				}
				, '-', {
					xtype: 'button',
					text:'Logout',
					ui:'alt decline',
					margin:'auto',
					handler: function(){                        
                        localStorage.clear();
                        window.location.href="/";
                    }
				}
			]
		}
	}]
});
