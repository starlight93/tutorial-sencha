Ext.define('project.MenuView', {
    extend: 'Ext.list.Tree',
    xtype: 'projectmenuview',
    viewModel: {},
    ui: 'nave',
    requires: [
        'Ext.data.TreeStore',
    ],
    scrollable: true,
    bind: { 
        store: '{menu}', 
        micro: '{navCollapsed}' 
    },
    hideAnimation:"fadeOut",
    expanderFirst: false,
    expanderOnly: false,
    afterRender:function(){
        qlproject.getApplication().afterLogin();
    }
});
