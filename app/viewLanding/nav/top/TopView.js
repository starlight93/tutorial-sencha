Ext.define('project.TopView', {
    extend: 'Ext.Toolbar',
    xtype: 'projecttopview',
    cls: 'topview',
    viewModel: {},
    shadow: true,
    items: [
        {
            xtype: 'container', 
            cls: 'topviewtext',
            bind: {
                hidden: '{navCollapsed}' 
            },
            html:"project",
            afterRender:function(){
                this.setHtml(qlconfig.project);
            }
        },
        '->',
        {
            xtype: 'button',
            ui: 'topviewbutton',
            reference: 'projectnavtoggle',
            handler: 'onTopViewNavToggle',
            iconCls: 'x-fa fa-bars'
        }
    ]
});