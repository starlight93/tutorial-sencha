var qlconfig = 

// =================================================WILAYAH PENGATURAN DIMULAI

{
    project                     : "LATIHAN",
    development                 : true,
    use_backend                 : false,
    use_login                   : false,
    always_check_auth           : false,
    use_role                    : false,
    auto_uppercase              : false,
    initializePerPage           : false,
    //==============================================BACKEND CONFIGURATION
    backend_development_api     : "https://backend.dejozz.com/pciserver/public/operation",
    backend_development_custom  : "https://backend.dejozz.com/pciserver/public/custom",
    backend_development_login   : "https://backend.dejozz.com/pciserver/public/login",
    backend_development_auth    : "https://backend.dejozz.com/pciserver/public/me",
    backend_development_root_key: "data",
    backend_development_parameters:{
        paginate : "paginate",
        page     : "page",
        current_page:"current_page",
        page_end : "last_page",
        filter_fields : "searchfield",
        filter_text   : "search",
        order_by : "orderBy",
        order_type: "orderType",
        select_fields : "selectField"
    },

    backend_production_api      : "https://backend.dejozz.com/pciserver/public/operation",
    backend_production_custom   : "https://backend.dejozz.com/pciserver/public/custom",
    backend_production_login    : "https://backend.dejozz.com/pciserver/public/login",
    backend_production_auth     : "https://backend.dejozz.com/pciserver/public/me",
    backend_production_root_key : "data",
    backend_production_parameters:{
        paginate : "paginate",
        page     : "page",
        current_page:"current_page",
        page_end : "last_page",
        filter_fields : "searchfield",
        filter_text   : "search",
        order_by : "orderBy",
        order_type: "orderType",
        select_fields : "selectField"
    },
    
    backend_barcode_renderer    : "https://backend.dejozz.com/pdfrenderer/barcode.php",
    backend_pdf_renderer        : "https://backend.dejozz.com/pdfrenderer/one.php",
    backend_xls_renderer        : "https://backend.dejozz.com/pdfrenderer/excel.php",
    // report_header_design        :`<table style="width:100%">
    //     <tr><td></td><td style="width:32%"></td><td style="align:right;">
    //         <img src="http://www.igaabadi.com/docfile/page/contact1_pict1.jpg" />
    //     </td></tr>
    // </table>`,
    //report_header_design      : `<table style="width:30%">
    //     <tr><td style="width:200px">
    //         <img src="http://www.igaabadi.com/docfile/page/contact1_pict1.jpg" />
    //     </td><td></td></tr>
    // </table>`,
    backend_error_action        : function(model, errors, code){
        if(code!=500 && errors!==null){
            var errorList = errors['errors'];
            if(!Array.isArray(errorList) ){
                if(typeof(errorList)=='object'){
                    if(errorList['error']!==undefined){
                        Ext.Msg.alert("Failed!","Maybe Server Error");
                        return;
                    }else{
                        Ext.Msg.alert("Failed!","Maybe Server Error");
                        return;                        
                    }
                }
                Ext.Msg.alert("Failed!",errorList);
                return;
            }
            var errorMessage = "";
            var form = Ext.getCmp(model);
            var fields = form!==undefined?form.getFields():{};
            errorList.forEach(function(error){
                let errorString = error.split(".")[0];
                if(errorString.includes( "The " ) && error.includes(model)){
                    errorString = (errorString.split("]The ")[1]);
                    let fieldName = !errorString.includes(" field ")?(errorString.split(" ")[0]):(errorString.split(" field")[0]).replace(/ /g,"_");
                    let field = fields[ fieldName ];
                    if(field!==undefined){ field.setError(errorString) }
                }
                errorMessage += Ext.String.capitalize(errorString)+"<br>";
            });
            Ext.Msg.alert("Failed!",errorMessage);
        }else{
            Ext.Msg.alert("Failed!", "Mungkin Server Sedang Bermasalah!" );
        }
    },
    backend_where_and           : function( store, filters ){
        let whereParameter = null;
        let table = store.getProxy().table===undefined?store.getStoreId() : store.getProxy().table;
        if(store.getProxy().parameters!==undefined && store.getProxy().parameters.where!==undefined){
            whereParameter = store.getProxy().parameters.where;
        }
        let whereCandidate = "";
        filters.forEach( (dt,i) => {
            if( !(dt.property).includes(".") ){
                dt.property = table + "." + dt.property;
            }
            whereCandidate+=`${i==0?'':' AND'} lower(${dt.property}::TEXT)=lower('${dt.value}')`;
        });
        if(whereParameter!==null){
            whereCandidate+=`AND (${whereParameter})`;
        }
        if( store.getProxy().parameters.oldWhere !== undefined ){
            if(store.getProxy().parameters.oldWhere != whereCandidate){
                store.getProxy().setExtraParam('where', whereCandidate);
                store.load();
            }
        }else{
            store.getProxy().setExtraParam('where', whereCandidate);
            store.load();
        }
        store.getProxy().parameters.oldWhere = whereCandidate;
    }
    //===============================================BACKEND CONFIGURATION END
    
}

//==================================================WILAYAH PENGATURAN SELESAI