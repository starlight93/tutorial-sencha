var qlconfig_sidebar =
    //MENU MENU YANG AKAN DITAMPILKAN DI SIDEBAR DI MULAI DI DALAM KURUNG ARRAY [ ]
    [
        { text: "Home", iconCls: "x-fa fa-home", xtype: "Home", leaf: true, expanded: true },
        { text: "Practice 1 Layout", iconCls: "x-fa fa-sitemap", xtype: "practice_1_layout", leaf: true, expanded: true },
        { text: "Practice 2 Field & Events", iconCls: "x-fa fa-equals", xtype: "practice_2_field", leaf: true, expanded: true },
        { text: "Practice 3 Form & Validation", iconCls: "x-fa fa-file-alt", xtype: "practice_3_form", leaf: true, expanded: true },
        { text: "Practice 4 Grid & Store", iconCls: "x-fa fa-table", xtype: "practice_3_grid_table", leaf: true, expanded: true },
        { text: "Practice 5 Tab Panel", iconCls: "x-fa fa-home", xtype: "", leaf: true, expanded: true },
        { text: "Practice 6 Http Request", iconCls: "x-fa fa-home", xtype: "", leaf: true, expanded: true }        
    ]
var qlconfig_sidebar_copy = qlconfig_sidebar;
//MENU SELESAI