Ext.define('qlproject.Application', {
	extend: 'Ext.app.Application',
	name: 'qlproject',
	requires: ['qlproject.*'],
	defaultToken:"Login",
	removeSplash: function () {
		Ext.getBody().removeCls('launching')
		var elem = document.getElementById("splash")
		elem.parentNode.removeChild(elem)
	},
	launch: function () {
		document.addEventListener('keydown',keyPress);
		if(localStorage.auth!==undefined){
			api.setHeaders(JSON.parse(localStorage.auth));
			api.afterLogin();
		}	
		this.removeSplash();
		localStorage.first = "1";
		api.getMe(function(json){
			localStorage.me=JSON.stringify(json);
			document.title = qlconfig.project+" - "+json['username'];
			try{Ext.getCmp("header-btn-profile").setText(json['username']);}catch(e){}
		});
	},

	onAppUpdate: function () {
		Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
			function (choice) {
				if (choice === 'yes') {
					window.location.reload();
				}
			}
		);
	},

	afterLogin:function(){
		if(qlconfig.use_role){
			Ext.getStore("store_online_current_user_role").load();
		}else{
			Ext.StoreManager.lookup("storemenulist").setRoot({
				"expanded": true,
				"children": qlconfig_sidebar
			});			
            var oldXtype = localStorage.xtype;
            window.location.href="#-";
            if( (window.location.href).includes("#")){
                setTimeout(function () {
                    window.location.href="#"+oldXtype;
                },100);
            }
		}
	},
	
	loginSuccess:function(data){
		try{
			let auth = {
				Authorization	: `${data['token_type']} ${data['token']}`
			};
			api.setHeaders(auth);
			api.afterLogin();
			api.getMe(function(json){
				localStorage.me=JSON.stringify(json);
				document.title = qlconfig.project+" - "+json['username'];
				try{Ext.getCmp("header-btn-profile").setText(json['username']);}catch(e){}
			});
		}catch(e){console.log(e);}
		this.setDefaultToken("Home");
		if((Ext.Viewport.getItems().items).length>0){
			Ext.Viewport.remove(0);
		}
		Ext.Viewport.add([{xtype:"projectmainview"}]);	
		var oldXtype = localStorage.xtype!=undefined?localStorage.xtype:"Home";
		window.location.href="#-";
		setTimeout(function () {
			window.location.href="#"+oldXtype;
		},100);
		localStorage.first="0";
	}
});
