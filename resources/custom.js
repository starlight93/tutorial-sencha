var findParentModel = function(el) {
    var parent = el.getParent();
    if(parent===null || parent===undefined){
        return null;
    }
	if (parent.getViewModel() != undefined && parent.getViewModel() != null ) {
        return parent.getViewModel();
    } else {
        return findParentModel(parent);
    }
};
function getStoreData(storeId){
    var data = [];
    try{
        var rawData = Ext.getStore(storeId).getData().items;
        rawData.forEach(dt=>{
            data.push(dt.data);
        });
    }catch(e){}
    return data;
}
var keyPress =(e)=>{
    if(e.key === "Escape") {
        if(confirm('refresh anda setelah ini akan ke home?')){
            sessionStorage.clear();
            window.location.replace("/");
        }
    }
}
var updateHeight=(element)=>{
    try{
        var tabpanel = element.up("navigationview").getItems().items[2].getItems().items[1];
        tabpanel.getItems().items.forEach(function(children){
            let grid = children.down("grid");
            if(grid!==null){
                let gridHeightHeader = grid.renderElement.dom.getElementsByClassName("x-headercontainer")[0].offsetHeight;
                let gridHeightBody   = 0;
                let rows = grid.renderElement.dom.getElementsByClassName("x-listitem");
                Array.from(rows).forEach(function(row){
                    gridHeightBody+=row.offsetHeight;
                });
                grid.setHeight(gridHeightHeader+gridHeightBody+40);
            }
        });
        tabpanel.afterRender();
    }catch(e){
        console.log(e);
    }
}